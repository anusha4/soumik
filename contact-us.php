<?php  include('addons/header.php'); ?>

        <!-- Sticky Header -->
        <div class="sticky-header">
            <div class="auto-container clearfix">
                <!--Logo-->
                <div class="logo pull-left">
                    <a href="index.php" title=""><img src="images/logo-small.png" alt="soumik-scroll-logo" title="soumik-scroll-logo"></a>
                </div>
                <!--Right Col-->
                <div class="pull-right">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="about-us.php">About us</a></li>
                                <li class="dropdown"><a href="services.php" >Services</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="support-for-education.php"> Education </a></li>    
                                        <li><a href="support-for-health.php"> Health </a></li>
                                        <li><a href="helping-poor-people.php"> Helping People </a></li>
                                    </ul>
                                </li>
                                <li><a href="causes-list.php">Causes</a></li> 
                                <li><a href="event-list.php">Events</a></li>                              
                                <li><a href="contact-us.php">Contact Us</a></li>
                               <?php
                                if(isset($_SESSION['username'])=='')
                                {
                                ?>
                                <li><a href="signin-registration.php">Login</a></li>
                                <?php
                            }
                            else
                            {
                            ?>

                                <li class="dropdown"><a href="services.php" ><?php echo $_SESSION['fullname'];?></a>
                                    <ul class="dropdown-menu">
                                       
                                        <li><a href="profile.php"> My Profile </a></li>
                                        <li><a href="logout.php"> Logout </a></li>
                                    </ul>
                                </li>
                                <?php
                            }
                            ?>
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>
            </div>
        </div>
    </header>
    <!--End Main Header -->

    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3860.0505958309145!2d77.60837331484083!3d14.653069389769104!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6e496b019cc3d878!2sSoumik+Charitable+Trust-Non+Profit+Organisation+in+Hyderabad%2CAnantapur%2CIndia!5e0!3m2!1sen!2sin!4v1541802281819" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

     <!-- <section class="container-fluid">
        
            <div class="map-column">
                <div class="inner-column">
                    <div class="map-outer">
                        
                        <div style="height: 420px;" class="map-canvas"
                            data-zoom="12"
                            data-lat="14.6819"
                            data-lng="77.6006"
                            data-type="roadmap"
                            data-hue="#ffc400"
                            data-title="Envato"
                            data-icon-path="images/icons/map-marker.png"
                            data-content="Melbourne VIC 3000, Australia<br><a href='mailto:info@youremail.com'>info@youremail.com</a>">
                        </div>
                    </div>
                </div>
            </div>
    </section> -->
    <!-- Contact Page Section -->

    <section class="container-fluid contact-page-section">
        <div class="col-md-6 form-column">
            <div class="clearfix inner-column">
                <h2>Get in touch</h2>
                  <hr>
                    <p>Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further the overall value proposition. Organically grow the holistic world view of disruptive innovation via workplace diversity and empowerment.</p>
                        <div class="info-box col-md-12 col-sm-12 col-xs-12">
                             <div class=" col-md-6 col-sm-6 col-xs-12">
                             <span class="icon flaticon-e-mail-envelope"></span>
                             <h4>EMAIL</h4>
                        </div>
                         <div class="col-md-6 col-sm-6 col-xs-12">
                             <p>info@soumikcharitabletrust.org</p>
                             <p>help@soumikcharitabletrust.org</p>
                          </div>
                    </div>
                    <div class="info-box col-md-12 col-sm-12 col-xs-12">
                             <div class=" col-md-6 col-sm-6 col-xs-12">
                            <span class="icon flaticon-smartphone"></span>
                            <h4>Phone</h4>
                            </div>
                            <div class=" col-md-6 col-sm-6 col-xs-12">
                                  <p class="">+91 9000302235 / +91 9010942738 </p>
                                 
                             </div>
                    </div>

                        <div class="info-box col-md-12 col-sm-12 col-xs-12">
                             <div class=" col-md-6 col-sm-6 col-xs-12">
                             <span class="icon flaticon-map-1"></span>
                            <h4>Address</h4>
                            </div>
                             <div class="col-md-6 col-sm-6 col-xs-12">
                             <p>D.No: 28-4-797-2 <br> State Bank Colony
                             JNTU Road <br> Ananthapuram - 515002 (AP)</p>
                           
                          
                        </div>
                    </div>
            </div>
            <style>
                .error
                {
                    color:red;
                }
            </style>

        </div>
        <div class="col-md-6">
            <div class="contact-form">
                        <h2 class="leave-a-reply text-center">Leave a Reply</h2>
                        <hr>
                        <!--Contact Form-->
                        <form method="post" action="#" onsubmit="return false">
                            <div class="row clearfix">
                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <input type="text" name="username" id="name" placeholder="Name">
                                    <span class="error" style="display:none">Enter Name</span>
                                </div>
                               
                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <input type="email" name="email" id="email" placeholder="Mail">
                                    <span class="error" style="display:none">Enter valid emailId</span>
                                </div>
                                 <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <input type="text" name="email" id="mobileno" placeholder="Mobileno" onkeypress="return isNumberKey(event)" maxlength="10">
                                    <span class="error" style="display:none">Enter valid Mobileno</span>
                                </div>
                               
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <textarea name="message" id="comment" placeholder="Message"></textarea>
                                    <span class="error" style="display:none">Enter Description</span>
                                    <span class="sub_error_msg"></span>
                                </div>
                               
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group text-center">
                                    <button class="theme-btn btn-style-one" id="submit_contact" type="submit" name="submit-form">Send Now</button>
                                </div>
                            </div>
                        </form>
                        
                    </div><!--endif Contact Form -->

                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                    <script type="text/javascript">
                        $("#submit_contact").on('click',function()
              {
                  
                  $('.error').hide();
                  
                var name=$('#name').val();
                var mail=$('#email').val();
                var mobileno=$('#mobileno').val();
                var comment = $("#comment").val();
                var atpos = mail.indexOf("@");
                var dotpos = mail.lastIndexOf(".");
                
                if(name.length==0)
                {
                    $('#name').siblings('.error').show();
                }
               else if(mail.length==0)
                {
                    $('#email').siblings('.error').show();
                }
                 else if (atpos<1 || dotpos<atpos+2 || dotpos+2>=mail.length) 
                          {
                           
                           $('#email').siblings('.error').show();
                                  
                          }
                
                else if(mobileno.length==0)
                {
                  $('#mobileno').siblings('.error').show();
                }
                 else if(mobileno.length<10)
                {
                   $('#mobileno').siblings('.error').show();
                }
                 else if(mobileno.length>10)
                {
                    $('#mobileno').siblings('.error').show();
                }
                else if(comment.length==0)
                {
                   $('#comment').siblings('.error').show();
                }
                else
                {
              
                                $('.sub_error_msg').fadeIn().css('color','green').html("<b>Thank You We Will Contact Very Soon...</b>");
                                
                          
                 $.ajax({

                          type: "post",
                          url: "https://soumikcharitabletrust.org/sendmail1.php",
                          data: {name:name,mail:mail,mobileno:mobileno,comment:comment},

                          success:function(data){
                            var jsondata = JSON.parse(data);

                            if(jsondata.status == 1)
                            {
                                 window.location="https://soumikcharitabletrust.org";
                           
                           
                          
                           // $('.sub_success_msg').hide(); 
                            //alert("thank you we will contact very soon..");

                            }

                            else
                            {
                              alert("Failure");
                            }
                          }

                      });
                }


               
              });
              </script>
        </div>
    </section>

    <!--Main Footer-->
    <footer class="main-footer" style="background-image: url(images/background/2.jpg);">
        <!--Upper-->
        <div class="footer-upper">
            <div class="auto-container">
                <div class="outer-box">
                    <!--Footer Logo-->
                    <div class="footer-logo"><a href="index.php"><img src="images/footer-logo.png" alt="footer-logo" title="footer-logo"></a></div>
                    
                    <div class="row clearfix">
                        <div class="big-column col-md-4 col-sm-12 col-xs-12">
                            <div class="row clearfix">
                                <!--Footer Column-->
                                <h3>Menu</h3>
                                <ul class="footer-links">
                                     <li><a href="index.php">Home</a></li>
                                    <li><a href="about-us.php">About us</a></li>
                                    <li><a href="causes-list.php">Causes</a></li> 
                                    <li><a href="event-list.php">Events</a></li>                              
                                    <li><a href="contact-us.php">Contact Us</a></li>
                                </ul> 
                            </div>
                        </div>

                        <div class="big-column col-md-4 col-sm-12 col-xs-12">
                            <div class="row clearfix">
                                <!--Footer Column-->
                               <h3>Contact </h3>
                                <ul class="footer-links contact-links">
                                    <li>D.No: 28-4-797-2, State Bank Colony <br>
                                     JNTU Road, Ananthapuram - 515002 (AP)</li>
                                    <li>+91 9000302235 / 9010942738</li>
                                    <li><a href="#">info@soumikcharitabletrust.org</a></li>
                                </ul> 
                            </div>
                        </div>
                        <div class="big-column col-md-4 col-sm-12 col-xs-12">
                            <div class="row clearfix">    
                                <!--Footer Column-->
                                <h3>About</h3>
                                <div class="text">
                                    <p>Our charity lends the helping hand to the people in desperate situations. When their life becomes the destiny of the darkest path.</p>
                                </div>
                                <br>
                                <h3>Follow</h3>
                                <ul class="footer-social-links">
                                    <li><a href="https://www.facebook.com/soumikcharitabletrust/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="https://twitter.com/SoumikTrust" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                    <!-- <li><a href="#"><i class="fa fa-google-plus"></i></a></li> -->
                                </ul> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!--Bottom-->
        <div class="footer-bottom">
            <div class="auto-container">
                <div class="copyright">Copyright<a href="index.php">Soumik Charitable Trust</a> © 2018. All Rights Reserved.</div>
            </div>
        </div>
    </footer>
    <!-- End Footer -->

</div>  
<!--End pagewrapper-->


<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-angle-double-up"></span></div>
<script src="js/jquery.js?soumikversion=1"></script> 
<script src="js/bootstrap.min.js?soumikversion=1"></script>
<script src="js/jquery-ui.js?soumikversion=1"></script> 
<script src="js/jquery.fancybox.js?soumikversion=1"></script>
<script src="js/owl.js?soumikversion=1"></script>
<script src="js/wow.js?soumikversion=1"></script>
<script src="js/validate.js?soumikversion=1"></script>
<script src="js/script.js?soumikversion=1"></script>
<!--Google Map APi Key-->
<!-- <script src="http://maps.google.com/maps/api/js?key=AIzaSyDKU4t6_OPgpV1FjB0yRpGAJLz9ycg4tF8"></script>
 -->
<!-- <script src='https://maps.googleapis.com/maps/api/js?v=3&libraries=places&key=AIzaSyAqV9BGFW-A_WUYrZch4B-UEI3c_9A1j7U'></script> -->

<script src="http://maps.google.com/maps/api/js?key=AIzaSyBKS14AnP3HCIVlUpPKtGp7CbYuMtcXE2o"></script>

<script src="js/map-script.js"></script>
<!--End Google Map APi-->
</body>
</html>

<script type="text/javascript">
      
      function isNumberKey(evt){
          var charCode = (evt.which) ? evt.which : event.keyCode
          if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
          return true;
      }

    </script>
<?php include('addons/header.php'); ?>

        <!-- Sticky Header -->
        <div class="sticky-header">
            <div class="auto-container clearfix">
                <!--Logo-->
                <div class="logo pull-left">
                    <a href="index.php" title=""><img src="images/logo-small.png" alt="soumik-scroll-logo" title="soumik-scroll-logo"></a>
                </div>
                <!--Right Col-->
                <div class="pull-right">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="about-us.php">About us</a></li>
                                <li class="dropdown"><a href="services.php" >Services</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="support-for-education.php"> Education </a></li>    
                                        <li><a href="support-for-health.php"> Health </a></li>
                                        <li><a href="helping-poor-people.php"> Helping People </a></li>
                                    </ul>
                                </li>
                                <li><a href="causes-list.php">Causes</a></li> 
                                <li><a href="event-list.php">Events</a></li>                              
                                <li><a href="contact-us.php">Contact Us</a></li>
                               <?php
                                if(isset($_SESSION['username'])=='')
                                {
                                ?>
                                <li><a href="signin-registration.php">Login</a></li>
                                <?php
                            }
                            else
                            {
                            ?>

                                <li class="dropdown"><a href="services.php" ><?php echo $_SESSION['fullname'];?></a>
                                    <ul class="dropdown-menu">
                                       
                                        <li><a href="profile.php"> My Profile </a></li>
                                        <li><a href="logout.php"> Logout </a></li>
                                    </ul>
                                </li>
                                <?php
                            }
                            ?>
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>
            </div>
        </div>
    </header>
    <!--End Main Header -->

    <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/education-bg.jpg)">
        <div class="auto-container">
            <h1>Education</h1>
            <ul class="bread-crumb clearfix">
                <li><a href="index.php">Home </a></li>
               <!--  <li><a href="services.php">Services</li> -->
                    <li>Education</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    <section class="work-section style-two alternate">
        <div class="auto-container">
            <!-- Work Steps -->
            <div class="steps-outer">
                <div class="row clearfix">
                    <hr>
                   <div class="sec-title text-center">
               <h2> Support for <span> Children Education</span></h2>
                <div class="separator-two"></div>
            </div>
            <br>
                    

                    <div class="work-step education-inner col-md-12 col-sm-12 col-xs-12">
                        <div class="inner-box">
                            <h3 style="color: orange;text-align: center;"><span style="font-weight: bold;font-size: 30px;">“</span> The future rests in the hands of the Younger generation <span style="font-weight: bold;font-size: 30px;">”</span></h3><br>
                            <p>Education is vital aspect to shape the future of the younger generation. But, just question yourself, how many youngsters are able to attend the school and afford the expenses in the education system. There are many bright children who are not able to attend the school or college or higher studies, just because of the financial disability. </p><br>
                                <p><b>Why most of the children are becoming child labour?</b><p><br>
                                   <ul>
                                <li><b>*</b> Lack of financial support</li>
                                <li><b>*</b> Lack of food and clothing</li>
                                <li><b>*</b> Lack of awareness about the education to the family</li>
                                <li><b>*</b> Lack of support from the family</li>
                                <li><b>*</b> Some are becoming orphans at a younger age</li>
                            </ul><br>
                               <p> Despite all the odds, Soumik charity stood strong to help the students who are looking for an assistance to continue their education at any level. Soumik Charity conducted awareness programs and enlightens the parents about the value of education and helped many students to enjoy their schooling.</p><br>
                                <p>Soumik charity has the intent for building a better society and we believe that helping an individual will certainly provide a better future for many individuals in the society.</p><br>

                            <p>    Support the children to continue their education and become a part in building a bright future for the future generations.</p>
                            
                             </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
    <!--end work section-->

    


  <?php include('addons/footer.php'); ?>
<?php include('addons/header.php'); ?>

        <!-- Sticky Header -->
        <div class="sticky-header">
            <div class="auto-container clearfix">
                <!--Logo-->
                <div class="logo pull-left">
                    <a href="index.php" title=""><img src="images/logo-small.png" alt="" title=""></a>
                </div>
                <!--Right Col-->
                <div class="pull-right">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="about-us.php">About us</a></li>
                                <li class="dropdown"><a href="services.php" >Services</a>
                                	<ul class="dropdown-menu">
                                		<li><a href="support-for-education.php"> Education </a></li>	
                                		<li><a href="support-for-health.php"> Health </a></li>
                                		<li><a href="helping-poor-people.php"> Helping People </a></li>
                                	</ul>
                                </li>
                                <li><a href="causes-list.php">Causes</a></li> 
                                <li><a href="event-list.php">Events</a></li>                              
                                <li><a href="contact-us.php">Contact Us</a></li>
                                <li><a href="signin-registration.php"> Login</a></li>
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>
            </div>
        </div>
    </header>
    <!--End Main Header -->

    <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/18.jpg);">
        <div class="auto-container">
            <h1>Event Single Post</h1>
            <ul class="bread-crumb clearfix">
                <li><a href="index.php">Home </a></li>
                <li>Event Single Post</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->

    <!-- Sidebar Page Container -->
    <div class="sidebar-page-container sidebar-content-margin">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Content Side-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="event-single">
                        <!-- Event Block -->
                        <div class="event-block-two">
                            <div class="inner-box">
                                <div class="image-box">
                                    <div class="date"><span>04</span> April</div>
                                    <img src="images/resource/event-single.jpg" alt="">
                                </div>
                                <div class="content-box">
                                    <h4>Gear up for giving</h4>
                                    <ul class="info">
                                        <li><i class="fa fa-clock-o"></i>at 5.00 pm - 7.30 pm</li>
                                        <li><i class="fa fa-map-marker"></i>25 Newyork City</li>
                                    </ul>
                                    <ul class="detail clearfix">
                                        <li><span>Topics :</span>  Donation & Help,</li>
                                        <li><span>Start Date :</span>  April 06 2018,</li>
                                        <li><span>Host :</span>  Kodesolution Lmd,</li>
                                        <li><span>End Date :</span>  April 08 2018,</li>
                                        <li><span>Location :</span>  121 King Street, Melbourne, Australia</li>
                                    </ul>
                                    <p>Bring to the table win-win survival strategies to ensure proactive domination. Capitalize on low hanging fruittionPodcasting operational change management inside of workflows to establish a framework. Taking seamless key Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further the overall value proposition. Organically grow the holistic world view of disruptive innovation via workplace diversity and empowerment.</p>

                                    <p>Bring to the table win-win survival strategies to ensure proactive domination. At the end of the day, going forward, a new normal that has evolved from generation X is on the runway heading towards a lined cloud solution. User generated content in real-time will have multiple touchpoints for offshoring.Capitalize on low hanging fruit to identify a ballpark value added activity to beta test.<br> 
                                    Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop on focusing solely on the bottom line.</p>

                                    <p>Podcasting operational change management inside of workflows to establish a framework. Taking less key performance indicators offline to maximise the long tail. Keeping your eye on the ball while ing a deep dive on the start-up mentality to derive convergence on cross-platform integration.</p>
                                </div>
                            </div>
                        </div>

                        
                    </div>
                </div>

                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <aside class="sidebar">
                        <!-- Search Box -->
                        <div class="sidebar-widget search-box">
                            <div class="widget-content">
                                <div class="sidebar-title"><h3>Search</h3></div>
                                <form method="post" action="http://wp3.commonsupport.com/html/varna-charity/blog.html">
                                    <div class="form-group">
                                        <input type="search" name="search-field" value="" placeholder="Enter Your Keyword" required="">
                                        <button type="submit" class="theme-btn btn-style-one">Search</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <!-- Post Widget -->
                        <div class="sidebar-widget post-widget">
                            <div class="sidebar-title"><h3>Past Event</h3></div>
                            <div class="widget-content">
                                <article class="post">
                                    <div class="thumb"><img src="images/resource/post-thumb-1.jpg" alt=""></div>
                                    <h3>Providing drinking water for charity</h3>
                                    <div class="date">08:30 am / Apr 14, 2018</div>
                                </article>

                                <article class="post">
                                    <div class="thumb"><img src="images/resource/post-thumb-2.jpg" alt=""></div>
                                    <h3>The childrents family <br> trust</h3>
                                    <div class="date">08:30 am / Apr 14, 2018</div>
                                </article>

                                <article class="post">
                                    <div class="thumb"><img src="images/resource/post-thumb-3.jpg" alt=""></div>
                                    <h3>Famous qutes about giving</h3>
                                    <div class="date">08:30 am / Apr 14, 2018</div>
                                </article>

                                <article class="post">
                                    <div class="thumb"><img src="images/resource/post-thumb-4.jpg" alt=""></div>
                                    <h3>Providing drinking water for charity</h3>
                                    <div class="date">08:30 am / Apr 14, 2018</div>
                                </article>

                                <article class="post">
                                    <div class="thumb"><img src="images/resource/post-thumb-1.jpg" alt=""></div>
                                    <h3>Providing drinking water for charity</h3>
                                    <div class="date">08:30 am / Apr 14, 2018</div>
                                </article>
                            </div>
                        </div>

                        
                    </aside>
                </div>

            </div>
        </div>
    </div>  
    <!-- End Sidebar Page Container -->

    <section>
        <div class="auto-container">
            <div class="row">
                <!-- Related Events -->
                <div class="related-events">
                    <h3>Related Event</h3>

                    <div class="row clearfix">
                        <!-- Event Block -->
                        <div class="event-block-two col-md-4 col-sm-4 col-xs-12">
                            <div class="inner-box">
                                <div class="image-box">
                                    <div class="date"><span>08</span> April</div>
                                    <img src="images/resource/event-grid-5.jpg" alt="">
                                </div>
                                <div class="content-box">
                                    <h4>Collecting poor childrens</h4>
                                    <ul class="info">
                                        <li><i class="fa fa-clock-o"></i>at 5.00 pm - 7.30 pm</li>
                                        <li><i class="fa fa-map-marker"></i>25 Newyork City</li>
                                    </ul>
                                    <p>Bring to the table win-win survival strategies to ensure proactive domination. Capitalize on low hanging fruittionPodcasting operational change management inside of workflows to establish a framework. Taking seamless key</p>
                                </div>
                            </div>
                        </div>

                        <!-- Event Block -->
                        <div class="event-block-two col-md-4 col-sm-4 col-xs-12">
                            <div class="inner-box">
                                <div class="image-box">
                                    <div class="date"><span>09</span> April</div>
                                    <img src="images/resource/event-grid-6.jpg" alt="">
                                </div>
                                <div class="content-box">
                                    <h4>Drought Information</h4>
                                    <ul class="info">
                                        <li><i class="fa fa-clock-o"></i>at 5.00 pm - 7.30 pm</li>
                                        <li><i class="fa fa-map-marker"></i>25 Newyork City</li>
                                    </ul>
                                    <p>Bring to the table win-win survival strategies to ensure proactive domination. Capitalize on low hanging fruittionPodcasting operational change management inside of workflows to establish a framework. Taking seamless key</p>
                                </div>
                            </div>
                        </div>
                        <!-- Event Block -->
                        <div class="event-block-two col-md-4 col-sm-4 col-xs-12">
                            <div class="inner-box">
                                <div class="image-box">
                                    <div class="date"><span>09</span> April</div>
                                    <img src="images/resource/event-grid-6.jpg" alt="">
                                </div>
                                <div class="content-box">
                                    <h4>Drought Information</h4>
                                    <ul class="info">
                                        <li><i class="fa fa-clock-o"></i>at 5.00 pm - 7.30 pm</li>
                                        <li><i class="fa fa-map-marker"></i>25 Newyork City</li>
                                    </ul>
                                    <p>Bring to the table win-win survival strategies to ensure proactive domination. Capitalize on low hanging fruittionPodcasting operational change management inside of workflows to establish a framework. Taking seamless key</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Styled Pagination -->
                <!-- <div class="styled-pagination">
                    <ul class="clearfix">
                        <li class="prev"><a href="#">Prev</a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#" class="active">3</a></li>
                        <li class="next"><a href="#">Next</a></li>
                    </ul>
                </div> -->
            </div>
        </div>
    </section>


    

  <?php include('addons/footer.php'); ?>
<?php
session_start();
include('admin/api/db.php');  ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Help for Education|Health|Food-NGO-Soumik Charitable Trust</title>

<!-- Stylesheets -->
<link href="css/bootstrap.css?soumikversion=1.1" rel="stylesheet">
<link href="css/style.css?soumikversion=1.1" rel="stylesheet">


<link href="css/responsive.css?soumikversion=1.1" rel="stylesheet">

<link href="css/forms.css?soumikversion=1.1" rel="stylesheet">

<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
<link rel="icon" href="images/favicon.png" type="image/x-icon">

         <?php

             $urlArray = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
             $segments = explode('/', $urlArray);
             $numSegments = count($segments); 
             $currentSegment = $segments[$numSegments - 1];

                 $queryseo = "SELECT * FROM home_seo where seo_page='$currentSegment'" ;
                $resultseo=mysqli_query($con,$queryseo);
                $i=1;
               if($data = mysqli_fetch_assoc($resultseo))
               {
                ?>
                

<meta name="description" content="<?php echo $data['seo_description']; ?>" />
<meta name="keywords" content="<?php echo $data['seo_keywords']; ?>" />
<meta name="title" content="<?php echo $data['seo_title']; ?>" />

<?php
}
?>


<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="-1" />

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127259927-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127259927-1');
</script>

<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5bdc0946a5b4b87720425e91/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
</head>

<body>
    
    <style>
.sticky{

  
    position:fixed; padding:7px 7px; right:-27px; top: 40%;text-align: center; background: linear-gradient(45deg,#ea7325 1%,#fcc005 100%); color: white; cursor: hand; cursor: pointer;font-weight: bold;font-size: 12px; border-top-left-radius: 5px; border-top-right-radius: 5px;
    transform: rotate(-90deg);z-index: 100;border: orange;
}
</style>
<a href="payu-confirm.php"><button class="sticky">DONATE NOW</button></a>

<div class="page-wrapper">
    <!-- Preloader -->
    <div class="preloader"></div>

    <!-- Main Header-->
    <header class="main-header header-style-two">
        <!--Header Top-->
        <div class="header-top">
            <div class="auto-container">
                <div class="inner-container clearfix">
                    <div class="top-left pull-left">
                       <ul class="contact-info">
                           <li><span class="fa fa-envelope"></span>info@soumikcharitabletrust.org</li>
                       </ul>
                    </div>
                    <div class="top-left pull-right">
                       <ul class="contact-info">
                           <li><span class="fa fa-phone"></span> +91 9000302235 / 9010942738</li>
                       </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Header Top -->

        <!-- Header Lower -->
        <div class="header-lower">
            <div class="auto-container">
               <div class="main-box clearfix">
                    <!--Logo Box-->
                    <div class="logo-box">
                        <div class="logo"><a href="index.php"><img src="images/logo-2.png" alt="soumik-logo1" title="soumik-logo1"></a></div>
                    </div>

                    <!--Nav Outer-->
                    <div class="nav-outer clearfix">
                        <!-- Main Menu -->
                        <nav class="main-menu">
                            <div class="navbar-header">
                                <!-- Toggle Button -->
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>

                            <div class="navbar-collapse collapse clearfix">
                                <ul class="navigation clearfix">
                                    
                                    <li><a href="index.php">Home</a></li>
                                    <li><a href="about-us.php">About us</a></li>
                                    <li class="dropdown"><a href="services.php" >Services</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="support-for-education.php"> Education </a></li>    
                                            <li><a href="support-for-health.php"> Health </a></li>
                                            <li><a href="helping-poor-people.php"> Helping People </a></li>
                                        </ul>
                                    </li>
                                    <li><a href="causes-list.php">Causes</a></li> 
                                    <li><a href="event-list.php">Events</a></li>                              
                                    <li><a href="contact-us.php">Contact Us</a></li>
                                    
                                    <?php
                                if(isset($_SESSION['username'])=='')
                                {
                                ?>
                                <li><a href="signin-registration.php">Login</a></li>
                                <?php
                            }
                            else
                            {
                            ?>

                                <li class="dropdown"><a href="services.php" ><?php echo $_SESSION['fullname'];?></a>
                                    <ul class="dropdown-menu">
                                        <?php
                                if(isset($_SESSION['user_usertype'])=='Donaters')
                                {
                                ?>
                                       
                                        <li><a href="profile.php"> My Profile </a></li>
                                         <li><a href="my-donations.php"> My Donations </a></li>
                                        <li><a href="logout.php"> Logout </a></li>
                                    </ul>
                                </li>
                                <?php
                                }
                                else
                                {
                                ?>
                                 <li><a href="profile.php"> My Profile </a></li>
                                         
                                        <li><a href="logout.php"> Logout </a></li>
                                    </ul>
                                </li>
                                <?php
                                }
                            }
                            ?>
                            
                                </ul>
                            </div>
                        </nav>
                        <!-- Main Menu End-->

                        

                    </div>
                </div>
            </div>
        </div>
        <!-- End Header Lower -->
        
        <div class="popup_overlay"></div>
    <div class="validatiin_opopip" style="display:none">
        <button class="conformation_msg_close_btn">X</button>
        <p id="popupquestion" class="text-center">Please select Registration Type</p>
        <button class="conformation_cncl" style=""  onclick="closepopup()">Ok</button>
    </div> 
<style type="text/css">
       
            .conformation_msg_close_btn {
                position: absolute;
                right: 0;
                top: 0;
                width: 30px;
                height: 30px;
                text-align: center;
                border-radius: 0% 0% 0% 50%;
                box-shadow: -1px 1px 2px #444;
                background-color: #ea7325;
                border-radius: 5px;
                color: #fff;
                padding: 5px;
                padding-right: 8px;
                border:0;
                font-family:roboto !important;
            }
            .validatiin_opopip
            {
                position: fixed;
                z-index: 101;
                top: 50%;
                left: 50%;
                transform: translate(-50%,-50%);
                -moz-transform: translate(-50%,-50%);
                -webkit-transform: translate(-50%,-50%);
                -ms-transform: translate(-50%,-50%);
                -o-transform: translate(-50%,-50%);
                background-color: #fff;
                width: 500px;
                padding: 42px;
                text-align: center;
                border: 4px solid #D7D7D7;
                border-radius: 7px;
                font-family: "Open Sans";
                display: none;
            }
            .popup_overlay{
                position: fixed;
                top:0;
                width: 100%;
                height: 100%;
                background-color: rgba(0,0,0,0.7);
                z-index: 100;
                display: none;
            }
            .conformation_cncl {
                padding-left: 20px;
                padding-right: 20px;
                margin-top: 55px;
               background-color: #ea7325;
                border-radius: 5px;
                color: #fff;
                width: auto;
                border: 0;
                padding-top: 5px;
                padding-bottom: 5px;
                 font-family:roboto !important;
            }
    </style>

        
       
        



 
  <!-- Subscribe section -->
    <section class="subscribe-section">
        <div class="auto-container">
            <div class="inner-container">
                <div class="row clearfix">
                    <div class="title-column col-md-6 col-sm-12 col-xs-12">
                        <div class="title">
                            <h2>Donation Hotline : +91 9000302235 / 9010942738</h2>
                        </div>
                    </div>
                    <div class="form-column col-md-6 col-sm-12 col-xs-12">
                        <div class="subscribe-form">
                          <form method="post" action="#" onsubmit="return false">
                                <div class="form-group">
                                    <input type="email" name="email" id="subscribeemail" value="" placeholder="Enter Your email">
                                    <span class="error"></span>
                                     <p class="sub_error_msg"></p>
                                    <button type="submit" class="theme-btn btn-style-three subscribe" >Subscribe</button>
                                </div>
                            </form>
                            	
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </section>
    <!-- End Subscribe section -->
    <!--Main Footer-->
    <footer class="main-footer" style="background-image: url(images/background/2.jpg);">
        <!--Upper-->
        <div class="footer-upper">
            <div class="auto-container">
                <div class="outer-box">
                    <!--Footer Logo-->
                    <div class="footer-logo"><a href="index.php"><img src="images/footer-logo.png" alt="footer-logo" title="footer-logo"></a></div>

                    <div class="row clearfix">
                        <div class="big-column col-md-4 col-sm-12 col-xs-12">
                            <div class="row clearfix">
                                <!--Footer Column-->
                                <h3>Menu</h3>
                                <ul class="footer-links">
                                     <li><a href="index.php">Home</a></li>
                                    <li><a href="about-us.php">About us</a></li>
                                    <li><a href="causes-list.php">Causes</a></li> 
                                    <li><a href="event-list.php">Events</a></li>                              
                                    <li><a href="contact-us.php">Contact Us</a></li>
                                </ul> 
                            </div>
                        </div>

                        <div class="big-column col-md-4 col-sm-12 col-xs-12">
                            <div class="row clearfix">
                                <!--Footer Column-->
                               <h3>Contact </h3>
                                <ul class="footer-links contact-links">
                                    <li>D.No: 28-4-797-2, State Bank Colony <br>
                                     JNTU Road, Ananthapuram - 515002 (AP)</li>
                                    <li>+91 9000302235 / 9010942738</li>
                                    <li>info@soumikcharitabletrust.org</li>
                                </ul> 
                            </div>
                        </div>
                        <div class="big-column col-md-4 col-sm-12 col-xs-12">
                            <div class="row clearfix">    
                                <!--Footer Column-->
                                <h3>About</h3>
                                <div class="text">
                                    <p>Our charity lends the helping hand to the people in desperate situations. When their life becomes the destiny of the darkest path.</p>
                                </div>
                                <br>
                                <h3>Follow</h3>
                                <ul class="footer-social-links">
                                    <li><a href="https://www.facebook.com/soumikcharitabletrust/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="https://twitter.com/SoumikTrust" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                    <!-- <li><a href="#"><i class="fa fa-google-plus"></i></a></li> -->
                                </ul> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--Bottom-->
        <div class="footer-bottom">
            <div class="auto-container">
                <div class="copyright">Copyright <a href="index.php">Soumik Charitable Trust</a> © 2018. All Rights Reserved.</div>
            </div>
        <!--    <div class="row">-->
									
								<!--	<div class="col-md-6">-->
								<!--	             <p class="text-left">Designed and Developed by <a href="http://cenithub.com" target="_blank">Cenit It Hub Private Limited</a></p>-->
									    
								<!--	</div>-->
								<!--	<div class="col-md-6">-->
								<!--	     <p class="text-right">Copyright <a href="index.php">Soumik Charitable Trust</a> © 2018. All Rights Reserved.</p>   -->
								<!--	</div>-->
									
								<!--</div>-->
        </div>
    </footer>
    <!-- End Footer -->
    
   
</div>
<!--End pagewrapper-->

<!--Scroll to top-->
 <div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-angle-double-up"></span></div> 
<script src="js/jquery.js?soumikversion=1.1"></script>
<script src="js/bootstrap.min.js?soumikversion=1.1"></script>
<script src="js/jquery-ui.js?soumikversion=1.1"></script>
<script src="js/jquery.fancybox.js?soumikversion=1.1"></script>
<script src="js/owl.js?soumikversion=1.1"></script>
<script src="js/wow.js?soumikversion=1.1"></script>
<script src="js/appear.js?soumikversion=1.1"></script>
<script src="js/script.js?soumikversion=1.1"></script>
</body>
</html>



    

<script type="text/javascript">
    $('.subscribe').on('click',function()
    {

         var subscribeemail=$('#subscribeemail').val();
          var atpos = subscribeemail.indexOf("@");
        var dotpos = subscribeemail.lastIndexOf(".");

        // alert(subscribeemail);

         if(subscribeemail.length==0)
         {
            //alert('Enter EmailId');
            $('.sub_error_msg').css('color','red').fadeIn().html("<i class='fa fa-times'></i> <b>Enter EmailId!</b>");
         }
          else if (atpos<1 || dotpos<atpos+2 || dotpos+2>=subscribeemail.length)
                          {
                          
                          //alert('Enter Valid EmailId');
                           $('.sub_error_msg').css('color','red').fadeIn().html("<i class='fa fa-times'></i> <b>Enter Valid EmailId!</b>");
                                 
                           }
                           else
                           {
                               $('.validatiin_opopip').show();
                                    $('.popup_overlay').show();
                                $('#popupquestion').html('<b class="text-center">Successfully Subscribed Your Details</b>');

                $.ajax({
                              type:"post",
                              url:"subscribeemail.php",
                              data:{subscribeemail:subscribeemail},
                              success:function(data){
                                var jsondata=JSON.parse(data);
                                if(jsondata.status==1)
                                {
                           
                                // alert("successfully subscribed");
                                
                               location.reload();
                                 


                                }
                              
                                else
                                {
                                  alert("Failed To Subscribe");
                                }
                            }
                 });
            }

    });
</script>
<script>
 $('.conformation_msg_close_btn ,.conformation_cncl').on('click',function(){
            $('.validatiin_opopip').hide();
            $('.popup_overlay').hide();
        });
    </script>
</script>
<?php include('addons/header.php'); ?>

        <!-- Sticky Header -->
        <div class="sticky-header">
            <div class="auto-container clearfix">
                <!--Logo-->
                <div class="logo pull-left">
                    <a href="index.php" title=""><img src="images/logo-small.png" alt="" title=""></a>
                </div>
                <!--Right Col-->
                <div class="pull-right">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li><a href="index.php">Home</a></li>
								<li><a href="about-us.php">About us</a></li>
                                <li class="dropdown"><a href="services.php" >Services</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="support-for-education.php"> Education </a></li>    
                                        <li><a href="support-for-health.php"> Health </a></li>
                                        <li><a href="helping-poor-people.php"> Helping People </a></li>
                                    </ul>
                                </li>
                                <li><a href="causes-list.php">Causes</a></li> 
                                <li><a href="event-list.php">Events</a></li>                              
                                <li><a href="contact-us.php">Contact Us</a></li>
                                <?php
                                if(isset($_SESSION['username'])=='')
                                {
                                ?>
                                <li><a href="signin-registration.php">Login</a></li>
                                <?php
                            }
                            else
                            {
                            ?>

                                <li class="dropdown"><a href="services.php" ><?php echo $_SESSION['fullname'];?></a>
                                    <ul class="dropdown-menu">
                                       
                                        <li><a href="profile.php"> My Profile </a></li>
                                        <li><a href="logout.php"> Logout </a></li>
                                    </ul>
                                </li>
                                <?php
                            }
                            ?>
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>
            </div>
        </div>
    </header>
    <!--End Main Header -->

    <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/causes-bg.jpg);">
        <div class="auto-container">
            <h1>Causes </h1>
            <ul class="bread-crumb clearfix">
                <li><a href="index.php">Home </a></li>
                <li>Causes List</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
 
    <!-- Causes Section -->
    <section class="causes-section">
        <div class="auto-container">
            <div class="causes-list">
                <div class="row clearfix">
                    <!-- Cause Block -->
                <?php
                 $query13 = "SELECT * FROM home_causes" ;
                $result13=mysqli_query($con,$query13);
                $i=1;
       
               while($data = mysqli_fetch_assoc($result13))
               {
                ?>
                    <div class="cause-block col-md-3 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image-box-cause">
                            <figure><img src="images/resource/<?php echo $data['cause_image']; ?>" width="363px !important" height="363px !important" alt=""></figure>
                            <div class="overlay-box"><a href="payu-confirm.php" class="link">Donate Now &gt;</a></div>
                        </div>
                        <div class="lower-content">
                            <h2><a href="causes-single.php?sl_no=<?php echo $data['slno']?>"><?php echo $data['cause_title']; ?></a></h2>
                            <p><?php echo $data['cause_desc']; ?></p>
                            
                            <div class="progress-bar">
                                <div class="bar-inner"><div class="bar progress-line" data-width="0"><div class="bar-percentage"><div class="count-box"><span class="count-text" data-speed="2000" data-stop="0">0</span>%</div></div></div></div>
                            </div>
                                
                            <div class="info-box clearfix">
                                <a class="raised">Raised: <span><i class="fa fa-rupee"></i> 0</span></a>
                                <a class="goal">Goal: <span><i class="fa fa-rupee"></i> 0</span></a>
                            </div>  
                        </div>
                    </div>
                </div>
                     <?php
            }
            ?>


                    
                </div>
            </div>
        	<!-- <div class="styled-pagination text-center">
                <ul class="clearfix">
                    <li class="prev"><a href="#" >Prev</a></li>
                    <li><a href="#"  class="active">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li class="next"><a href="#">Next</a></li>
                </ul>
            </div> -->
        </div>
    </section>
    <!-- End Causes Section -->

    

   <?php include('addons/footer.php'); ?>
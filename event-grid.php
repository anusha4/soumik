<?php include('addons/header.php'); ?>

        <!-- Sticky Header -->
        <div class="sticky-header">
            <div class="auto-container clearfix">
                <!--Logo-->
                <div class="logo pull-left">
                    <a href="index.php" title=""><img src="images/logo-small.png" alt="" title=""></a>
                </div>
                <!--Right Col-->
                <div class="pull-right">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                         <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="about-us.php">About us</a></li>
                                <li class="dropdown"><a href="services.php" >Services</a>
                                	<ul class="dropdown-menu">
                                		<li><a href="support-for-education.php"> Education </a></li>	
                                		<li><a href="support-for-health.php"> Health </a></li>
                                		<li><a href="helping-poor-people.php"> Helping People </a></li>
                                	</ul>
                                </li>
                                <li><a href="causes-list.php">Causes</a></li> 
                                <li><a href="event-list.php">Events</a></li>                              
                                <li><a href="contact-us.php">Contact Us</a></li>
                                <li><a href="signin-registration.php">Login</a></li>
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>
            </div>
        </div>
    </header>
    <!--End Main Header -->

    <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/event-grid-bg.jpg);">
        <div class="auto-container">
            <h1>Event Grid View</h1>
            <ul class="bread-crumb clearfix">
                <li><a href="index.php">Home </a></li>
                <li>Event Grid View</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->

    <!-- Sidebar Page Container -->
    <div class="sidebar-page-container">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Content Side-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="event-grid">
                        <div class="row clearfix">
                            <!-- Event Block -->
                            <div class="event-block-two col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box">
                                    <div class="image-box">
                                        <div class="date"><span>04</span> April</div>
                                        <a href="event-single.php"><img src="images/resource/event-grid-1.jpg" alt=""></a>
                                    </div>
                                    <div class="content-box">
                                        <h4><a href="event-single.php">Gear up for giving</a></h4>
                                        <ul class="info">
                                            <li><a ><i class="fa fa-clock-o"></i>at 5.00 pm - 7.30 pm</a></li>
                                            <li><a ><i class="fa fa-map-marker"></i>25 Newyork City</a></li>
                                        </ul>
                                        <p>Bring to the table win-win survival strategies to ensure proactive domination. Capitalize on low hanging fruittionPodcasting operational change management inside of workflows to establish a framework. Taking seamless key</p>
                                    </div>
                                </div>
                            </div>

                            <!-- Event Block -->
                            <div class="event-block-two col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box">
                                    <div class="image-box">
                                        <div class="date"><span>05</span> April</div>
                                        <a href="event-single.php"><img src="images/resource/event-grid-2.jpg" alt=""></a>
                                    </div>
                                    <div class="content-box">
                                        <h4><a href="event-single.php">Sponcer a child today</a></h4>
                                        <ul class="info">
                                            <li><a><i class="fa fa-clock-o"></i>at 5.00 pm - 7.30 pm</a></li>
                                            <li><a><i class="fa fa-map-marker"></i>25 Newyork City</a></li>
                                        </ul>
                                        <p>Bring to the table win-win survival strategies to ensure proactive domination. Capitalize on low hanging fruittionPodcasting operational change management inside of workflows to establish a framework. Taking seamless key</p>
                                    </div>
                                </div>
                            </div>

                            <!-- Event Block -->
                            <div class="event-block-two col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box">
                                    <div class="image-box">
                                        <div class="date"><span>06</span> April</div>
                                        <a href="event-single.php"><img src="images/resource/event-grid-3.jpg" alt=""></a>
                                    </div>
                                    <div class="content-box">
                                        <h4><a href="event-single.php">Clothes Coming</a></h4>
                                        <ul class="info">
                                            <li><a><i class="fa fa-clock-o"></i>at 5.00 pm - 7.30 pm</a></li>
                                            <li><a><i class="fa fa-map-marker"></i>25 Newyork City</a></li>
                                        </ul>
                                        <p>Bring to the table win-win survival strategies to ensure proactive domination. Capitalize on low hanging fruittionPodcasting operational change management inside of workflows to establish a framework. Taking seamless key</p>
                                    </div>
                                </div>
                            </div>

                            <!-- Event Block -->
                            <div class="event-block-two col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box">
                                    <div class="image-box">
                                        <div class="date"><span>07</span> April</div>
                                        <a href="event-single.php"><img src="images/resource/event-grid-4.jpg" alt=""></a>
                                    </div>
                                    <div class="content-box">
                                        <h4><a href="event-single.php">Teachers Presentation</a></h4>
                                        <ul class="info">
                                            <li><a><i class="fa fa-clock-o"></i>at 5.00 pm - 7.30 pm</a></li>
                                            <li><a><i class="fa fa-map-marker"></i>25 Newyork City</a></li>
                                        </ul>
                                        <p>Bring to the table win-win survival strategies to ensure proactive domination. Capitalize on low hanging fruittionPodcasting operational change management inside of workflows to establish a framework. Taking seamless key</p>
                                    </div>
                                </div>
                            </div>

                            <!-- Event Block -->
                            <!-- <div class="event-block-two col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box">
                                    <div class="image-box">
                                        <div class="date"><span>08</span> April</div>
                                        <a href="event-single.php"><img src="images/resource/event-grid-5.jpg" alt=""></a>
                                    </div>
                                    <div class="content-box">
                                        <h4><a href="event-single.php">Collecting poor childrens</a></h4>
                                        <ul class="info">
                                            <li><a href="event-single.php"><i class="fa fa-clock-o"></i>at 5.00 pm - 7.30 pm</a></li>
                                            <li><a href="event-single.php"><i class="fa fa-map-marker"></i>25 Newyork City</a></li>
                                        </ul>
                                        <p>Bring to the table win-win survival strategies to ensure proactive domination. Capitalize on low hanging fruittionPodcasting operational change management inside of workflows to establish a framework. Taking seamless key</p>
                                    </div>
                                </div>
                            </div> -->

                            <!-- Event Block -->
                            <!-- <div class="event-block-two col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box">
                                    <div class="image-box">
                                        <div class="date"><span>09</span> April</div>
                                        <a href="event-single.php"><img src="images/resource/event-grid-6.jpg" alt=""></a>
                                    </div>
                                    <div class="content-box">
                                        <h4><a href="event-single.php">Drought Information</a></h4>
                                        <ul class="info">
                                            <li><a href="event-single.php"><i class="fa fa-clock-o"></i>at 5.00 pm - 7.30 pm</a></li>
                                            <li><a href="event-single.php"><i class="fa fa-map-marker"></i>25 Newyork City</a></li>
                                        </ul>
                                        <p>Bring to the table win-win survival strategies to ensure proactive domination. Capitalize on low hanging fruittionPodcasting operational change management inside of workflows to establish a framework. Taking seamless key</p>
                                    </div>
                                </div>
                            </div> -->
                        </div>

                        <!-- Styled Pagination -->
                        <!-- <div class="styled-pagination">
                            <ul class="clearfix">
                                <li class="prev"><a href="#">Prev</a></li>
                                <li><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#" class="active">3</a></li>
                                <li class="next"><a href="#">Next</a></li>
                            </ul>
                        </div> -->
                    </div>
                </div>

                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <aside class="sidebar">
                        <!-- Search Box -->
                        <div class="sidebar-widget search-box">
                            <div class="widget-content">
                                <div class="sidebar-title"><h3>Search</h3></div>
                                <form method="post" action="blog.php">
                                    <div class="form-group">
                                        <input type="search" name="search-field" value="" placeholder="Enter Your Keyword" required="">
                                        <button type="submit" class="theme-btn btn-style-one">Search</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <!-- Post Widget -->
                        <div class="sidebar-widget post-widget">
                            <div class="sidebar-title"><h3>Past Event</h3></div>
                            <div class="widget-content">
                                <article class="post">
                                    <div class="thumb"><img src="images/resource/post-thumb-1.jpg" alt=""></div>
                                    <h3>Providing drinking water for charity</h3>
                                    <div class="date">08:30 am / Apr 14, 2018</div>
                                </article>

                                <article class="post">
                                    <div class="thumb"><img src="images/resource/post-thumb-2.jpg" alt=""></div>
                                    <h3>The childrents family <br> trust</h3>
                                    <div class="date">08:30 am / Apr 14, 2018</div>
                                </article>

                                <article class="post">
                                    <div class="thumb"><a href="event-single.php"><img src="images/resource/post-thumb-3.jpg" alt=""></a></div>
                                    <h3>Famous qutes about giving</h3>
                                    <div class="date">08:30 am / Apr 14, 2018</div>
                                </article>

                                <article class="post">
                                    <div class="thumb"><img src="images/resource/post-thumb-4.jpg" alt=""></div>
                                    <h3>Providing drinking water for charity</h3>
                                    <div class="date">08:30 am / Apr 14, 2018</div>
                                </article>
                            </div>
                        </div>

                        <!-- Testimonial Widget -->
                        <!-- <div class="sidebar-widget qutoe-widget">
                            <div class="widget-content">
                                <blockquote>
                                    <span class="icon flaticon-right-quotation-sign"></span>
                                    Collaboratively administrate empowered markets via networks. ize customer directed convergence without revolutionary ROI.
                                    <cite>-Nattasha</cite>
                                </blockquote>
                            </div>
                        </div> -->

                     
                    </aside>
                </div>
            </div>
        </div>
    </div>  
    <!-- End Sidebar Page Container -->

    

    <?php include('addons/footer.php'); ?>
<?php include('addons/header.php'); ?>
<?php
include("constants.php");
include("./api/db.php");
$result = array();

if (isset($_GET['slideid'])) {
//     //echo $_GET['ulb'];

     $id = $_GET['slideid'];

    

    $query = mysqli_query($con,"SELECT * FROM home_contactus WHERE contact_id = '$id'");

   $data = mysqli_fetch_assoc($query);

  
}

?>


</head>

<body>

	<!-- Main navbar -->
	<?php include('addons/navbar.php'); ?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<?php include('addons/left_side_bar.php'); ?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><span class="text-semibold"></span>Contact-Us Info</h4>
						</div>
						<div class="heading-elements">
						<a href="contact-us.php"><button type="button" class="btn border-slates bg-coral btn-flats"><i class="fas fa-arrow-left"></i> Back</button></a>
						</div>
					</div>
				</div>
				<!-- Content area -->
				<div class="content">

					<!-- Main charts -->
					
					<!-- /main charts -->


					<!-- Dashboard content -->
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12">

						
							<div class="panel panel-flat">
								<div class="panel-heading">
									<!-- <h3 class="panel-title">Donation Info</h3> -->
									<div class="heading-elements">
										<ul class="icons-list">
											
					                	</ul>
				                	</div>
			                	</div>
			                	<div class="panel-body">
									<div class="row add_user_form">
									<div class="col-lg-2 col-md-2 col-sm-2"></div>
									<div class="col-lg-8 col-md-8 col-sm-8">
			                		<div class="form-group">
										<label class="control-label col-lg-3"> <b>Name :</b></label>
										<div class="col-lg-9">
											<?php echo $data['contact_name']; ?>
										</div>
										<div class="clear"></div>
										</div>

										<div class="form-group">
										<label class="control-label col-lg-3"><b> Email :</b></label>
											<div class="col-lg-9">
											<?php echo $data['contact_email']; ?>
										</div>
										<div class="clear"></div>
										</div>
										<div class="form-group">
										<label class="control-label col-lg-3"><b> Mobile :</b></label>
											<div class="col-lg-9">
											<?php echo $data['contact_mobileno']; ?>
										</div>
										<div class="clear"></div>
										</div>
										<div class="form-group">
										<label class="control-label col-lg-3"><b> Message :</b></label>
											<div class="col-lg-9">
											<?php echo $data['contact_message']; ?>
										</div>
										<div class="clear"></div>
										</div>
										<div class="form-group">
										<label class="control-label col-lg-3"><b> Addedon :</b></label>
											<div class="col-lg-9">
											<?php echo date("m-d-Y h:i A", strtotime($data['contact_addedon']));  ?>
										</div>
										<div class="clear"></div>
										</div>
									
								
								</div>
							</div>
						</div>
					</div>
				</div>
							<!-- /latest posts -->

						</div>
					<!-- /dashboard content -->
					<!-- Footer -->
					<?php include("addons/footer.php") ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>

<?php include('addons/header.php'); ?>

</head>

<body>
	<!-- Main navbar -->
	<?php include('addons/navbar.php'); ?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			
			<!-- /main sidebar -->
			<?php include('addons/left_side_bar.php'); ?>

			<!-- Main content -->
			<div class="content-wrapper">
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><span class="text-semibold"></span>CONTACT-US</h4>
						</div>
					</div>
				</div>
				<!-- Content area -->
				<div class="content">

					<!-- Main charts -->
					
					<!-- /main charts -->


					<!-- Dashboard content -->
					<div class="row">
						<div class="col-lg-12">

						
							<div class="panel panel-flat">
								

			                	<div class="clear"></div>
								<div class="panel-body">
									<div class="row table-responsive" id="contacts_table">
									
									</div>
								</div>
							</div>
							<!-- /latest posts -->

						</div>

						
					</div>
					<!-- /dashboard content -->


					<!-- Footer -->
						<?php include("addons/footer.php") ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->


<script type="text/javascript">
	
	 $(document).ready(function() {

	 	    $.ajax({    //create an ajax request to load_page.php
        type: "POST",
        url: "api/getcontact.php", 
        //data: {readpro : 1} ,
        // dataType: 'json' ,         
          //expect html to be returned                
        success: function(data){  

            console.log(data);
            var contact = JSON.parse(data);
            var contact_data = contact.contact_data;
            var str='<table id="contact_tables" class="table  table-bordered"width="100%"><thead><tr><th class="th-sm">SNo</th><th >Name</th><th> EmailId</th><th> MobileNo</th><th> Message</th><th >UserAddedon</th><th class="text-center">Actions</th></tr></thead><tbody class="list_ads">'
            // var str='<table class="table datatable-basic" id="user_table"><thead><tr><th>SNo</th><th>Slide Name</th><th>Slide Title</th><th>Image</th><th class="text-center">Actions</th></tr></thead><tbody>';

            for(var i=0; i<contact_data.length; i++ ){

              	var j =i+1;
              	str+="<tr><td>" + j + "</td>" +
              		"<td>" + contact_data[i].name + "</td>" +
              		"<td>" + contact_data[i].email + "</td>" +  
              		"<td>" + contact_data[i].mobileno + "</td>" + 
              		"<td>" + contact_data[i].message + "</td>" + 
              		"<td>" + contact_data[i].addedon + "</td>" + 
              		"<td style='white-space:nowrap'>" + "<a href='contact_details.php?slideid=" + contact_data[i].id + "''><i  class='table_action bg-orange fa fa-eye' title='view'></i></a> "+"</td>"
              	    	



              	
              		// "<td>" + "<button class='view_btn'>View </button> <button class='edit_btn'>Edit<button> <button class='delete_btn'>Delete </button>" + "</td>"
              	}

              	$("#contacts_table").html(str+"</tbody></table>");
				 $('#contact_tables').dataTable({
      			//paging: false
    		});
    		}
				});
	 	   });

            </script>

            
</body>
</html>
<script type="text/javascript">
	$(document).ready(function () {
  $('#contact_tables').DataTable();
  $('.dataTables_length').addClass('bs-select');
});
	$(document).ready(function () {
  $('#contact_tables').DataTable({
    "paging": false // false to disable pagination (or any other option)
  });
  $('.dataTables_length').addClass('bs-select');
});
	// Basic example
$(document).ready(function () {
  $('#contact_tables').DataTable({
    "pagingType": "simple" // "simple" option for 'Previous' and 'Next' buttons only
  });
  $('.dataTables_length').addClass('bs-select');
});
</script>

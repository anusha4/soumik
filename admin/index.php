<?php include("constants.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Admin panel</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="assets/css/core.css" rel="stylesheet" type="text/css">
	<link href="assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="assets/css/colors.css" rel="stylesheet" type="text/css">
	<link href="assets/css/extrastyles.css" rel="stylesheet" type="text/css">
	<link rel="icon" href="assets/images/favicon.png" type="image/x-icon">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->


	<!-- Theme JS files -->
	<script type="text/javascript" src="assets/js/core/app.js"></script>
	<!-- /theme JS files -->

</head>

<body>

	<!-- Main navbar -->
	<div class="navbar navbar-inverse">
		<div class="navbar-header">
			<!-- <a class="navbar-brand" href="index.html"><img src="assets/images/logo_light.png" alt=""></a> -->

			<ul class="nav navbar-nav pull-right visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav navbar-right">
				<!-- <li>
					<a href="#">
						<i class="icon-display4"></i> <span class="visible-xs-inline-block position-right"> Go to website</span>
					</a>
				</li>

				<li>
					<a href="#">
						<i class="icon-user-tie"></i> <span class="visible-xs-inline-block position-right"> Contact admin</span>
					</a>
				</li>

				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-cog3"></i>
						<span class="visible-xs-inline-block position-right"> Options</span>
					</a>
				</li> -->
			</ul>
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container login-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

					<!-- Simple login form -->
					<!-- <form action="index.html"> -->
						<div class="col-md-4 col-md-offset-4 panel panel-body login-form">
							<div class="text-center">
								<div class="icon-object"><img src="../images/logo-2.png" height="80px" width="80px"></div>
								<h5 class="content-group"><b>Login to your account </b><small class="display-block">Enter your credentials below</small></h5>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								<input type="text" class="form-control" id="ven_uname" placeholder="Username">
								<div class="form-control-feedback">
									<i class="icon-user text-muted"></i>
								</div>
								<span class="errormsg" style="display:none">Enter Valid UserName</span>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								<input type="password" id="ven_pass" class="form-control" placeholder="Password">
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
								</div>
								<span class="errormsg" style="display:none">Enter Valid Password</span>

								<span class="successmessage"></span>
								<span class="errormessage"></span>
							</div>

							<div class="form-group">
								<button type="submit" id="login_vendor" style="float: right;background:linear-gradient(45deg, #ea7325 1%, #fcc005 100%);" class="btn btn-primary">Sign in <i class="icon-circle-right2 position-right"></i></button>
							</div>

							<div class="text-center">
								<!-- <a href="login_password_recover.html">Forgot password?</a> -->
							</div>
						</div>
					<!-- </form> -->
					<!-- /simple login form -->


					<!-- Footer -->
					<!-- <div class="footer text-muted">
						&copy; 2017. <a href="#">Petdeal</a> by <a href="http://www.logicprog.com" target="_blank">LogicProg</a>
					</div> -->
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
	<script type="text/javascript">

	$('#login_vendor').on('click',function(){
	$('.errormsg').hide();
	var uname = $('#ven_uname').val();
	var pass = $('#ven_pass').val();
	var url = '<?php echo $base_url; ?>';

		if (uname.length == 0)
              {  

	             $('#ven_uname').siblings('.errormsg').show();
	              flag =true;   
              }
  
            else if (pass.length==0)
              {  
	             $('#ven_pass').siblings('.errormsg').show();
	              flag =true;   
              }
              else{

	 $.ajax({
                url:'api/login_check.php',
                //dataType: 'html',
                type:'POST',
                //dataType: 'json',
                data:{uname:uname,pass:pass},
           
            success:function(data){

               var jsondata=JSON.parse(data);

                     if(jsondata.status == 1)
                     {
                     	$('.successmessage').css('color','green').html('<b>Successfully Login Redirecting..</b>');
                        	$('.errormessage').hide();
									  		  setTimeout(function(){
									  		  	window.location="user_table.php";


									  		}, 1000);
                     	
                     }

                     else
                     {
                     	$('.errormessage').css('color','red').html('<b>Username and Password are Incorrect</b>');

									  		 
                 	} 	


                 	} 

               });
	}
	 	});
   </script>

</body>
</html>
<?php 
	
 ?>
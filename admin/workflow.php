<?php include('addons/header.php'); ?>

</head>

<body>

	<!-- Main navbar -->
	<?php include('addons/navbar.php'); ?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			
			<!-- /main sidebar -->
			<?php include('addons/left_side_bar.php'); ?>

			<!-- Main content -->
			<div class="content-wrapper">
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><span class="text-semibold"></span>Who We Are</h4>
						</div>
						
					</div>
				</div>
				<!-- Content area -->
				<div class="content">

					<!-- Main charts -->
					
					<!-- /main charts -->


					<!-- Dashboard content -->
					<div class="row">
						<div class="col-lg-12">

						
							<div class="panel panel-flat">
								<div class="panel-heading">
									<!-- <h3 class="panel-title">BANNERS</h3> -->
									<div class="heading-elements">
										<ul class="icons-list">
					                		<!-- <li><a data-action="collapse"></a></li>
					                		<li><a data-action="reload"></a></li> -->
					                	</ul>
				                	</div>
			                	</div>
			                	<!-- <div class="col-lg-12 col-md-12 text-right"><a href="addbanner.php"><button type="button" class="btn border-slate text-slate-800 btn-flat"><i class="icon-plus3 position-left"></i> Add</button></a></div> -->
			                	<div class="clear"></div>
								<div class="panel-body">
									<div class="display row table-responsive" id="user_table_section">
										
										
									</div>
								</div>
							</div>
							<!-- /latest posts -->

						</div>

						
					</div>
					<!-- /dashboard content -->


					<!-- Footer -->
					<?php include("addons/footer.php") ?>
					<!-- /footer -->
					

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
<div class="modal fade" id="deleteWorkflowModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	 <div class="modal-dialog" role="document">
			<div class="modal-content">
				
					<div class="modal-header">						
						<h4 class="modal-title">Delete Who We Are</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <i class="fa fa-times-circle"></i><span aria-hidden="true"></span>
				        </button>
					</div>
					<div class="modal-body">					
						<p >Are you sure you want to delete <b class="text-warning "s><span id="get_name_here"></span>?</b></p>
						<!-- <p ></p> -->
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
						<input type="submit" class="btn btn-danger" data-dismiss="modal" value="Delete" onclick="deleteworkflow()">
					</div>
				
			</div>
		</div>
	</div>
	<script type="text/javascript">
	
	 $(document).ready(function() {

	 	    $.ajax({    //create an ajax request to load_page.php
        type: "POST",
        url: "api/getworkflow.php", 
        //data: {readpro : 1} ,
        // dataType: 'json' ,         
          //expect html to be returned                
        success: function(data){  

            console.log(data);
             dataa=JSON.parse(data).workflow;
            var we_are = JSON.parse(data);
            var workflow = we_are.workflow;

            var str='<table class="table table-bordered" id="user_table" ><thead><tr><th>SNo</th><th>Title</th><th>Description</th><th>Image</th><th class="text-center">Actions</th></tr></thead><tbody class="list_ads">';

            for(var i=0; i<workflow.length; i++ ){

              	var j =i+1;
              	str+="<tr><td>" + j + "</td>" +
              		// "<td>" + workflow[i].name + "</td>" +
              		"<td>" + workflow[i].workflow_title + "</td>" +  
              		"<td>" + workflow[i].workflow_description + "</td>" + 
              		"<td>" + workflow[i].workflow_backgroundimage + "</td>" + 
              		

              		

              		"<td style='white-space: nowrap;'>" + "<a href='workflow_details.php?slideid=" + workflow[i].workflow_id + "''><i  class='table_action bg-orange fa fa-eye' title='view'></i></a> "+
              		 "<a href='edit_workflow.php?slideid=" + workflow[i].workflow_id + "''><i class='table_action bg-blue fa fa-edit' title='Edit'></i></a> "+"<a href='#deleteWorkflowModal' class='delete' data-toggle='modal'><i class='table_action bg-danger fa fa-trash deleteworkflow' data-id="+workflow[i].workflow_id+" title='Delete' onclick='deleteWorkflow("+workflow[i].workflow_id+")'></i></a>"+"</td>"

              		// "<td>" + "<button class='view_btn'>View </button> <button class='edit_btn'>Edit<button> <button class='delete_btn'>Delete </button>" + "</td>"
              	}

              	$("#user_table_section").html(str+"</tbody></table>");
				 $('#user_table').dataTable({
      			//paging: false
    		});
    		}
				});
	 	   });

            </script>

            <script>
document.getElementById('products').classList.add('active'); //add
</script>

</body>
</html>
<script type="text/javascript">
	function deleteWorkflow(delid){
		
		console.log("data is ",dataa);
		for(i=0;i<dataa.length;i++){
			if(dataa[i].workflow_id==delid){
				 // reqNameId=dataa[i].page;
				 reqNameId=document.getElementById("get_name_here").innerText=dataa[i].workflow_title;
				 reqSeoId=dataa[i].workflow_id;
				console.log("the page name is ",dataa[i].workflow_title)
			}
		}
	}
	function deleteworkflow(){
		console.log("delete is ",reqSeoId);
		window.location.href='api/deleteworkflow.php?del_id=' +reqSeoId+'';
		
	}
</script>
<!-- <script type="text/javascript">
	
	$(document).on('click', '.deleteslider', function(){

		//$(this).parent().parent().parent().remove();
		 var el = $(this);
		
		 var bannerid=$(this).data('id');
		 $.ajax({    //create an ajax request to load_page.php
        type: "POST",
        url: "api/deleteworkflow.php",
        data:{bannerid:bannerid},
                     
        success: function(data){ 

         var jsondata = JSON.parse(data);
         if(jsondata.status==1)
         {
         	alert("Successfully Deleted");
         	// location.reload();
         	el.parent().parent().parent().remove();
         
         }
         else
         {
         	alert("Failure");
         }

        }
        }); 
		
	});
</script> -->

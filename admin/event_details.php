<?php include('addons/header.php'); ?>
<?php
include("constants.php");
include("./api/db.php");
$result = array();

if (isset($_GET['causeid'])) {
//     //echo $_GET['ulb'];

     $id = $_GET['causeid'];

    

    $query = mysqli_query($con,"SELECT * FROM home_events WHERE events_id = '$id'");

   $data = mysqli_fetch_assoc($query);

  


    


   	// echo "ID: ".$data['id']."<br>";
    // echo "FULL NAME: ".$data['fname'] ." ". $data['lname']."<br>";
    // echo "MOBILE: ".$data['mobile']."<br>";
    // echo "E-MAIL: ".$data['email']."<br>";
    // if($data['status']==0)
    // 	$status = "Un Verified";
    // if($data['status']==1)
    // 	$status = "Verified";

    // echo "STATUS: ".$status;
}

?>


</head>

<body>

	<!-- Main navbar -->
    <?php include('addons/navbar.php'); ?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<?php include('addons/left_side_bar.php'); ?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><span class="text-semibold"></span>Event InFo</h4>
						</div>
						<div class="heading-elements">
						<a href="events.php"><button type="button" class="btn border-slates bg-coral btn-flats"><i class="fas fa-arrow-left"></i> Back</button></a>
						</div>
					</div>
				</div>
				<!-- Content area -->
				<div class="content">

					<!-- Main charts -->
					
					<!-- /main charts -->


					<!-- Dashboard content -->
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12">

						
							<div class="panel panel-flat">
								<div class="panel-heading">
									<!-- <h3 class="panel-title">Event Info</h3> -->
									<div class="heading-elements">
										<ul class="icons-list">
											<!-- <a href="events.php"><button type="button" class="btn border-slates text-slate-800 btn-flats">Back</button></a> -->
					                		<!-- <li><a data-action="collapse"></a></li>
					                		<li><a data-action="reload"></a></li> -->
					                	</ul>
				                	</div>
			                	</div>
			                	<div class="panel-body">
									<div class="row add_user_form">
									<div class="col-lg-2 col-md-2 col-sm-2"></div>
									<div class="col-lg-8 col-md-8 col-sm-8">
			                	<div class="form-group">
										<label class="control-label col-lg-3"><b> Title :</b></label>
										<div class="col-lg-9">
											<?php echo $data['events_name']; ?>
										</div>
										<div class="clear"></div>
										</div>

									<div class="form-group">
										<label class="control-label col-lg-3"><b> Description :</b></label>
											<div class="col-lg-9">
											<?php echo $data['events_description']; ?>
										</div>
										<div class="clear"></div>
									</div>
									<div class="form-group">
										<label class="control-label col-lg-3"><b> City :</b></label>
											<div class="col-lg-9">
											<?php echo $data['events_city']; ?>
										</div>
										<div class="clear"></div>
									</div>
									<div class="form-group">
										<label class="control-label col-lg-3"><b> Date :</b></label>
											<div class="col-lg-9">
											<?php echo $data['events_date']; ?>
										</div>
										<div class="clear"></div>
									</div>
									<div class="form-group">
										<label class="control-label col-lg-3"><b> FromTime :</b></label>
											<div class="col-lg-9">
											<?php echo $data['events_fromtime']; ?>
										</div>
										<div class="clear"></div>
									</div>
			                	
			                	<div class="form-group">
										<label class="control-label col-lg-3"><b> ToTime :</b></label>
											<div class="col-lg-9">
											<?php echo $data['events_totime']; ?>
										</div>
										<div class="clear"></div>
									</div>
			                	
								<div class="form-group">
									<label class="control-label col-lg-3"><b> Image :</b></label>
									<div class="col-lg-9">
										<?php
										if($data['events_image']!='')
										{

										?>
											<img src="../images/resource/<?php echo $data['events_image']; ?>" class="profile_img" style="border-radius: 0%">
											<?php
										}
										else
										{
										?>
										<img src="../images/resource/avatar-img.png" width="200px" height="220px" id="profile_pic" title="no image"><br><br>

										<?php
									}
									?>
										</div>
										
										<div class="clear"></div>
									</div>
								
								</div>
							</div>
						</div>
					</div>
				</div>
							<!-- /latest posts -->

						</div>
					<!-- /dashboard content -->
					<!-- Footer -->
					<?php include("addons/footer.php") ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>

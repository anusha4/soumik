<?php include('addons/header.php'); ?>

</head>

<body>
	<!-- Main navbar -->
	<?php include('addons/navbar.php'); ?>
	<!-- /main navbar -->
	

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			
			<!-- /main sidebar -->
			<?php include('addons/left_side_bar.php'); ?>

			<!-- Main content -->
			<div class="content-wrapper">
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><span class="text-semibold"></span>USERS</h4>
						</div>
					</div>
				</div>
				<!-- Content area -->
				<div class="content">

					<!-- Main charts -->
					
					<!-- /main charts -->


					<!-- Dashboard content -->
					<div class="row">
						<div class="col-lg-12">

						
							<div class="panel panel-flat">
								<div class="panel-heading">
									<div class="col-md-2 "style="float: right;">
								<p>User type:</p>
								<select class="form-control" id="usertype">
									<option value="all">All</option>
									<option value="Donaters">Donaters</option>
									<option value="Volunteers">Volunteers</option>
									<option value="Others">Others</option>
								</select>
							</div>
			                	<div class="col-lg-10 col-md-12 text-right"><a href="adduser.php"><button type="button" class="btn border-slate text-slate-800 add_btn btn-flat"><i class="fa fa-plus"> Add</i></button></a></div>
									
			                	</div>

			                	<div class="clear"></div>
								<div class="panel-body">
									<div class="row table-responsive" id="users_table">
									
									</div>
								</div>
							</div>
							<!-- /latest posts -->

						</div>

						
					</div>
					<!-- /dashboard content -->


					<!-- Footer -->
						<?php include("addons/footer.php") ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
<div class="modal fade" id="deleteUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	 <div class="modal-dialog" role="document">
			<div class="modal-content">
				
					<div class="modal-header">						
						<h4 class="modal-title">Delete UserData</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <i class="fa fa-times-circle"></i><span aria-hidden="true"></span>
				        </button>
					</div>
					<div class="modal-body">					
						<p >Are you sure you want to delete these <b class="text-warning "s><span id="get_name_here"></span>?</b></p>
						<!-- <p ></p> -->
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
						<input type="submit" class="btn btn-danger" data-dismiss="modal" value="Delete" onclick="deleteuserdata()">
					</div>
				
			</div>
		</div>
	</div>

<script type="text/javascript">
	
	 $(document).ready(function() {

	 	    $.ajax({    //create an ajax request to load_page.php
        type: "POST",
        url: "api/usersdata.php", 
        //data: {readpro : 1} ,
        // dataType: 'json' ,         
          //expect html to be returned                
        success: function(data){  

            console.log(data);
            dataa=JSON.parse(data).user_data;
            var userex = JSON.parse(data);
            var user_data = userex.user_data;
            var str='<table id="user_tables" class="table  table-bordered"width="100%"><thead><tr><th class="th-sm">SNo</th><th >Name</th><th> EmailId</th><th> MobileNo</th><th> UserType</th><th >UserAddedon</th><th >AddedBy</th><th class="text-center">Actions</th></tr></thead><tbody class="list_ads">'
            // var str='<table class="table datatable-basic" id="user_table"><thead><tr><th>SNo</th><th>Slide Name</th><th>Slide Title</th><th>Image</th><th class="text-center">Actions</th></tr></thead><tbody>';

            for(var i=0; i<user_data.length; i++ ){

              	var j =i+1;
              	str+="<tr><td>" + j + "</td>" +
              		"<td>" + user_data[i].name + "</td>" +
              		"<td>" + user_data[i].email + "</td>" +  
              		"<td>" + user_data[i].mobile + "</td>" + 
              		"<td>" + user_data[i].usertype + "</td>" + 
              		"<td>" + user_data[i].useraddedon + "</td>" + 
              		"<td>" + user_data[i].addedby + "</td>" + 


                   	// "<td style='white-space:nowrap'>" + "<a href='user_details.php?slideid=" + user_data[i].id + "''><i  class='table_action bg-orange fa fa-eye' title='view'></i></a> "+

              	     // 	 "<a href='edit_user.php?slideid=" + user_data[i].id + "''><i class='table_action bg-blue fa fa-edit' title='Edit'></i></a>"+"<a href='#deleteUserModal' class='delete' data-toggle='modal'><i class='table_action bg-danger fa fa-trash deleteuser' data-id="+user_data[i].id+" title='Delete' onclick='deleteuser("+user_data[i].id+")'></i></a>"+"</td>"
              		
              	   
              	    	"<td style='white-space:nowrap'>" + "<a href='user_details.php?slideid=" + user_data[i].id + "''><i  class='table_action bg-orange fa fa-eye' title='view'></i></a> "+

              	     	 
              	     	 "<a href='edit_user.php?slideid=" + user_data[i].id + "''><i class='table_action bg-blue fa fa-edit' title='Edit'></i></a>"



              	     	 +"<a href='#deleteUserModal' class='delete' data-toggle='modal'><i class='table_action bg-danger fa fa-trash deleteuser' data-id="+user_data[i].id+" title='Delete' onclick='deleteuser("+user_data[i].id+")'></i></a>"+"</td>"
              	    	



              	
              		// "<td>" + "<button class='view_btn'>View </button> <button class='edit_btn'>Edit<button> <button class='delete_btn'>Delete </button>" + "</td>"
              	}

              	$("#users_table").html(str+"</tbody></table>");
				 $('#user_tables').dataTable({
      			//paging: false
    		});
    		}
				});
	 	   });

            </script>

            
</body>
</html>
<script type="text/javascript">
	$(document).ready(function () {
  $('#user_tables').DataTable();
  $('.dataTables_length').addClass('bs-select');
});
	$(document).ready(function () {
  $('#user_tables').DataTable({
    "paging": false // false to disable pagination (or any other option)
  });
  $('.dataTables_length').addClass('bs-select');
});
	// Basic example
$(document).ready(function () {
  $('#user_tables').DataTable({
    "pagingType": "simple" // "simple" option for 'Previous' and 'Next' buttons only
  });
  $('.dataTables_length').addClass('bs-select');
});
$('#usertype').on('change',function()
{
//alert('hi');
var usertypedata = $(this).val();
//alert(usertypedata);
$.ajax({    //create an ajax request to load_page.php
        type: "POST",
        url: "api/usersdataonchange.php", 
        data: {usertypedata:usertypedata},
        // dataType: 'json' ,         
          //expect html to be returned                
        success: function(data){  

            console.log(data);
            dataa=JSON.parse(data).user_data;
            var userex = JSON.parse(data);
            var user_data = userex.user_data;
            var str='<table id="user_tables" class="table  table-bordered" width="100%"><thead><tr><th class="th-sm">SNo</th><th >Name</th><th> EmailId</th><th> MobileNo</th><th> UserType</th><th >UserAddedon</th><th >AddedBy</th><th class="text-center">Actions</th></tr></thead><tbody class="list_ads">'
            // var str='<table class="table datatable-basic" id="user_table"><thead><tr><th>SNo</th><th>Slide Name</th><th>Slide Title</th><th>Image</th><th class="text-center">Actions</th></tr></thead><tbody>';

            for(var i=0; i<user_data.length; i++ ){

              	var j =i+1;
              	str+="<tr><td>" + j + "</td>" +
              		"<td>" + user_data[i].name + "</td>" +
              		"<td>" + user_data[i].email + "</td>" +  
              		"<td>" + user_data[i].mobile + "</td>" + 
              		"<td>" + user_data[i].usertype + "</td>" + 
              		"<td>" + user_data[i].useraddedon + "</td>" + 
              		"<td>" + user_data[i].addedby + "</td>" + 
              		

              		
					"<td style='white-space:nowrap'>" + "<a href='user_details.php?slideid=" + user_data[i].id + "''><i  class='table_action bg-orange fa fa-eye' title='view'></i></a> "+
              		 "<a href='edit_user.php?slideid=" + user_data[i].id + "''><i class='table_action bg-blue fa fa-edit' title='Edit'></i></a> "+"<a href='#deleteUserModal' class='delete' data-toggle='modal'><i class='table_action bg-danger fa fa-trash deleteuser' data-id="+user_data[i].id+" title='Delete' onclick='deleteuser("+user_data[i].id+")'></i></a>"+"</td>"

              		// "<td>" + "<button class='view_btn'>View </button> <button class='edit_btn'>Edit<button> <button class='delete_btn'>Delete </button>" + "</td>"
              	}

              	$("#users_table").html(str+"</tbody></table>");
				 $('#user_tables').dataTable({
      			//paging: false
    		});
    		}
				});
});
</script>
<script type="text/javascript">
	function deleteuser(delid){
		
		console.log("data is ",dataa);
		for(i=0;i<dataa.length;i++){
			if(dataa[i].id==delid){
				 // reqNameId=dataa[i].page;
				 reqNameId=document.getElementById("get_name_here").innerText=dataa[i].name;
				 reqSeoId=dataa[i].id;
				console.log("the page name is ",dataa[i].name)
			}
		}
	}
	function deleteuserdata(){
		console.log("delete is ",reqSeoId);
		window.location.href='api/deleteuser.php?del_id=' +reqSeoId+'';
		
	}
</script>
<!-- <script type="text/javascript">
	
	$(document).on('click', '.deleteuser', function(){

		//$(this).parent().parent().parent().remove();
		 var el = $(this);
		
		 var bannerid=$(this).data('id');
		 $.ajax({    //create an ajax request to load_page.php
        type: "POST",
        url: "api/deleteuser.php",
        data:{bannerid:bannerid},
                     
        success: function(data){ 

         var jsondata = JSON.parse(data);
         if(jsondata.status==1)
         {
         	alert("Successfully Deleted");
         	// location.reload();
         	el.parent().parent().parent().remove();
         
         }
         else
         {
         	alert("Failure");
         }

        }
        }); 
		
	});
</script> -->
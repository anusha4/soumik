<?php include('addons/header.php'); ?>
</head>

<body>

	<!-- Main navbar -->
		<?php include('addons/navbar.php'); ?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">
		
		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<?php include('addons/left_side_bar.php'); ?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><span class="text-semibold"></span>Add Users</h4>
						</div>
						<div class="heading-elements">
						<a href="user_table.php"><button type="button" class="btn border-slates bg-coral btn-flats"><i class="fas fa-arrow-left"></i> Back</button></a>
						</div>
					</div>
				</div>
				<!-- Content area -->
				<div class="content">

					<!-- Main charts -->
					
					<!-- /main charts -->


					<!-- Dashboard content -->
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12">

						<!-- <div class="col-lg-12 col-md-12 text-right causes"><a href="causes.php"><button type="button" class="btn border-slates text-slate-800 btn-flats">Back</button></a></div> -->
							<div class="panel panel-flat">
								<div class="panel-heading">
									<!-- <h3 class="panel-title">Add New user</h3> -->
									<div class="heading-elements">
										<ul class="icons-list">
					                		<!-- <li><a data-action="collapse"></a></li>
					                		<li><a data-action="reload"></a></li> -->
					                	</ul>
				                	</div>
			                	</div>

								<div class="panel-body">
									<div class="row add_user_form">
									<div class="col-lg-2 col-md-2 col-sm-2"></div>
									<div class="col-lg-8 col-md-8 col-sm-8">
										<div class="form-group">
										<label class="control-label col-lg-3">Name</label>
										<div class="col-lg-9">
											<input type="text" id="name" class="form-control" placeholder="Enter Name">
											<span class="errormsg" style="display:none">Please Enter Atleast Four Letter Name</span>
										</div>
										<div class="clear"></div>
										</div>


										<div class="form-group">
										<label class="control-label col-lg-3">EmailId</label>
										<div class="col-lg-9">
											<input type="email" id="email" class="form-control" placeholder="Enter Emailid">
											<span class="errormsg" style="display:none">Enter Valid Email id</span>
										</div>
										<div class="clear"></div>
										</div>
										<div class="form-group">
										<label class="control-label col-lg-3">MobileNo</label>
										<div class="col-lg-9">
											<input type="mobile" id="mobile" class="form-control" placeholder="Enter mobileno" maxlength="10" onkeypress="return isNumberKey(event)">
											<span class="errormsg" style="display:none">Enter Valid Mobile Number</span>
											
										</div>
										<div class="clear"></div>
										</div>
										<div class="form-group">
										<label class="control-label col-lg-3">UserType</label>
										<div class="col-lg-3">
											<select class="form-control" id="usertype">
												<option value="">Select</option>
												<option value="Donaters">Donaters</option>
												<option value="Volunteers">Volunteers</option>
												<option value="Others">Others</option>
											</select><br><br>
											<span class="errormsg" style="display:none">Select Usertype</span>
											<span class="successmessage"></span>
										</div>
										<div class="clear"></div>
										</div>
										

										<!-- <div class="form-group">
										<label class="control-label col-lg-3">Username</label>
										<div class="col-lg-9">
											<input type="text" id="uname" class="form-control" placeholder="Enter Username">
										</div>
										<div class="clear"></div>
										</div> -->

										<!-- <div class="form-group">
										<label class="control-label col-lg-3">Cause Image</label>
										<div class="col-lg-9">
											<input type="file" style="display: none;" name="file[]" id="pimage" class="form-control" onchange="document.getElementById('profile_pic').src = window.URL.createObjectURL(this.files[0])" name="profilepic[]" accept="image/* ">

											<img src="../images/resource/<?php echo $image; ?>" width="270px" height="225px" id="profile_pic">
											<script type="text/javascript">
												$('.form-group img').on('click',function(){
													$(this).siblings('input:file').click();
												});
											</script>
										</div>
										<div class="clear"></div>
										</div>
 -->




										<div class="col-md-12 text-center"><button id="add_causes" class="btn btn-success"><i class="fa fa-check"> Save</i></button></div>

									</div>
									<div class="col-lg-2 col-md-2 col-sm-2"></div>
									</div>
								</div>
							</div>
							<!-- /latest posts -->

						</div>

						
					</div>
					<!-- /dashboard content -->


					<!-- Footer -->
					<?php include("addons/footer.php") ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

	<script type="text/javascript">
			
		$("#add_causes").click(function()
		{
			 $('.errormsg').hide();
			var name = $("#name").val();
			var email = $("#email").val();
			var mobile = $("#mobile").val();
			var usertype = $("#usertype").val();
			var atposition = email.indexOf("@");
            var dotposition = email.lastIndexOf(".");

			

			// if(name.length == 0 || email.length == 0 || mobile.length == 0|| usertype.length == 0)
			// {
			// 	alert("Please enter all fields");
			// }
			var flag=false;
			 if(name.length<=2 || name.length>=30)
          	{

          		$('#name').siblings('.errormsg').show();
             //alert("Please enter atleast four letter name");  
             flag =true;  
             }

             else if (atposition<1 || dotposition<atposition+2 || dotposition+2>=email.length)
              {  
             // alert("Please enter a valid E-mail Id ");  
             // return false; 

             $('#email').siblings('.errormsg').show();
             //alert("Please enter atleast four letter name");  
             flag =true;   
              }
  
             else if(mobile.length!=10)
	          {

              $('#mobile').siblings('.errormsg').show();
             //alert("Please enter atleast four letter name");  
             flag =true; 
	           }
	          else if(usertype=='')
	          {
      
              // alert("Please select atleast one user type");  
              //  return false;  
               $('#usertype').siblings('.errormsg').show();
             //alert("Please enter atleast four letter name");  
             flag =true; 
	          }

			else
			{
			$.ajax({

				url:"api/adduser.php",
				type:"post",
				data:{name:name,email:email,mobile:mobile,usertype:usertype},

				success:function(data)
				{
					var jsondata = JSON.parse(data);

					if(jsondata.status == 1)
					{
						
						 $('.successmessage').css('color','green').html('<i class="fa fa-check"><b>Successfully Added Redirecting..</b>');

									  		  setTimeout(function(){
									  		  	window.location="user_table.php";


									  		}, 1000);
									  			
					
					
					}

					
					else
					{
						alert("Error");
					}
				}

			});
		}
		
			});

		// function submitinfo(slide_id)
		// {
		//     var form_data = new FormData();  
		//     var file_data = $("#pimage").prop("files")[0]; 
		//     for(var loopvar=0;loopvar<$("#pimage").prop("files").length;loopvar++)
		//     {
		//        var file_data = $("#pimage").prop("files")[loopvar]; 
		//       form_data.append("file[]", file_data);
		//     }
		//     if($("#pimage").prop("files").length==0)
		//       form_data.append("file[]", "");
		  
		  
		//       form_data.append("productid", slide_id);
		      
		//       $.ajax({
		//             url: "api/addcause_image.php",
		//             dataType: 'text',  // what to expect back from the PHP script, if anything
		//             cache: false,
		//             contentType: false,
		//             processData: false,
		//             data: form_data,                         
		//             type: 'post',
		//             success: function(php_script_response){
		//               var jsondata=JSON.parse(php_script_response);
		//                 if(jsondata.status==1)
		//                 {
		//                 	alert("Success");
		//                 	//location.reload();
		//                 	//location.reload();
		//                 }
		//                 else
		//                 {
		//             	    alert("Failed");
		//                 }
		//         }
		//       });
		// }


	</script>

	<script type="text/javascript">
            
            function isNumberKey(evt){
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
                return true;
            }

        </script>

       <!--  <script>
document.getElementById('add_user_menu').classList.add('active'); //add
</script> -->

</body>
</html>

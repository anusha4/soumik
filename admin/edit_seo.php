<?php include('addons/header.php'); ?>
<?php
include("./api/db.php");

$causeid = $_GET['causeid'];

$query = mysqli_query($con,"SELECT * FROM home_seo WHERE seo_slno = '$causeid'");

if(mysqli_num_rows($query)>0)
{
	$data = mysqli_fetch_assoc($query);


	$id = $data['seo_slno'];
	$cpage = $data['seo_page'];
	$ctitle = $data['seo_title'];
	$ckey = $data['seo_keywords'];
	$cdesc = $data['seo_description'];
	

	

}


?>

	<!-- Theme JS files -->
	<script type="text/javascript" src="assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/forms/selects/select2.min.js"></script>
	

	<script type="text/javascript" src="assets/js/core/app.js"></script>
	<script type="text/javascript" src="assets/js/pages/datatables_advanced.js"></script>
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="assets/css/extrastyles.css">
	<script type="text/javascript" src="tinymce/jquery.tinymce.min.js"></script>
	<script type="text/javascript" src="tinymce/tinymce.min.js"></script>
	<!-- CDN links -->
		<!-- <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script> -->
		<link rel="stylesheet" type="text/css" href="assets/css/amsify.suggestags.css">

		<!-- Amsify Plugin -->
		<script type="text/javascript" src="assets/js/jquery.amsify.suggestags.js"></script>
			<!-- /theme JS files -->

</head>
	
<body>

	<!-- Main navbar -->
    <?php include('addons/navbar.php'); ?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			
			<!-- /main sidebar -->
			<?php include('addons/left_side_bar.php'); ?>

			<!-- Main content -->
			<div class="content-wrapper">
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><span class="text-semibold"></span>Edit Seo</h4>
						</div>
						<div class="heading-elements">
						<a href="seo.php"><button type="button" class="btn border-slates bg-coral btn-flats"><i class="fas fa-arrow-left"></i> Back</button></a>

						</div>
					</div>
				</div>
				<!-- Content area -->
				<div class="content">

					<!-- Main charts -->
					
					<!-- /main charts -->


					<!-- Dashboard content -->
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12">

						
							<div class="panel panel-flat">
								<div class="panel-heading">
									<!-- <h6 class="panel-title">Edit Cause</h6> -->
									<div class="heading-elements">
										<ul class="icons-list">
											<!-- <a href="causes.php"><button type="button" class="btn border-slates text-slate-800 btn-flats">Back</button></a> -->
					                		<!-- <li><a data-action="collapse"></a></li>
					                		<li><a data-action="reload"></a></li> -->
					                	</ul>
				                	</div>
			                	</div>

								<div class="panel-body">
									<div class="row add_user_form">
									<div class="col-lg-2 col-md-2 col-sm-2"></div>
									<div class="col-lg-8 col-md-8 col-sm-8">
										<div class="form-group">
										<label class="control-label col-lg-3">Page</label>
										<div class="col-lg-9">
											<input type="text" id="cpage" class="form-control" placeholder="Enter Seo page" value="<?php echo $cpage; ?>">
											<span class="errormsg" style="display:none"> please Enter Seo Page  Name</span>
										</div>
										<div class="clear"></div>
										</div>

										<div class="form-group">
										<label class="control-label col-lg-3">Title</label>
										<div class="col-lg-9">
											<input type="text" id="ctitle" class="form-control" placeholder="Enter Seo title" value="<?php echo $ctitle; ?>">
											<span class="errormsg" style="display:none">Please Enter Seo Page Title</span>
										</div>
										<div class="clear"></div>
										</div>

										<div class="form-group">
										<label class="control-label col-lg-3">Keywords</label>
										<div class="col-lg-9">
											<input type="text" id="ckey" class="form-control" placeholder="Enter Seo keywords" name="ckey" value="<?php echo $ckey; ?>">
											<span class="errormsg" style="display:none">After Type the keywords press Enter key</span>
										</div>
										<div class="clear"></div>
										</div>

										<div class="form-group">
										<label class="control-label col-lg-3">Description</label>
										<div class="col-lg-9">
											<textarea  id="cdesc" data-rule-required="true" class="form-control" placeholder="Enter Seo desc" name="cdesc" value="<?php echo $cdesc; ?>"><?php echo $cdesc; ?></textarea>
											<span class="errormsg" style="display:none">Please Enter Minimum 5 letters  of Description</span>
											<span class="successmessage"></span>
										</div>
										<div class="clear"></div>
										</div>
										<div class="col-md-12 text-center"><button type="button" class="btn bg-darkcyan" id="add_new_pro"><i class="fa fa-check"></i> Update </button></div>


									</div>
									<div class="col-lg-2 col-md-2 col-sm-2"></div>
									</div>
								</div>
							</div>
							<!-- /latest posts -->

						</div>

						
					</div>
					<!-- /dashboard content -->


					<!-- Footer -->
				<?php include("addons/footer.php") ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

	<script type="text/javascript">
		
		$("#add_new_pro").click(function(event)
		{
			$('.errormsg').hide();
			var id = "<?php echo $causeid; ?>";
			var cpage = $("#cpage").val();
			var ctitle = $("#ctitle").val();
			var ckey = $("#ckey").val();
			// var cdesc = $("#cdesc").val();
			var cdesc= tinymce.get('cdesc').getContent();
			
			// if(cpage.length == 0 || ctitle.length == 0 )
			// {
			// 	alert("Please enter all fields");
			// }
			// else if (ckey.length == 0){
			// 	alert("After Type the keywords press Enter key");
			// }
			// else if (cdesc.length == 0){
			// 	alert("Please Enter Minimum 5 letters");
			// }
			var flag=false;
			if (cpage.length == 0)
              {  

	             $('#cpage').siblings('.errormsg').show();
	              flag =true;   
              }
  
            else if (ctitle.length==0)
              {  
	             $('#ctitle').siblings('.errormsg').show();
	              flag =true;   
              }
               else if (ckey.length==0)
              {  
	             $('#ckey').siblings('.errormsg').show();
	              flag =true;
	              
              }
               else if (cdesc.length==0)
              {  
	             $('#cdesc').siblings('.errormsg').show();
	              flag =true;   
              }
			//alert(product_id);
			else
			{
			$.ajax({
                              type:"post",
                              url:"api/edit_seo.php",
                              data:{id:id,cpage:cpage,ctitle:ctitle,ckey:ckey,cdesc:cdesc},

                              success:function(data){
                              	jsondata = JSON.parse(data);


									  	if(jsondata.status == 1)
									  	{


									  		$('.successmessage').css('color','green').html('<i class="fa fa-check"><b>Successfully Updated Redirecting..</b>');

									  		  setTimeout(function(){
									  		  	window.location="seo.php";


									  		}, 1000);

									  		//alert("Product Added Successfully");
									  		
											
									  	}

									  	else
									  	{
									  		alert("Error");
									  	}

                               
                            }


                        });
		}
		});



		
	</script>

	<script type="text/javascript">
            
            function isNumberKey(evt){
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
                return true;
            }

        </script>

</body>
</html>
<script>
	$('input[name="ckey"]').amsifySuggestags({
		type : 'amsify'
	});
	tinymce.init({
		selector: "#cdesc",
		plugins: [
			"textcolor colorpicker link image",
			"searchreplace visualblocks code fullscreen",
			"insertdatetime media table contextmenu paste"
		],
		toolbar: "insertfile undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link forecolor backcolor | link image"
	});
	// $(".form-horizontal").validate();
</script>

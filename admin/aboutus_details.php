<?php include('addons/header.php'); ?>
<?php
include("constants.php");
include("./api/db.php");
$result = array();

if (isset($_GET['aboutid'])) {
//     //echo $_GET['ulb'];

     $id = $_GET['aboutid'];

    

    $query = mysqli_query($con,"SELECT * FROM home_aboutus WHERE aboutus_id = '$id'");

   $data = mysqli_fetch_assoc($query);

  
}

?>

</head>

<body>

	<!-- Main navbar -->
    <?php include('addons/navbar.php'); ?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<?php include('addons/left_side_bar.php'); ?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><span class="text-semibold"></span>AboutUs Info</h4>
						</div>
						<div class="heading-elements">
										<ul class="icons-list">
											<div class="col-lg-12 col-md-12 text-right causes">
												<a href="aboutus.php"><button type="button" class="btn border-slates bg-coral btn-flats"><i class="fas fa-arrow-left"></i> Back</button></a>
					                		<!-- <li><a data-action="collapse"></a></li>
					                		<li><a data-action="reload"></a></li> -->
					                	</ul>
				                	</div>
						
					</div>
				</div>
				
				<!-- Content area -->
				<div class="content">

					<!-- Main charts -->
					
					<!-- /main charts -->


					<!-- Dashboard content -->
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12">


						
							<div class="panel panel-flat">
								<div class="panel-heading">
									<!-- <h3 class="panel-title">Add New causes</h3> -->
									<div class="heading-elements">
										<!-- <ul class="icons-list">
					                		<li><a data-action="collapse"></a></li>
					                		<li><a data-action="reload"></a></li>
					                	</ul> -->
				                	</div>
			                	</div>
			                	<div class="panel-body">
									<div class="row add_user_form">
									<div class="col-lg-2 col-md-2 col-sm-2"></div>
									<div class="col-lg-8 col-md-8 col-sm-8">
			                			<div class="form-group">
										<label class="control-label col-lg-3"><b>Title :</b></label>
										<div class="col-lg-9">
											<?php echo $data['aboutus_title']?> 
										</div>
										<div class="clear"></div>
										</div>


										<div class="form-group">
										<label class="control-label col-lg-3"><b>Description :</b></label>
										<div class="col-lg-9">
											<?php echo $data['aboutus_description']?>
										</div>
										<div class="clear"></div>
										</div>

										<div class="form-group">
									
										<label class="control-label col-lg-3"><b>Image :</b></label>
										<div class="col-lg-9">
											<?php
										if($data['aboutus_image1']!='')
										{

										?>
												<img src="../images/resource/<?php echo $data['aboutus_image1']; ?>" class="profile_img" style="border-radius: 0%">
											<?php
										}
										else
										{
										?>
										<img src="../images/resource/avatar-img.png" title="no image" width="200px" height="220px" id="profile_pic"><br><br>

									<?php
									}
									?>
											
											<div class="clear"></div>
										</div>
									
										</div>
							</div>
						</div>
					</div>
				</div>
							<!-- /latest posts -->

						</div>

						
					</div>
					<!-- /dashboard content -->
					<!-- Footer -->
					<?php include("addons/footer.php") ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>

<?php include('addons/header.php'); ?>


	<!-- Theme JS files -->
	<script type="text/javascript" src="assets/js/plugins/visualization/d3/d3.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
	<script type="text/javascript" src="assets/js/plugins/forms/styling/switchery.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
	<script type="text/javascript" src="assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/pickers/daterangepicker.js"></script>

	<script type="text/javascript" src="assets/js/core/app.js"></script>
	<script type="text/javascript" src="assets/js/pages/dashboard.js"></script>
	<!-- /theme JS files -->

</head>

<body>

	<!-- Main navbar -->	
    <?php include('addons/navbar.php'); ?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<?php include('addons/left_side_bar.php'); ?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

					<!-- Main charts -->
					
					<!-- /main charts -->


					<!-- Dashboard content -->
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12">

						
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h3 class="panel-title">Add New Banner</h3>
									<div class="heading-elements">
										<ul class="icons-list">
											<div class="col-lg-12 col-md-12 text-right causes"><a href="slides.php"><button type="button" class="btn border-slates text-slate-800 btn-flats">Back</button></a></div>
					                		<!-- <li><a data-action="collapse"></a></li>
					                		<li><a data-action="reload"></a></li> -->
					                	</ul>
				                	</div>
			                	</div>

								<div class="panel-body">
									<div class="row add_user_form">
									<div class="col-lg-2 col-md-2 col-sm-2"></div>
									<div class="col-lg-8 col-md-8 col-sm-8">
										<div class="form-group">
										<label class="control-label col-lg-3">Slide Name</label>
										<div class="col-lg-9">
											<input type="text" id="sname" class="form-control" placeholder="Enter Slidename">
										</div>
										<div class="clear"></div>
										</div>


										<div class="form-group">
										<label class="control-label col-lg-3">Slide Title</label>
										<div class="col-lg-9">
											<input type="text" id="stitle" class="form-control" placeholder="Enter Slidetitle">
										</div>
										<div class="clear"></div>
										</div>

										<!-- <div class="form-group">
										<label class="control-label col-lg-3">Username</label>
										<div class="col-lg-9">
											<input type="text" id="uname" class="form-control" placeholder="Enter Username">
										</div>
										<div class="clear"></div>
										</div> -->

										<div class="form-group">
										<label class="control-label col-lg-3">Slide Image</label>
										<div class="col-lg-9">
											<input type="file" style="display: none;" name="file[]" id="pimage" class="form-control" onchange="document.getElementById('profile_pic').src = window.URL.createObjectURL(this.files[0])" name="profilepic[]" accept="image/* ">

											<img src="../images/resource/<?php echo $image; ?>" width="270px" height="225px" id="profile_pic">
											<script type="text/javascript">
												$('.form-group img').on('click',function(){
													$(this).siblings('input:file').click();
												});
											</script>
										</div>
										<div class="clear"></div>
										</div>

										<div class="form-group">
										<label class="control-label col-lg-3">Slide video</label>
										<div class="col-lg-9">
											<!-- <input type="url" name="svideoUpload" class="form-control"  id="svideoUpload"placeholder="https://example.com"pattern="https://.*"required /> -->
											<input type="file" name="pimage1" id="pimage1">

										</div>
										<div class="clear"></div>
										</div>




										<div class="col-md-12 text-center"><button id="add_banner" class="btn btn-primary">Add New Banner</button></div>

									</div>
									<div class="col-lg-2 col-md-2 col-sm-2"></div>
									</div>
								</div>
							</div>
							<!-- /latest posts -->

						</div>

						
					</div>
					<!-- /dashboard content -->


					<!-- Footer -->
					<div class="footer text-muted">
						&copy; 2018. <a href="#">Soumik charity Trust </a> 
					</div>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

	<script type="text/javascript">
			
		$("#add_banner").click(function()
		{

			var sname = $("#sname").val();
			var stitle = $("#stitle").val();
			var pimage = $("#pimage").val();
			var pimage1 = $("#pimage1").val();
			

			if(sname.length == 0 || stitle.length == 0 || pimage.length == 0)
			{
				alert("Please enter all fields");
			}

			else
			{
			$.ajax({

				url:"api/add_banner.php",
				type:"post",
				data:{sname:sname,stitle:stitle},

				success:function(data)
				{
					var jsondata = JSON.parse(data);

					if(jsondata.status == 1)
					{
						
						var slide_id=jsondata.slno;
						if(pimage.length==0){

									  			alert("success");
									  			location.reload();
									  		}
						else{
						submitinfo(slide_id);
					}
					if(pimage1.length==0)
									  		{
									  			alert("success");
									  			//location.reload();
									  		}

									  		else{
									  			submitinfo1(slide_id);
									  		}
					}

					
					else
					{
						alert("Error");
					}
				}

			});
		}
		
			});

		function submitinfo(slide_id)
		{
		    var form_data = new FormData();  
		    var file_data = $("#pimage").prop("files")[0]; 
		    for(var loopvar=0;loopvar<$("#pimage").prop("files").length;loopvar++)
		    {
		       var file_data = $("#pimage").prop("files")[loopvar]; 
		      form_data.append("file[]", file_data);
		    }
		    if($("#pimage").prop("files").length==0)
		      form_data.append("file[]", "");
		  
		  
		      form_data.append("productid", slide_id);
		      
		      $.ajax({
		            url: "api/addproduct_image.php",
		            dataType: 'text',  // what to expect back from the PHP script, if anything
		            cache: false,
		            contentType: false,
		            processData: false,
		            data: form_data,                         
		            type: 'post',
		            success: function(php_script_response){
		              var jsondata=JSON.parse(php_script_response);
		                if(jsondata.status==1)
		                {
		                	alert("Success");
		                	//location.reload();
		                	//location.reload();
		                }
		                else
		                {
		            	    alert("Failed");
		                }
		        }
		      });
		}
function submitinfo1(slide_id)
		{
		    var form_data = new FormData();  
		    var file_data = $("#pimage1").prop("files")[0]; 
		    for(var loopvar=0;loopvar<$("#pimage1").prop("files").length;loopvar++)
		    {
		       var file_data = $("#pimage1").prop("files")[loopvar]; 
		      form_data.append("file[]", file_data);
		    }
		    if($("#pimage1").prop("files").length==0)
		      form_data.append("file[]", "");
		  
		  
		      form_data.append("productid", slide_id);
		      
		      $.ajax({
		            url: "api/addproduct_image1.php",
		            dataType: 'text',  // what to expect back from the PHP script, if anything
		            cache: false,
		            contentType: false,
		            processData: false,
		            data: form_data,                         
		            type: 'post',
		            success: function(php_script_response){
		              var jsondata=JSON.parse(php_script_response);
		                if(jsondata.status==1)
		                {
		                	alert("Success");
		                	location.reload();
		                	//location.reload();
		                }
		                else
		                {
		            	    alert("Failed");
		                }
		        }
		      });
		}


	</script>

	<script type="text/javascript">
            
            function isNumberKey(evt){
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
                return true;
            }

        </script>

       <!--  <script>
document.getElementById('add_user_menu').classList.add('active'); //add
</script> -->

</body>
</html>

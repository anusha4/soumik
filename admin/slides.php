<?php include('addons/header.php'); ?>

</head>
	
<body>

	<!-- Main navbar -->
	<?php include('addons/navbar.php'); ?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			
			<!-- /main sidebar -->
			<?php include('addons/left_side_bar.php'); ?>

			<!-- Main content -->
			<div class="content-wrapper">
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><span class="text-semibold"></span>BANNERS</h4>
						</div>
					</div>
				</div>
				<!-- Content area -->
				<div class="content">

					<!-- Main charts -->
					
					<!-- /main charts -->


					<!-- Dashboard content -->
					<div class="row">
						<div class="col-lg-12">

						
							<div class="panel panel-flat">
								
			                	<!-- <div class="col-lg-12 col-md-12 text-right"><a href="addbanner.php"><button type="button" class="btn border-slate text-slate-800 btn-flat"><i class="icon-plus3 position-left"></i> Add</button></a></div> -->
			                	<div class="clear"></div>
								<div class="panel-body">
									<div class="display row table-responsive" id="user_table_section">
									
										
									</div>
								</div>
							</div>
							<!-- /latest posts -->

						</div>

						
					</div>
					<!-- /dashboard content -->


					<!-- Footer -->
					<?php include("addons/footer.php") ?>
					<!-- /footer -->
					

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
<div class="modal fade" id="deleteSliderModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	 <div class="modal-dialog" role="document">
			<div class="modal-content">
				
					<div class="modal-header">						
						<h4 class="modal-title">Delete Slider</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <i class="fa fa-times-circle"></i><span aria-hidden="true"></span>
				        </button>
					</div>
					<div class="modal-body">					
						<p >Are you sure you want to delete <b class="text-warning"><span id="get_name_here"></span>?</b></p>
						<!-- <p ></p> -->
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
						<input type="submit" class="btn btn-danger" data-dismiss="modal" value="Delete" onclick="deleteslider()">
					</div>
				
			</div>
		</div>
	</div>
	<script type="text/javascript">
	
	 $(document).ready(function() {

	 	    $.ajax({    //create an ajax request to load_page.php
        type: "POST",
        url: "api/getslider.php", 
        //data: {readpro : 1} ,
        // dataType: 'json' ,         
          //expect html to be returned                
        success: function(data){  

            console.log(data);
            dataa=JSON.parse(data).sliders;

            var sliders1 = JSON.parse(data);
            var sliders = sliders1.sliders;
            var str='<table id="user_table" class="table  table-bordered" style="white-space:nowrap"    width="100%"><thead><tr><th>SNo</th><th > Name</th><th> Title</th><th >Image</th><th class="text-center">Actions</th></tr></thead><tbody class="list_ads">'
            // var str='<table class="table datatable-basic" id="user_table"><thead><tr><th>SNo</th><th>Slide Name</th><th>Slide Title</th><th>Image</th><th class="text-center">Actions</th></tr></thead><tbody>';

            for(var i=0; i<sliders.length; i++ ){

              	var j =i+1;
              	str+="<tr><td>" + j + "</td>" +
              		"<td>" + sliders[i].name + "</td>" +
              		"<td>" + sliders[i].title + "</td>" +  
              		"<td>" + sliders[i].image + "</td>" + 
              		

              		

              		"<td>" + "<a href='slider_details.php?slideid=" + sliders[i].id + "''><i  class='table_action bg-orange fa fa-eye' title='view'></i></a> "+
              		 "<a href='edit_slide.php?slideid=" + sliders[i].id + "''><i class='table_action bg-blue fa fa-edit' title='Edit'></i></a> "+"<a href='#deleteSliderModal' class='delete' data-toggle='modal'><i class='table_action bg-danger fa fa-trash deleteslider' data-id="+sliders[i].id+" title='Delete'onclick='deleteslide("+sliders[i].id+")'></i></a>"+"</td>"

              		// "<td>" + "<button class='view_btn'>View </button> <button class='edit_btn'>Edit<button> <button class='delete_btn'>Delete </button>" + "</td>"
              	}

              	$("#user_table_section").html(str+"</tbody></table>");
				 $('#user_table').dataTable({
      			//paging: false
    		});
    		}
				});
	 	   });

            </script>

            <script>
document.getElementById('products').classList.add('active'); //add
</script>

</body>
</html>
<script type="text/javascript">
	$(document).ready(function () {
  $('#user_table').DataTable();
  $('.dataTables_length').addClass('bs-select');
});
	$(document).ready(function () {
  $('#user_table').DataTable({
    "paging": false // false to disable pagination (or any other option)
  });
  $('.dataTables_length').addClass('bs-select');
});
	// Basic example
$(document).ready(function () {
  $('#user_table').DataTable({
    "pagingType": "simple" // "simple" option for 'Previous' and 'Next' buttons only
  });
  $('.dataTables_length').addClass('bs-select');
});
</script>

<script type="text/javascript">
	function deleteslide(delid){
		
		console.log("data is ",dataa);
		for(i=0;i<dataa.length;i++){
			if(dataa[i].id==delid){
				 // reqNameId=dataa[i].page;
				 reqNameId=document.getElementById("get_name_here").innerText=dataa[i].name;
				 reqSeoId=dataa[i].id;
				console.log("the page name is ",dataa[i].name)
			}
		}
	}
	function deleteslider(){
		console.log("delete is ",reqSeoId);
		window.location.href='api/deleteslider.php?del_id=' +reqSeoId+'';
		
	}
</script>


<!-- <script type="text/javascript">
	
	$(document).on('click', '.deleteslider', function(){

		//$(this).parent().parent().parent().remove();
		 var el = $(this);
		
		 var bannerid=$(this).data('id');
		 $.ajax({    //create an ajax request to load_page.php
        type: "POST",
        url: "api/deleteslider.php",
        data:{bannerid:bannerid},
                     
        success: function(data){ 

         var jsondata = JSON.parse(data);
         if(jsondata.status==1)
         {
         	alert("Successfully Deleted");
         	// location.reload();
         	el.parent().parent().parent().remove();
         
         }
         else
         {
         	alert("Failure");
         }

        }
        }); 
		
	});
</script> -->

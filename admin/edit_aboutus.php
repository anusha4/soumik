<?php include('addons/header.php'); ?>
<?php
include("./api/db.php");

$aboutid = $_GET['aboutid'];

$query = mysqli_query($con,"SELECT * FROM home_aboutus WHERE aboutus_id = '$aboutid'");

if(mysqli_num_rows($query)>0)
{
	$data = mysqli_fetch_assoc($query);


	$id = $data['aboutus_id'];
	$title = $data['aboutus_title'];
	$desc = $data['aboutus_description'];
	$image1 = $data['aboutus_image1'];
	
	

}


?>

</head>

<body>

	<!-- Main navbar -->
    <?php include('addons/navbar.php'); ?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			
			<!-- /main sidebar -->
			<?php include('addons/left_side_bar.php'); ?>

			<!-- Main content -->
			<div class="content-wrapper">
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><span class="text-semibold"></span>Edit About Us</h4>
						</div>
						<div class="heading-elements">
						<a href="aboutus.php"><button type="button" class="btn border-slates bg-coral btn-flats"><i class="fas fa-arrow-left"></i> Back</button></a>
						</div>

					</div>
				</div>
					
				<!-- Content area -->
				<div class="content">

					<!-- Main charts -->
					
					<!-- /main charts -->


					<!-- Dashboard content -->
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12">

						
							<div class="panel panel-flat">
								<div class="panel-heading">
									<!-- <h6 class="panel-title">Edit Aboutus</h6> -->
									<div class="heading-elements">
										<!-- <ul class="icons-list">
					                		<li><a data-action="collapse"></a></li>
					                		<li><a data-action="reload"></a></li>
					                	</ul> -->
				                	</div>
			                	</div>

								<div class="panel-body">
									<div class="row add_user_form">
									<div class="col-lg-2 col-md-2 col-sm-2"></div>
									<div class="col-lg-8 col-md-8 col-sm-8">
										<div class="form-group">
										<label class="control-label col-lg-3">Title</label>
										<div class="col-lg-9">
											<input type="text" id="aboutus_title" class="form-control" placeholder="Enter cause Name" value="<?php echo $title; ?>">
											<span class="errormsg" style="display:none">Please Enter AboutUs Title</span>
										</div>
										<div class="clear"></div>
										</div>

										<div class="form-group">
										<label class="control-label col-lg-3">Description</label>
										<div class="col-lg-9">
											<textarea id="aboutus_description" class="form-control" placeholder="Enter cause desc" value="<?php echo $desc; ?>"><?php echo $desc; ?></textarea>
											<span class="errormsg" style="display:none">Please Enter AboutUs Description</span>
										</div>
										<div class="clear"></div>
										</div>

										


										<div class="form-group">
										<label class="control-label col-lg-3">Image</label>
										<div class="col-lg-9">
											<!-- <input type="file" id="pimage" class="form-control" placeholder="upload Image"> -->

									<input type="file" style="display: none;" name="file[]" id="aboutus_image1" class="form-control" onchange="document.getElementById('profile_pic').src = window.URL.createObjectURL(this.files[0])" name="profilepic[]" accept="image/* ">

									<?php
										if($image1!='')
										{

										?>
											<img src="../images/resource/<?php echo $image1; ?>" width="200px" height="220px" id="profile_pic"><br><br>
											
									<?php
										}
										else
										{
										?>
										<img src="../images/resource/avatar-img.png" width="200px" height="220px" id="profile_pic"><br><br>

									<?php
									}
									?>
									<span class ="uploaded_file_name"></span>

											<script type="text/javascript">
												$('.form-group img').on('click',function(){
													$(this).siblings('input:file').click();
												});
											</script>
											<span class="errormsg" style="display:none">Please Select Image and please give valid size 1mb</span>
											<span class="successmessage"></span>
										</div>
										<div class="clear"></div>
										</div>
										


									<!-- <div class="form-group">
										<label class="control-label col-lg-3">Aboutus Image2</label>
										<div class="col-lg-9">
											 <input type="file" id="pimage" class="form-control" placeholder="upload Image"> 

									<input type="file" style="display: none;" name="file[]" id="aboutus_image2" class="form-control" onchange="document.getElementById('profile_pic1').src = window.URL.createObjectURL(this.files[0])" name="profilepic[]" accept="image/* ">

											
											<img src="../images/resource/<?php echo $image2; ?>" width="200px" height="220px" id="profile_pic1"><br><br>
											<span class ="uploaded_file_name"></span>

											<script type="text/javascript">
												$('.form-group img').on('click',function(){
													$(this).siblings('input:file').click();
												});
											</script>




										</div>
										<div class="clear"></div>
										</div> -->

										<div class="col-md-12 text-center"><button type="button" class="btn bg-darkcyan" id="add_new_pro"><i class="fa fa-check"></i> Update </button></div>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2"></div>
									</div>
								</div>
							</div>
							<!-- /latest posts -->

						</div>

						
					</div>
					<!-- /dashboard content -->


					<!-- Footer -->
					<?php include("addons/footer.php") ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

	<script type="text/javascript">
		
		$("#add_new_pro").click(function(event)
		{
			 $('.errormsg').hide();
			var id = "<?php echo $aboutid; ?>";
			//alert(id);
			var aboutus_title = $("#aboutus_title").val();
			var aboutus_description = $("#aboutus_description").val();
			//var donationtype = $("#aboutus_donationtype").val();
			var aboutus_image1 = $("#aboutus_image1").val();
			var file = $('#aboutus_image1').prop("files")[0];

			var url = $('#profile_pic').attr('src');
			var isLastSlash = (url[url.length -1]=="/")? true: false;
			var url= url.split("/");
			var avatorimage = url[url.length - (isLastSlash? 2: 1)];
			// var image2 = $("#aboutus_image2").val();

			var flag=false;

			 if(aboutus_title.length== 0)
          	{

          		$('#aboutus_title').siblings('.errormsg').show();
             //alert("Please enter atleast four letter name");  
             flag =true;  
             }

             else if (aboutus_description.length == 0)
              {  
             // alert("Please enter a valid E-mail Id ");  
             // return false; 

             $('#aboutus_description').siblings('.errormsg').show();
             //alert("Please enter atleast four letter name");  
             flag =true;   
              }
  
           
              else if(avatorimage=='avatar-img.png'){
              	$('#aboutus_image1').siblings('.errormsg').show();
             	flag =true;   
              }
			else
			{

			$.ajax({
                              type:"post",
                              url:"api/edit_aboutus.php",
                              data:{id:id,aboutus_title:aboutus_title,aboutus_description:aboutus_description},

                              success:function(data){
                              	jsondata = JSON.parse(data);


									  	if(jsondata.status == 1)
									  	{

									  		if(aboutus_image1.length==0)
									  		{
									  			$('.successmessage').css('color','green').html('<i class="fa fa-check"><b>Successfully Updated Redirecting..</b>');

									  		  setTimeout(function(){
									  		  	window.location="aboutus.php";


									  		}, 1000);
									  			//location.reload();
									  		}
									  		else
									  		{
									  			submitinfo(id);
									  		}
									  		

									  		

									  		//alert("Product Added Successfully");
									  		
											
									  	}

									  	else
									  	{
									  		alert("Error");
									  	}

                               
                            }


                        });
		}
		});



		function submitinfo(slide_id)
		{
			// var image2 = $("#aboutus_image2").val();
		    var form_data = new FormData();  
		    var file_data = $("#aboutus_image1").prop("files")[0]; 
		    for(var loopvar=0;loopvar<$("#aboutus_image1").prop("files").length;loopvar++)
		    {
		       var file_data = $("#aboutus_image1").prop("files")[loopvar]; 
		      form_data.append("file[]", file_data);
		    }
		    if($("#aboutus_image1").prop("files").length==0)
		      form_data.append("file[]", "");
		  
		  
		      form_data.append("productid", slide_id);
		      
		      $.ajax({
		            url: "api/edit_aboutusimage.php",
		            dataType: 'text',  // what to expect back from the PHP script, if anything
		            cache: false,
		            contentType: false,
		            processData: false,
		            data: form_data,                         
		            type: 'post',
		            success: function(php_script_response){
		              var jsondata=JSON.parse(php_script_response);
		                if(jsondata.status==1)
		                {
		            	            
									  			$('.successmessage').css('color','green').html('<i class="fa fa-check"><b>Successfully Updated Redirecting..</b>');

									  		  setTimeout(function(){
									  		  	window.location="aboutus.php";


									  		}, 1000);
									  		
		                }
		                else
		                {
		            	    alert("Failed");
		                }
		        }
		      });
		}

// function submitinfo1(slide_id)
// 		{
// 			var image2 = $("#aboutus_image2").val();
// 		    var form_data = new FormData();  
// 		    var file_data = $("#aboutus_image2").prop("files")[0]; 
// 		    for(var loopvar=0;loopvar<$("#aboutus_image2").prop("files").length;loopvar++)
// 		    {
// 		       var file_data = $("#aboutus_image2").prop("files")[loopvar]; 
// 		      form_data.append("file[]", file_data);
// 		    }
// 		    if($("#aboutus_image2").prop("files").length==0)
// 		      form_data.append("file[]", "");
		  
		  
// 		      form_data.append("productid", slide_id);
		      
// 		      $.ajax({
// 		            url: "api/edit_aboutusimage1.php",
// 		            dataType: 'text',  // what to expect back from the PHP script, if anything
// 		            cache: false,
// 		            contentType: false,
// 		            processData: false,
// 		            data: form_data,                         
// 		            type: 'post',
// 		            success: function(php_script_response){
// 		              var jsondata=JSON.parse(php_script_response);
// 		                if(jsondata.status==1)
// 		                {
// 		                	alert("Success");
// 		                	//location.reload();
// 		                	//location.reload();
// 		                }
// 		                else
// 		                {
// 		            	    alert("Failed");
// 		                }
// 		        }
// 		      });
// 		}


	</script>
<script type="text/javascript">
		$('#aboutus_image1').on('change',function()
		{

			
			var image = $("#aboutus_image1").val();

			//alert(val);
           var file = $('#aboutus_image1').prop("files")[0];


			//alert('hi');
			if (!image.match(/(?:gif|png|jpg|bmp)$/))
                                                 {
    
                                                 alert("inputted file path is not an image!");
                                                 }
    										   else
    										 if(file.size>1024000)
                                                 {
                                                 alert('please give valid size 1mb');
                                                  }

		});
	</script>
	<script type="text/javascript">
            
            function isNumberKey(evt){
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
                return true;
            }

        </script>

</body>
</html>

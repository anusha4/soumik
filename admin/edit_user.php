<?php include('addons/header.php'); ?>
<?php
include("./api/db.php");

$slideid = $_GET['slideid'];

$query = mysqli_query($con,"SELECT * FROM users WHERE user_slno = '$slideid'");

if(mysqli_num_rows($query)>0)
{
	$data = mysqli_fetch_assoc($query);


	$id = $data['user_slno'];
	$name = $data['user_name'];
	$email = $data['user_email'];
	$mobile = $data['user_mobile'];
	$usertype = $data['user_usertype'];
	$addedby = $data['addedby'];
	// $addedon = $data['user_addedon'];

	

}


?>


</head>

<body>

	<!-- Main navbar -->
	<?php include('addons/navbar.php'); ?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			
			<!-- /main sidebar -->
			<?php include('addons/left_side_bar.php'); ?>

			<!-- Main content -->
			<div class="content-wrapper">
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><span class="text-semibold"></span>Edit Users</h4>
						</div>
						<div class="heading-elements">
						<a href="user_table.php"><button type="button" class="btn border-slates bg-coral btn-flats"><i class="fas fa-arrow-left"></i> Back</button></a>
						</div>
					</div>
				</div>
				<!-- Content area -->
				<div class="content">

					<!-- Main charts -->
					
					<!-- /main charts -->


					<!-- Dashboard content -->
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12">

						
							<div class="panel panel-flat">
								<div class="panel-heading">
									<!-- <h6 class="panel-title">Edit Cause</h6> -->
									<div class="heading-elements">
										<ul class="icons-list">
											<!-- <a href="causes.php"><button type="button" class="btn border-slates text-slate-800 btn-flats">Back</button></a> -->
					                		<!-- <li><a data-action="collapse"></a></li>
					                		<li><a data-action="reload"></a></li> -->
					                	</ul>
				                	</div>
			                	</div>

								<div class="panel-body">
									<div class="row add_user_form">
									<div class="col-lg-2 col-md-2 col-sm-2"></div>
									<div class="col-lg-8 col-md-8 col-sm-8">
										<div class="form-group">
										<label class="control-label col-lg-3">Name</label>
										<div class="col-lg-9">
											<input type="text" id="name" class="form-control" placeholder="Enter  Name" value="<?php echo $name; ?>">
											<span class="errormsg" style="display:none">Please Enter Atleast Four Letter Name</span>
										</div>
										<div class="clear"></div>
										</div>

										<div class="form-group">
										<label class="control-label col-lg-3">Email</label>
										<div class="col-lg-9">
											<input type="email" id="email" class="form-control" placeholder="Enter email" value="<?php echo $email; ?>">
											<span class="errormsg" style="display:none">Enter Valid Email id</span>
										</div>
										<div class="clear"></div>
										</div>
										<div class="form-group">
										<label class="control-label col-lg-3">Mobile</label>
										<div class="col-lg-9">
											<input type="mobile" id="mobile" class="form-control" placeholder="Enter mobileno" maxlength="10" value="<?php echo $mobile; ?>" onkeypress="return isNumberKey(event)">
											
											<span class="errormsg" style="display:none">Enter Valid Mobile Number</span>
										</div>
										<div class="clear"></div>
										</div>
										<div class="form-group">
										<label class="control-label col-lg-3">Usertype</label>
										<div class="col-lg-9">
											<!-- <input type="text" id="usertype" class="form-control userty"  value="<?php echo $usertype; ?>"> -->
											<select class="form-control usert" id="usertypes">

												<option value="<?php if($usertype!=''){ echo $usertype; }else { echo ''; } ?>"><?php if($usertype!=''){ echo $usertype; }else { echo 'Select'; } ?></option>
										   
										   <?php if($usertype=='Donaters')
											{
												?>
												<option value="Volunteers">Volunteers</option>
												<option value="Others">Others</option>


											<?php	

											}
											else if($usertype=='Volunteers')
											{
											?>
											<option value="Donaters">Donaters</option>
												<option value="Others">Others</option>
												<?php

											}
											else if($usertype=='Others')
											{
											?>
											<option value="Donaters">Donaters</option>
											<option value="Volunteers">Volunteers</option>

												<?php
											}
											else
											{
												?>
										

											<option value="Donaters">Donaters</option>
												<option value="Volunteers">Volunteers</option>
												<option value="Others">Others</option>
												<?php
											}
											?>


												
												
											</select>
											<span class="errormsg" style="display:none">Select Usertype</span>
											<span class="successmessage"></span>
										</div>
										<div class="clear"></div>
										</div>
										<!-- <div class="form-group">
										<label class="control-label col-lg-3">AddedBy</label>
										<div class="col-lg-9">
											<input type="text" id="addedby" class="form-control" value="<?php echo $addedby; ?>">
										</div>
										<div class="clear"></div>
										</div> -->
										<!-- <div class="form-group">
										<label class="control-label col-lg-3">Addedon</label>
										<div class="col-lg-9">
											<input type="text" id="addedon" class="form-control"  value="<?php echo date("m-d-Y h:i A", strtotime($data['user_addedon']));  ?>">
										</div>
										<div class="clear"></div>
										</div> -->
										

<!-- 
										<div class="form-group">
										<label class="control-label col-lg-3">Image</label>
										<div class="col-lg-9">
											

									<input type="file" style="display: none;" name="file[]" id="pimage" class="form-control" onchange="document.getElementById('profile_pic').src = window.URL.createObjectURL(this.files[0])" name="profilepic[]" accept="image/* ">

											
											<img src="../images/resource/<?php echo $image; ?>" width="200px" height="220px" id="profile_pic"><br><br>
											<span class ="uploaded_file_name"></span>

											<script type="text/javascript">
												$('.form-group img').on('click',function(){
													$(this).siblings('input:file').click();
												});
											</script>




										</div>
										<div class="clear"></div>
										</div> -->

										<div class="col-md-12 text-center"><button type="button" class="btn bg-darkcyan" id="add_new_pro"><i class="fa fa-check"></i> Update</button></div>

									</div>
									<!-- <div class="col-lg-2 col-md-2 col-sm-2"></div> -->
									</div>
								</div>
							</div>
							<!-- /latest posts -->

						</div>

						
					</div>
					<!-- /dashboard content -->


					<!-- Footer -->
				<?php include("addons/footer.php") ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

	<script type="text/javascript">
		
		$("#add_new_pro").click(function(event)
		{
			 $('.errormsg').hide();
			 // $('.usert').hide();

			var id = "<?php echo $slideid; ?>";
			var name = $("#name").val();
			var email = $("#email").val();
			var mobile = $("#mobile").val();
			var usertype = $("#usertypes").val();
			// var addedby = $("#addedby").val();
			// var addedon = $("#addedon").val();
			var atposition = email.indexOf("@");
            var dotposition = email.lastIndexOf(".");

			var flag=false;

			 if(name.length<=2 || name.length>=30)
          	{

          		$('#name').siblings('.errormsg').show();
             //alert("Please enter atleast four letter name");  
             flag =true;  
             }

             else if (atposition<1 || dotposition<atposition+2 || dotposition+2>=email.length)
              {  
             // alert("Please enter a valid E-mail Id ");  
             // return false; 

             $('#email').siblings('.errormsg').show();
             //alert("Please enter atleast four letter name");  
             flag =true;   
              }
  
             else if(mobile.length!=10)
	          {

              $('#mobile').siblings('.errormsg').show();
             //alert("Please enter atleast four letter name");  
             flag =true; 
	           }
	          else if(usertype=='')
	          {
      
              // alert("Please select atleast one user type");  
              //  return false;  
              // $('#usertypes').siblings('.usert').show();
               $('#usertypes').siblings('.errormsg').show();
             //alert("Please enter atleast four letter name");  
             flag =true; 
	          }
	          
			


			//alert(product_id);
			else{
			$.ajax({
                              type:"post",
                              url:"api/edit_user.php",
                              data:{id:id,name:name,email:email,mobile:mobile,usertype:usertype},

                              success:function(data){
                              	jsondata = JSON.parse(data);

                              	
									  	if(jsondata.status == 1)
									  	{

									  		
									  		//  //alert("Hello");
									  		  $('.successmessage').css('color','green').html('<i class="fa fa-check"><b>Successfully updated Redirecting..</b>');

									  		  setTimeout(function(){
									  		  	window.location="user_table.php";


									  		}, 1000);

									  	
											
									  	}

									  	else
									  	{
									  		alert("Error");
									  	}

                               
                            }


                        });
		}
		});



		// function submitinfo(slide_id)
		// {
		//     var form_data = new FormData();  
		//     var file_data = $("#pimage").prop("files")[0]; 
		//     for(var loopvar=0;loopvar<$("#pimage").prop("files").length;loopvar++)
		//     {
		//        var file_data = $("#pimage").prop("files")[loopvar]; 
		//       form_data.append("file[]", file_data);
		//     }
		//     if($("#pimage").prop("files").length==0)
		//       form_data.append("file[]", "");
		  
		  
		//       form_data.append("productid", slide_id);
		      
		//       $.ajax({
		//             url: "api/editcause_image.php",
		//             dataType: 'text',  // what to expect back from the PHP script, if anything
		//             cache: false,
		//             contentType: false,
		//             processData: false,
		//             data: form_data,                         
		//             type: 'post',
		//             success: function(php_script_response){
		//               var jsondata=JSON.parse(php_script_response);
		//                 if(jsondata.status==1)
		//                 {
		//                 	alert("Success");
		//                 	// location.reload();
		//                 	//location.reload();
		//                 }
		//                 else
		//                 {
		//             	    alert("Failed");
		//                 }
		//         }
		//       });
		// }


	</script>

	<script type="text/javascript">
            
            function isNumberKey(evt){
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
                return true;
            }

        </script>

</body>
</html>

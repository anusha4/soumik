<?php include('addons/header.php'); ?>

</head>
	
<body>

	<!-- Main navbar -->
	<?php include('addons/navbar.php'); ?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			
			<!-- /main sidebar -->
			<?php include('addons/left_side_bar.php'); ?>

			<!-- Main content -->
			<div class="content-wrapper">
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><span class="text-semibold"></span>SEO</h4>
						</div>
					</div>
				</div>
				<!-- Content area -->
				<div class="content">

					<!-- Main charts -->
					
					<!-- /main charts -->


					<!-- Dashboard content -->
					<div class="row">
						<div class="col-lg-12">

						
							<div class="panel panel-flat">
								
			                	<div class="col-lg-12 col-md-12 text-right"><a href="addseo.php"><button type="button" class="btn border-slate text-slate-800 add_btn btn-flat"><i class="fa fa-plus"> Add</i></button></a></div>
			                	<div class="clear"></div>
								<div class="panel-body">
									<div class="display row table-responsive" id="seo_table">
							
									</div>
								</div>
							</div>
							<!-- /latest posts -->

						</div>

						
					</div>
					<!-- /dashboard content -->


					<!-- Footer -->
					<?php include("addons/footer.php") ?>
					<!-- /footer -->
					

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
<!-- Delete Modal HTML -->
<div class="modal fade" id="deleteEmployeeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	 <div class="modal-dialog" role="document">
			<div class="modal-content">
				
					<div class="modal-header">						
						<h4 class="modal-title">Delete Seo</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
					</div>
					<div class="modal-body">					
						<p >Are you sure you want to delete <b class="text-warning "s><span id="get_name_here"></span>?</b></p>
						<!-- <p ></p> -->
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
						<input type="submit" class="btn btn-danger" data-dismiss="modal" value="Delete" onclick="deletedataseo()">
					</div>
				
			</div>
		</div>
	</div>
	<script type="text/javascript">
	
	 $(document).ready(function() {

	 	    $.ajax({    //create an ajax request to load_page.php
        type: "POST",
        url: "api/getseo.php", 
        //data: {readpro : 1} ,
        // dataType: 'json' ,         
          //expect html to be returned                
        success: function(data){  

            console.log(data);
            dataa=JSON.parse(data).seo_data;
            var seo = JSON.parse(data);
            var seo_data = seo.seo_data;
            var str='<table id="seotable" class="table  table-bordered" width="100%" ><thead><tr><th>SNo</th><th >Page</th><th> Title</th><th >keywords</th><th >Description</th><th >AddedOn</th><th class="text-center">Actions</th></tr></thead><tbody class="list_ads">'
            // var str='<table class="table datatable-basic" id="all_donaters_table"><thead><tr><th>SNo</th><th>Slide Name</th><th>Slide Title</th><th>Image</th><th class="text-center">Actions</th></tr></thead><tbody>';

            for(var i=0; i<seo_data.length; i++ ){

              	var j =i+1;
              	str+="<tr><td>" + j + "</td>" +
              		"<td>" + seo_data[i].page + "</td>" +
              		"<td>" + seo_data[i].title + "</td>" +  
              		"<td>" + seo_data[i].keywords + "</td>" + 
              		"<td>" + seo_data[i].desc +"</td>" + 
              		"<td>" + seo_data[i].addedon + "</td>" + 
              		
              		
              		"<td style='white-space: nowrap;'>" +
             		  "<a href='edit_seo.php?causeid=" + seo_data[i].id + "''><i class='table_action fa fa-edit bg-blue' title='Edit'></i></a> "+"<a href='#deleteEmployeeModal' class='delete' data-toggle='modal'><i class='table_action fa fa-trash bg-danger deleteseo' data-id="+seo_data[i].id+" title='Delete' onclick='deleteseo("+seo_data[i].id+")'></i></a>"+"</td>"
              		

              		// "<td>" + "<button class='view_btn'>View </button> <button class='edit_btn'>Edit<button> <button class='delete_btn'>Delete </button>" + "</td>"
              	}

              	$("#seo_table").html(str+"</tbody></table>");
				 $('#seotable').dataTable({
      			//paging: false
    		});
    		}
				});
	 	   });

            </script>

          <!--   <script>

document.getElementById('products').classList.add('active'); //add
</script> -->
<script type="text/javascript">
	function deleteseo(delid){
		
		console.log("data is ",dataa);
		for(i=0;i<dataa.length;i++){
			if(dataa[i].id==delid){
				 // reqNameId=dataa[i].page;
				 reqNameId=document.getElementById("get_name_here").innerText=dataa[i].page;
				 reqSeoId=dataa[i].id;
				console.log("the page name is ",dataa[i].page)
			}
		}
	}
	function deletedataseo(){
		console.log("delete is ",reqSeoId);
		window.location.href='api/deleteseo.php?del_id=' +reqSeoId+'';
		
	}
</script>
</body>
</html>
<script type="text/javascript">
	$(document).ready(function () {
  $('#seotable').DataTable();
  $('.dataTables_length').addClass('bs-select');
});
	$(document).ready(function () {
  $('#seotable').DataTable({
    "paging": false // false to disable pagination (or any other option)
  });
  $('.dataTables_length').addClass('bs-select');
});
	// Basic example
$(document).ready(function () {
  $('#seotable').DataTable({
    "pagingType": "simple" // "simple" option for 'Previous' and 'Next' buttons only
  });
  $('.dataTables_length').addClass('bs-select');
});
</script>
<!-- 
<script type="text/javascript">

	$(document).on('click', '.deleteseo', function(){

		//$(this).parent().parent().parent().remove();
		 var el = $(this);
		
		 var bannerid=$(this).data('id');

		 $.ajax({    //create an ajax request to load_page.php
        type: "POST",
        url: "api/delete_seo.php",
        data:{bannerid:bannerid},
                     
        success: function(data){ 

         var jsondata = JSON.parse(data);
         // return confirm("Are you sure you want to delete this item?");
         if(jsondata.status==1)
         {
         	 // return confirm("Are you sure you want to delete this item?");
         	alert("Successfully Deleted");
         	// location.reload();
         	el.parent().parent().parent().remove();
         	location.reload();
         
         }
         else
         {
         	alert("Failure");
         }

        }
        }); 
		
	});
</script>
<script type="text/javascript">
	$(document).ready(function(){

		// readProducts();
	$(document).on('click', '#delete_seo', function(e){

		//$(this).parent().parent().parent().remove();
		 // var el = $(this);
		
		 var bannerid=$(this).data('id');
		 SwalDelete(bannerid);
		 e.preventDefault();
		});
});
function SwalDelete(bannerid){
	swal({
		title:'Are You Sure?',
		text:'It Will Be Deleted Permanently!',
		type:'Warning',
		showCancelButton:true,
		confirmButtonColor:'#3085d6',
		cancelButtonColor:'#d33',
		confirmButtonText: 'Yes, delete it!',
		showLoaderOnConfirm:true,

		preConfirm: function(){
			return new Promise(function(resolve){
				 $.ajax({    //create an ajax request to load_page.php
			        type: "POST",
			        url: "api/delete_seo.php",
			        data:'delete='+bannerid,
			        dataType: 'json'
			})
				 .done(function(response){
				 	swal('Deleted!',response.message,response.status);
				 	// readProducts();
				 })
				 .fail(function(){
				 	swal('oops');
				 });
		});

	},
	allowOutsideClick:false
});
}
</script> -->

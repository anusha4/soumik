<?php include('addons/header.php'); ?>


</head>

<body>

	<!-- Main navbar -->
	<?php include('addons/navbar.php'); ?>
	<!-- /main navbar -->
	

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			
			<!-- /main sidebar -->
			<?php include('addons/left_side_bar.php'); ?>

			<!-- Main content -->
			<div class="content-wrapper">
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><span class="text-semibold"></span>VOLUNTEERS</h4>
						</div>
						
					</div>
				</div>
				<!-- Content area -->
				<div class="content">

					<!-- Main charts -->
					
					<!-- /main charts -->


					<!-- Dashboard content -->
					<div class="row">
						<div class="col-lg-12">

						
							<div class="panel panel-flat">
								
			                	<div class="col-lg-12 col-md-12 text-right"><a href="addvalountories.php"><button type="button" class="btn border-slate text-slate-800 add_btn btn-flat"><i class="fa fa-plus"> Add</i></button></a></div>
			                	<div class="clear"></div>
								<div class="panel-body">
									<div class="row table-responsive" id="volunteers_table">
										
									</div>
								</div>
							</div>
							<!-- /latest posts -->

						</div>

						
					</div>
					<!-- /dashboard content -->


					<!-- Footer -->
					<?php include("addons/footer.php") ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
<div class="modal fade" id="deletevolountriesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	 <div class="modal-dialog" role="document">
			<div class="modal-content">
				
					<div class="modal-header">						
						<h4 class="modal-title">Delete Volunteer</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <i class="fa fa-times-circle"></i><span aria-hidden="true"></span>
				        </button>
					</div>
					<div class="modal-body">					
						<p >Are you sure you want to delete <b class="text-warning "s><span id="get_name_here"></span>?</b></p>
						<!-- <p ></p> -->
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
						<input type="submit" class="btn btn-danger" data-dismiss="modal" value="Delete" onclick="deletevolunteer()">
					</div>
				
			</div>
		</div>
	</div>
	<script type="text/javascript">
	
	 $(document).ready(function() {

	 	    $.ajax({    //create an ajax request to load_page.php
        type: "POST",
        url: "api/getvolunteers.php", 
        //data: {readpro : 1} ,
        // dataType: 'json' ,         
          //expect html to be returned                
        success: function(data){  

            console.log(data);
            dataa=JSON.parse(data).volunteer;
            var volunteers = JSON.parse(data);
            var volunteer = volunteers.volunteer;

            var str='<table class="table table-bordered" id="volunteer_table"><thead><tr><th>SNo</th><th> Name</th><th> Image</th><th >Actions</th></tr></thead><tbody class="list_ads">';

            for(var i=0; i<volunteer.length; i++ ){

              	var j =i+1;
              	str+="<tr><td>" + j + "</td>" +
              		"<td>" + volunteer[i].name + "</td>" +  
              		"<td>" + volunteer[i].image + "</td>" + 
              		

              		"<td>" + "<a href='volountries_details.php?causeid=" + volunteer[i].id + "''><i  class='table_action bg-orange fa fa-eye' title='view'></i></a> "+
              		 "<a href='edit_volunteers.php?causeid=" + volunteer[i].id + "''><i class='table_action bg-blue fa fa-edit' title='Edit'></i></a> "+"<a href='#deletevolountriesModal' class='delete' data-toggle='modal'><i class='table_action bg-danger fa fa-trash deletevaluntories' data-id="+volunteer[i].id+" title='Delete'onclick='deleteVolunteers("+volunteer[i].id+")'></i></a>"+"</td>"

              		// "<td>" + "<button class='view_btn'>View </button> <button class='edit_btn'>Edit<button> <button class='delete_btn'>Delete </button>" + "</td>"
              	}

              	$("#volunteers_table").html(str+"</tbody></table>");
				 $('#volunteer_table').dataTable({
      			//paging: false
    		});
    		}
				});
	 	   });

            </script>

            <script>
document.getElementById('products').classList.add('active'); //add
</script>

</body>
</html>

<script type="text/javascript">
	function deleteVolunteers(delid){
		
		console.log("data is ",dataa);
		for(i=0;i<dataa.length;i++){
			if(dataa[i].id==delid){
				 // reqNameId=dataa[i].page;
				 reqNameId=document.getElementById("get_name_here").innerText=dataa[i].name;
				 reqSeoId=dataa[i].id;
				console.log("the page name is ",dataa[i].name)
			}
		}
	}
	function deletevolunteer(){
		console.log("delete is ",reqSeoId);
		window.location.href='api/deletevaluntories.php?del_id=' +reqSeoId+'';
		
	}
</script>

<!-- <script type="text/javascript">
	
	$(document).on('click', '.deletevaluntories', function(){

		//$(this).parent().parent().parent().remove();
		 var el = $(this);
		
		 var bannerid=$(this).data('id');
		 $.ajax({    //create an ajax request to load_page.php
        type: "POST",
        url: "api/deletevaluntories.php",
        data:{bannerid:bannerid},
                     
        success: function(data){ 

         var jsondata = JSON.parse(data);
         if(jsondata.status==1)
         {
//          	function JSalert(){
// // A confirm dialog
// alertify.confirm("Are you sure, you want to delete this file?", function (e) {
//     if (e) {
//         alertify.alert("File is Removed!");
//     } else {
//         alertify.alert("File is safe!");
//     }
// });
// }
         	alert("Successfully Deleted");
         	// location.reload();
         	el.parent().parent().parent().remove();
         
         }
         else
         {
         	alert("Failure");
         }

        }
        }); 
		
	});
</script> -->


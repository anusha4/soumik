<?php include('addons/header.php'); ?>
</head>
<body>

	<!-- Main navbar -->
<?php include('addons/navbar.php'); ?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<?php include('addons/left_side_bar.php'); ?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><span class="text-semibold"></span>Add New causes</h4>
						</div>
						<div class="heading-elements">
						<a href="causes.php"><button type="button" class="btn border-slates bg-coral btn-flats"><i class="fas fa-arrow-left"></i> Back</button></a>
						</div>
					</div>
				</div>
				<!-- Content area -->
				<div class="content">

					<!-- Main charts -->
					
					<!-- /main charts -->


					<!-- Dashboard content -->
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12">

						<!-- <div class="col-lg-12 col-md-12 text-right causes"><a href="causes.php"><button type="button" class="btn border-slates text-slate-800 btn-flats">Back</button></a></div> -->
							<div class="panel panel-flat">
								<div class="panel-heading">
									<!-- <h3 class="panel-title">Add New causes</h3> -->
									<div class="heading-elements">
										<!-- <ul class="icons-list">
					                		<li><a data-action="collapse"></a></li>
					                		<li><a data-action="reload"></a></li>
					                	</ul> -->
				                	</div>
			                	</div>

								<div class="panel-body">
									<div class="row add_user_form">
									<div class="col-lg-2 col-md-2 col-sm-2"></div>
									<div class="col-lg-8 col-md-8 col-sm-8">
										<div class="form-group">
										<label class="control-label col-lg-3"> Title</label>
										<div class="col-lg-9">
											<input type="text" id="ctitle" class="form-control" placeholder="Enter Cause title">
											<span class="errormsg" style="display:none">Please Enter Cause Title</span>
										</div>
										<div class="clear"></div>
										</div>


										<div class="form-group">
										<label class="control-label col-lg-3"> Description</label>
										<div class="col-lg-9">
											<textarea  id="cdesc" name="message" class="form-control" placeholder="Enter Cause description"></textarea>
											<span class="errormsg" style="display:none">Please Enter Cause Description</span>
										</div>
										<div class="clear"></div>
										</div>

										<!-- <div class="form-group">
										<label class="control-label col-lg-3">Username</label>
										<div class="col-lg-9">
											<input type="text" id="uname" class="form-control" placeholder="Enter Username">
										</div>
										<div class="clear"></div>
										</div> -->

										<div class="form-group">
										<label class="control-label col-lg-3"> Image</label>
										<div class="col-lg-9">
											<input type="file" style="display: none;" name="file[]" id="pimage" class="form-control" onchange="document.getElementById('profile_pic').src = window.URL.createObjectURL(this.files[0])" name="profilepic[]" accept="image/* ">

											<img src="../images/resource/avatar-img.png" width="270px" height="225px" id="profile_pic"><br><br>

											<script type="text/javascript">
												$('.form-group img').on('click',function(){
													$(this).siblings('input:file').click();
												});
											</script>
											<span class="errormsg" style="display:none">Please Select Image and please give valid size 1mb</span>
											<span class="successmessage"></span>
										</div>
										<div class="clear"></div>
										</div>





										<div class="col-md-12 text-center"><button id="add_causes" class="btn btn-success"><i class="fa fa-check"> Save</i></button></div>

									</div>
									<div class="col-lg-2 col-md-2 col-sm-2"></div>
									</div>
								</div>
							</div>
							<!-- /latest posts -->

						</div>

						
					</div>
					<!-- /dashboard content -->


					<!-- Footer -->
					<?php include("addons/footer.php") ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

	<script type="text/javascript">
			
		$("#add_causes").click(function()
		{
			 $('.errormsg').hide();
			var ctitle = $("#ctitle").val();
			var cdesc = $("#cdesc").val();
			var pimage = $("#pimage").val();
			var val = $("#pimage").val();
			var file = $('#pimage').prop("files")[0];

			

			// if(ctitle.length == 0 || cdesc.length == 0 || pimage.length == 0)
			// {
			// 	alert("Please enter all fields");
			// }

			var flag=false;
			 if(ctitle.length== 0)
          	{

          		$('#ctitle').siblings('.errormsg').show();
             //alert("Please enter atleast four letter name");  
             flag =true;  
             }

             else if (cdesc.length == 0)
              {  
             // alert("Please enter a valid E-mail Id ");  
             // return false; 

             $('#cdesc').siblings('.errormsg').show();
             //alert("Please enter atleast four letter name");  
             flag =true;   
              }
  
            else if (pimage.length==0 || !val.match(/(?:gif|jpg|png|bmp)$/) || file.size>1024000)
              {  
             // alert("Please enter a valid E-mail Id ");  
             // return false; 

             $('#pimage').siblings('.errormsg').show();
             //alert("Please enter atleast four letter name");  
             flag =true;   
              }

			else
			{
			$.ajax({

				url:"api/add_causes.php",
				type:"post",
				data:{ctitle:ctitle,cdesc:cdesc},

				success:function(data)
				{
					var jsondata = JSON.parse(data);

					if(jsondata.status == 1)
					{
						
						var slide_id=jsondata.slno;
						if(pimage.length==0){

									  			$('.successmessage').css('color','green').html('<i class="fa fa-check"><b>Successfully Added Redirecting..</b>');

									  		  setTimeout(function(){
									  		  	window.location="causes.php";


									  		}, 1000);
									  		}
						else{
						submitinfo(slide_id);
					}
					
					}

					
					else
					{
						alert("Error");
					}
				}

			});
		}
		
			});

		function submitinfo(slide_id)
		{
		    var form_data = new FormData();  
		    var file_data = $("#pimage").prop("files")[0]; 
		    for(var loopvar=0;loopvar<$("#pimage").prop("files").length;loopvar++)
		    {
		       var file_data = $("#pimage").prop("files")[loopvar]; 
		      form_data.append("file[]", file_data);
		    }
		    if($("#pimage").prop("files").length==0)
		      form_data.append("file[]", "");
		  
		  
		      form_data.append("productid", slide_id);
		      
		      $.ajax({
		            url: "api/addcause_image.php",
		            dataType: 'text',  // what to expect back from the PHP script, if anything
		            cache: false,
		            contentType: false,
		            processData: false,
		            data: form_data,                         
		            type: 'post',
		            success: function(php_script_response){
		              var jsondata=JSON.parse(php_script_response);
		                if(jsondata.status==1)
		                {
		                	$('.successmessage').css('color','green').html('<i class="fa fa-check"><b>Successfully Added Redirecting..</b>');

									  		  setTimeout(function(){
									  		  	window.location="causes.php";


									  		}, 1000);
		                }
		                else
		                {
		            	    alert("Failed");
		                }
		        }
		      });
		}


	</script>

	<script type="text/javascript">
            
            function isNumberKey(evt){
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
                return true;
            }

        </script>

       <!--  <script>
document.getElementById('add_user_menu').classList.add('active'); //add
</script> -->

</body>
</html>

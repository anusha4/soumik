<?php include('addons/header.php'); ?>
</head>
	
<body>

	<!-- Main navbar -->
	<?php include('addons/navbar.php'); ?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			
			<!-- /main sidebar -->
			<?php include('addons/left_side_bar.php'); ?>

			<!-- Main content -->
			<div class="content-wrapper">
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><span class="text-semibold"></span>DONATERS</h4>
						</div>
					</div>
				</div>
				<!-- Content area -->
				<div class="content">

					<!-- Main charts -->
					
					<!-- /main charts -->


					<!-- Dashboard content -->
					<div class="row">
						<div class="col-lg-12">

						
							<div class="panel panel-flat">
								
			                	<div class="clear"></div>
								<div class="panel-body">
									<div class="display row table-responsive" id="donaters_table">
							
									</div>
								</div>
							</div>
							<!-- /latest posts -->

						</div>

						
					</div>
					<!-- /dashboard content -->


					<!-- Footer -->
					<?php include("addons/footer.php") ?>
					<!-- /footer -->
					

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

	<script type="text/javascript">
	
	 $(document).ready(function() {

	 	    $.ajax({    //create an ajax request to load_page.php
        type: "POST",
        url: "api/getdonaters.php", 
        //data: {readpro : 1} ,
        // dataType: 'json' ,         
          //expect html to be returned                
        success: function(data){  

            console.log(data);

            var donaters = JSON.parse(data);
            var donaters_data = donaters.donaters_data;
            var str='<table id="all_donaters_table" class="table  table-bordered" style="white-space:nowrap"    width="100%"><thead><tr><th>SNo</th><th > Name</th><th> Email</th><th >MobileNo</th><th >Address</th><th >Amount</th><th >AddedDate</th><th class="text-center">Actions</th></tr></thead><tbody class="list_ads">'
            // var str='<table class="table datatable-basic" id="all_donaters_table"><thead><tr><th>SNo</th><th>Slide Name</th><th>Slide Title</th><th>Image</th><th class="text-center">Actions</th></tr></thead><tbody>';

            for(var i=0; i<donaters_data.length; i++ ){

              	var j =i+1;
              	str+="<tr><td>" + j + "</td>" +
              		"<td>" + donaters_data[i].name + "</td>" +
              		"<td>" + donaters_data[i].email + "</td>" +  
              		"<td>" + donaters_data[i].mobileno + "</td>" + 
              		"<td>" + donaters_data[i].address +","+"</br>"+donaters_data[i].address1+","+"</br>"+donaters_data[i].address2+","+"</br>"+donaters_data[i].address3+","+"</br>"+donaters_data[i].address4+"</td>" + 
              		"<td>" + donaters_data[i].donattedamt + "</td>" + 
              		"<td>" + donaters_data[i].donationaddeddate + "</td>" + 
              		

              		

              		"<td class='text-center'>" + "<a href='donaters_details.php?slideid=" + donaters_data[i].id + "''><i  class='table_action bg-orange fa fa-eye' title='view'></i></a> "+
              		 "</td>"

              		// "<td>" + "<button class='view_btn'>View </button> <button class='edit_btn'>Edit<button> <button class='delete_btn'>Delete </button>" + "</td>"
              	}

              	$("#donaters_table").html(str+"</tbody></table>");
				 $('#all_donaters_table').dataTable({
      			//paging: false
    		});
    		}
				});
	 	   });

            </script>

            <script>
document.getElementById('products').classList.add('active'); //add
</script>

</body>
</html>
<script type="text/javascript">
	$(document).ready(function () {
  $('#all_donaters_table').DataTable();
  $('.dataTables_length').addClass('bs-select');
});
	$(document).ready(function () {
  $('#all_donaters_table').DataTable({
    "paging": false // false to disable pagination (or any other option)
  });
  $('.dataTables_length').addClass('bs-select');
});
	// Basic example
$(document).ready(function () {
  $('#all_donaters_table').DataTable({
    "pagingType": "simple" // "simple" option for 'Previous' and 'Next' buttons only
  });
  $('.dataTables_length').addClass('bs-select');
});
</script>


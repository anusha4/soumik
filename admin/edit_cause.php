<?php include('addons/header.php'); ?>
<?php
include("./api/db.php");

$causeid = $_GET['causeid'];

$query = mysqli_query($con,"SELECT * FROM home_causes WHERE slno = '$causeid'");

if(mysqli_num_rows($query)>0)
{
	$data = mysqli_fetch_assoc($query);


	$id = $data['slno'];
	$title = $data['cause_title'];
	$desc = $data['cause_desc'];
	$image = $data['cause_image'];

	

}


?>


</head>

<body>

	<!-- Main navbar -->
	<?php include('addons/navbar.php'); ?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			
			<!-- /main sidebar -->
			<?php include('addons/left_side_bar.php'); ?>

			<!-- Main content -->
			<div class="content-wrapper">
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><span class="text-semibold"></span>Edit Cause</h4>
						</div>
						<div class="heading-elements">
						<a href="causes.php"><button type="button" class="btn border-slates bg-coral btn-flats"><i class="fas fa-arrow-left"></i> Back</button></a>
						</div>
					</div>
				</div>
				<!-- Content area -->
				<div class="content">

					<!-- Main charts -->
					
					<!-- /main charts -->


					<!-- Dashboard content -->
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12">

						
							<div class="panel panel-flat">
								<div class="panel-heading">
									<!-- <h6 class="panel-title">Edit Cause</h6> -->
									<div class="heading-elements">
										<ul class="icons-list">
											<!-- <a href="causes.php"><button type="button" class="btn border-slates text-slate-800 btn-flats">Back</button></a> -->
					                		<!-- <li><a data-action="collapse"></a></li>
					                		<li><a data-action="reload"></a></li> -->
					                	</ul>
				                	</div>
			                	</div>

								<div class="panel-body">
									<div class="row add_user_form">
									<div class="col-lg-2 col-md-2 col-sm-2"></div>
									<div class="col-lg-8 col-md-8 col-sm-8">
										<div class="form-group">
										<label class="control-label col-lg-3">Title</label>
										<div class="col-lg-9">
											<input type="text" id="ctitle" class="form-control" placeholder="Enter cause Name" value="<?php echo $title; ?>">
											<span class="errormsg" style="display:none">Please Enter Cause Title</span>
										</div>
										<div class="clear"></div>
										</div>

										<div class="form-group">
										<label class="control-label col-lg-3">Description</label>
										<div class="col-lg-9">
											<textarea name="message" id="cdesc" class="form-control" placeholder="Enter cause desc" value="<?php echo $desc; ?>"><?php echo $desc;?></textarea>
											<span class="errormsg" style="display:none">Please Enter Cause Description</span>
										</div>
										<div class="clear"></div>
										</div>

										


										<div class="form-group">
										<label class="control-label col-lg-3">Image</label>
										<div class="col-lg-9">
											<!-- <input type="file" id="pimage" class="form-control" placeholder="upload Image"> -->

									<input type="file" style="display: none;" name="file[]" id="pimage" class="form-control" onchange="document.getElementById('profile_pic').src = window.URL.createObjectURL(this.files[0])" name="profilepic[]" accept="image/* ">

											<?php
									if($image!='')
									{

									?>
											<img src="../images/resource/<?php echo $image; ?>" width="200px" height="220px" id="profile_pic"><br><br>
											

											<?php
										}
										else
										{
										?>
										<img src="../images/resource/avatar-img.png" width="200px" height="220px" id="profile_pic"><br><br>

										<?php
									}
									?>
									<span class ="uploaded_file_name"></span>

											<script type="text/javascript">
												$('.form-group img').on('click',function(){
													$(this).siblings('input:file').click();
												});
											</script>
											<span class="errormsg" style="display:none">Please Select Image and please give valid size 1mb</span>
											<span class="successmessage"></span>

										</div>
										<div class="clear"></div>
										</div>

										<div class="col-md-12 text-center"><button type="button" class="btn bg-darkcyan" id="add_new_pro"><i class="fa fa-check"></i> Update </button></div>

									</div>
									<div class="col-lg-2 col-md-2 col-sm-2"></div>
									</div>
								</div>
							</div>
							<!-- /latest posts -->

						</div>

						
					</div>
					<!-- /dashboard content -->


					<!-- Footer -->
				<?php include("addons/footer.php") ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

	<script type="text/javascript">
		
		$("#add_new_pro").click(function(event)
		{
			 $('.errormsg').hide();
			var id = "<?php echo $causeid; ?>";
			var ctitle = $("#ctitle").val();
			var cdesc = $("#cdesc").val();
			var image = $("#pimage").val();
			// var file = $('#pimage').prop("files")[0];
			var url = $('#profile_pic').attr('src');
			var isLastSlash = (url[url.length -1]=="/")? true: false;
			var url= url.split("/");
			var avatorimage = url[url.length - (isLastSlash? 2: 1)];


			var flag=false;

			 if(ctitle.length== 0)
          	{

          		$('#ctitle').siblings('.errormsg').show();
             //alert("Please enter atleast four letter name");  
             flag =true;  
             }

             else if (cdesc.length == 0)
              {  
             // alert("Please enter a valid E-mail Id ");  
             // return false; 

             $('#cdesc').siblings('.errormsg').show();
             //alert("Please enter atleast four letter name");  
             flag =true;   
              }
  
            else if(avatorimage=='avatar-img.png'){
              	$('#pimage').siblings('.errormsg').show();
             //alert("Please enter atleast four letter name");  
             flag =true;   
              }

			else
			{

			$.ajax({
                              type:"post",
                              url:"api/edit_cause.php",
                              data:{id:id,ctitle:ctitle,cdesc:cdesc},

                              success:function(data){
                              	jsondata = JSON.parse(data);


									  	if(jsondata.status == 1)
									  	{

									  		if(image.length==0)
									  		{
									  			$('.successmessage').css('color','green').html('<i class="fa fa-check"><b>Successfully updated Redirecting..</b>');

									  		  setTimeout(function(){
									  		  	window.location="causes.php";


									  		}, 1000);
									  			
									  		}

									  		else
									  		{
									  			// alert("image selected");
									  			submitinfo(id);
									  		}

									  		

									  		//alert("Product Added Successfully");
									  		
											
									  	}

									  	else
									  	{
									  		alert("Error");
									  	}

                               
                            }


                        });
		}
		});



		function submitinfo(slide_id)
		{
		    var form_data = new FormData();  
		    var file_data = $("#pimage").prop("files")[0]; 
		    for(var loopvar=0;loopvar<$("#pimage").prop("files").length;loopvar++)
		    {
		       var file_data = $("#pimage").prop("files")[loopvar]; 
		      form_data.append("file[]", file_data);
		    }
		    if($("#pimage").prop("files").length==0)
		      form_data.append("file[]", "");
		  
		  
		      form_data.append("productid", slide_id);
		      
		      $.ajax({
		            url: "api/editcause_image.php",
		            dataType: 'text',  // what to expect back from the PHP script, if anything
		            cache: false,
		            contentType: false,
		            processData: false,
		            data: form_data,                         
		            type: 'post',
		            success: function(php_script_response){
		              var jsondata=JSON.parse(php_script_response);
		                if(jsondata.status==1)
		                {
		                	$('.successmessage').css('color','green').html('<i class="fa fa-check"><b>Successfully updated Redirecting..</b>');

									  		  setTimeout(function(){
									  		  	window.location="causes.php";


									  		}, 1000);
		                }
		                else
		                {
		            	    alert("Failed");
		                }
		        }
		      });
		}


	</script>
	<script type="text/javascript">
		$('#pimage').on('change',function()
		{

			
			var image = $("#pimage").val();

			//alert(val);
           var file = $('#pimage').prop("files")[0];


			//alert('hi');
			if (!image.match(/(?:gif|png|jpg|bmp)$/))
                                                 {
    
                                                 alert("inputted file path is not an image!");
                                                 }
    										   else
    										 if(file.size>1024000)
                                                 {
                                                 alert('please give valid size 1mb');
                                                  }

		});
	</script>
	<script type="text/javascript">
            
            function isNumberKey(evt){
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
                return true;
            }

        </script>

</body>
</html>

<?php include('addons/header.php'); ?>
<!-- Theme JS files -->
	<script type="text/javascript" src="assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/forms/selects/select2.min.js"></script>
	

	<script type="text/javascript" src="assets/js/core/app.js"></script>
	<script type="text/javascript" src="assets/js/pages/datatables_advanced.js"></script>
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="assets/css/extrastyles.css">
	<script type="text/javascript" src="tinymce/jquery.tinymce.min.js"></script>
	<script type="text/javascript" src="tinymce/tinymce.min.js">
	
	</script>
		<link rel="stylesheet" type="text/css" href="assets/css/amsify.suggestags.css">

		<!-- Amsify Plugin -->
		<script type="text/javascript" src="assets/js/jquery.amsify.suggestags.js"></script>
	<!-- /theme JS files -->
</head>
<body>

	<!-- Main navbar -->
    <?php include('addons/navbar.php'); ?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<?php include('addons/left_side_bar.php'); ?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><span class="text-semibold"></span>Add Seo</h4>
						</div>
						<div class="heading-elements">
						<a href="seo.php"><button type="button" class="btn border-slates bg-coral btn-flats"><i class="fas fa-arrow-left"></i> Back</button></a>
						</div>
					</div>
				</div>
				<!-- Content area -->
				<div class="content">

					<!-- Main charts -->
					
					<!-- /main charts -->


					<!-- Dashboard content -->
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12">

						<!-- <div class="col-lg-12 col-md-12 text-right causes"><a href="causes.php"><button type="button" class="btn border-slates text-slate-800 btn-flats">Back</button></a></div> -->
							<div class="panel panel-flat">
								<div class="panel-heading">
									<!-- <h3 class="panel-title">Add New causes</h3> -->
									<div class="heading-elements">
										<ul class="icons-list">
					                		<!-- <li><a data-action="collapse"></a></li>
					                		<li><a data-action="reload"></a></li> -->
					                	</ul>
				                	</div>
			                	</div>

								<div class="panel-body">
									<div class="row add_user_form">
									<div class="col-lg-2 col-md-2 col-sm-2"></div>
									<div class="col-lg-8 col-md-8 col-sm-8">
										<div class="form-group">
										<label class="control-label col-lg-3"> Page</label>
										<div class="col-lg-9">
											<input type="text" id="cpage" class="form-control" placeholder="Enter Seopage">
											<span class="errormsg" style="display:none"> please Enter Seo Page  Name</span>
										</div>
										<div class="clear"></div>
										</div>

										<div class="form-group">
										<label class="control-label col-lg-3"> Title</label>
										<div class="col-lg-9">
											<input type="text" id="ctitle" class="form-control" placeholder="Enter Seotitle">
											<span class="errormsg" style="display:none">Please Enter Seo Page Title</span>
										</div>
										<div class="clear"></div>
										</div>

										<div class="form-group">
										<label class="control-label col-lg-3"> keywords</label>
										<div class="col-lg-9">
											<input type="text" id="ckey" name="ckey" class="form-control" placeholder="Enter Seokeywords">
											<span class="errormsg" style="display:none">After Type the keywords press Enter key</span>
										</div>
										<div class="clear"></div>
										</div>

										<div class="form-group">
										<label class="control-label col-lg-3"> Description</label>
										<div class="col-lg-9">
											<textarea  id="cdesc" class="form-control" placeholder="Enter Seodescription" ></textarea>
											<span class="errormsg" style="display:none">Please Enter Minimum 5 letters  of Description</span>
											<span class="successmessage"></span>
										</div>
										<div class="clear"></div>
										</div>
										<div class="col-md-12 text-center"><button id="add_seo" class="btn btn-success"><i class="fa fa-check"> Save</i></button></div>

									</div>
									<div class="col-lg-2 col-md-2 col-sm-2"></div>
									</div>
								</div>
							</div>
							<!-- /latest posts -->

						</div>

						
					</div>
					<!-- /dashboard content -->


					<!-- Footer -->
					<?php include("addons/footer.php") ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

	<script type="text/javascript">
			
		$("#add_seo").click(function()
		{
			$('.errormsg').hide();
			var cpage = $("#cpage").val();
			var ctitle = $("#ctitle").val();
			var ckey = $("#ckey").val();
			// var cdesc = $("textarea#cdesc").val();
			var cdesc= tinymce.get('cdesc').getContent();
			// alert(desc);
			var flag=false;
			if (cpage.length == 0)
              {  

	             $('#cpage').siblings('.errormsg').show();
	              flag =true;   
              }
  
            else if (ctitle.length==0)
              {  
	             $('#ctitle').siblings('.errormsg').show();
	              flag =true;   
              }
               else if (ckey.length==0)
              {  
	             $('#ckey').siblings('.errormsg').show();
	              flag =true;   
              }
               else if (cdesc.length==0)
              {  
	             $('#cdesc').siblings('.errormsg').show();
	              flag =true;   
              }

			// if(cpage.length == 0 || ctitle.length == 0 )
			// {
			// 	alert("Please enter all fields");
			// }
			// else if (ckey.length == 0){
			// 	alert("After Type the keywords press Enter key");
			// }
			// else if (cdesc.length == 0){
			// 	alert("Please Enter Minimum 5 letters");
			// }
			// else if (cdesc.length < 5){
			// 	alert("Please Enter Minimum 5 letters");
			// }
			else
			{
			$.ajax({

				url:"api/add_seo.php",
				type:"post",
				data:{cpage:cpage,ctitle:ctitle,ckey:ckey,cdesc:cdesc},

				success:function(data)
				{
					var jsondata = JSON.parse(data);

					if(jsondata.status == 1)
					{
						
						// var causeid=jsondata.slno;
						$('.successmessage').css('color','green').html('<i class="fa fa-check"><b>Successfully Added Redirecting..</b>');

									  		  setTimeout(function(){
									  		  	window.location="seo.php";


									  		}, 1000);
						
					}

					
					else
					{
						alert("Error");
					}
				}

			});
		}
		
			});

		


	</script>

	<script type="text/javascript">
            
            function isNumberKey(evt){
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
                return true;
            }

        </script>

       <!--  <script>
document.getElementById('add_user_menu').classList.add('active'); //add
</script> -->

</body>
</html>
<script>
	$('input[name="ckey"]').amsifySuggestags({
		type : 'amsify'
	});
	tinymce.init({
		selector: "#cdesc",
		plugins: [
			"textcolor colorpicker link image",
			"searchreplace visualblocks code fullscreen",
			"insertdatetime media table contextmenu paste"
		],
		toolbar: "insertfile undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link forecolor backcolor | link image"
	});
	// $(".form-horizontal").validate();
</script>


<?php include('addons/header.php'); ?>
</head>
	
<body>

	<!-- Main navbar -->
    <?php include('addons/navbar.php'); ?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<?php include('addons/left_side_bar.php'); ?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><span class="text-semibold"></span>Add Event</h4>
						</div>
						<div class="heading-elements">
						<a href="events.php"><button type="button" class="btn border-slates bg-coral btn-flats"><i class="fas fa-arrow-left"></i> Back</button></a>
						</div>
					</div>
				</div>
				<!-- Content area -->
				<div class="content">

					<!-- Main charts -->
					
					<!-- /main charts -->


					<!-- Dashboard content -->
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12">

						<!-- <div class="col-lg-12 col-md-12 text-right causes"><a href="events.php"><button type="button" class="btn border-slates text-slate-800 btn-flats">Back</button></a></div> -->
							<div class="panel panel-flat">
								<div class="panel-heading">
									<!-- <h3 class="panel-title">Add Event</h3> -->
									<div class="heading-elements">
										<!-- <ul class="icons-list">
					                		<li><a data-action="collapse"></a></li>
					                		<li><a data-action="reload"></a></li>
					                	</ul> -->
				                	</div>
			                	</div>

								<div class="panel-body">
									<div class="row add_user_form">
									<div class="col-lg-2 col-md-2 col-sm-2"></div>
									<div class="col-lg-8 col-md-8 col-sm-8">
										<div class="form-group">
										<label class="control-label col-lg-3">Title</label>
										<div class="col-lg-9">
											<input type="text" id="etitle" class="form-control" placeholder="Enter Event title">
											<span class="errormsg" style="display:none">Enter Event Title</span>
										</div>
										<div class="clear"></div>
										</div>


										<div class="form-group">
										<label class="control-label col-lg-3">Description</label>
										<div class="col-lg-9">
											<textarea id="edesc" class="form-control" placeholder="Enter Event Description"></textarea>
											<span class="errormsg" style="display:none">Enter Event Description</span>
										</div>
										<div class="clear"></div>
										</div>
										<div class="form-group">
										<label class="control-label col-lg-3">City</label>
										<div class="col-lg-9">
											<input type="text" id="city" class="form-control" placeholder="Enter Event City">
											<span class="errormsg" style="display:none">Enter Event City</span>
										</div>
										<div class="clear"></div>
										</div>
										<div class="form-group">
										<label class="control-label col-lg-3">Date</label>
										<div class="col-lg-9">
											<input type="date" id="date" class="form-control" placeholder="Enter Event date">
											<span class="errormsg" style="display:none">Enter Event Date</span>
										</div>
										<div class="clear"></div>
										</div>
										<div class="form-group">
										<label class="control-label col-lg-3">FromTime</label>
										<div class="col-lg-6">
											<input type="text" id="eftime" class="form-control">
											<span class="errormsg" style="display:none">Enter FromTime</span>
										</div>
										<div class="col-lg-3">
											<select id="cfromtimeselect">
												<option value="Am">am</option>
												<option value="Pm">pm</option>
											</select>
											<span class="errormsg" style="display:none">Select any One</span>
										</div>
										<div class="clear"></div>
										</div>
										<div class="form-group">
										<label class="control-label col-lg-3">Totime</label>
										<div class="col-lg-6">
											<input type="text" id="ettime" class="form-control">
											<span class="errormsg" style="display:none">Enter Totime</span>
										</div>
										<div class="col-lg-3">
											<select id="ctotimeselect">
												<option value="Am">am</option>
												<option value="Pm">pm</option>
											</select>
											<span class="errormsg" style="display:none">Select any One</span>
										</div>
										<div class="clear"></div>
										</div>


										<div class="form-group">
										<label class="control-label col-lg-3">Image</label>
										<div class="col-lg-9">
											<input type="file" style="display: none;" name="file[]" id="pimage" class="form-control" onchange="document.getElementById('profile_pic').src = window.URL.createObjectURL(this.files[0])" name="profilepic[]" accept="image/* ">
												
											<img src="../images/resource/avatar-img.png" title="Select Image" width="270px" height="225px" id="profile_pic"><br><br>
											<script type="text/javascript">
												$('.form-group img').on('click',function(){
													$(this).siblings('input:file').click();
												});
											</script>
											

									<span class="errormsg" style="display:none">Please Select Image and please give valid size 1mb</span>
									<span class="successmessage"></span>
										</div>
										<div class="clear"></div>
										</div>






										<div class="col-md-12 text-center"><button id="add_events" class="btn btn-success"><i class="fa fa-check"> Save</i></button></div>

									</div>
									<div class="col-lg-2 col-md-2 col-sm-2"></div>
									</div>
								</div>
							</div>
							<!-- /latest posts -->

						</div>

						
					</div>
					<!-- /dashboard content -->


					<!-- Footer -->
					<?php include("addons/footer.php") ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

	<script type="text/javascript">
			
		$("#add_events").click(function()
		{
			$('.errormsg').hide();
			var etitle = $("#etitle").val();

			var edesc = $("#edesc").val();
			var city = $("#city").val();
			var date = $("#date").val();
			var eftime = $("#eftime").val();
			var ettime = $("#ettime").val();
			var pimage = $("#pimage").val();
			var val = $("#pimage").val();
			var file = $('#pimage').prop("files")[0];
			var cfromtimeselect = $("#cfromtimeselect").val();
			var ctotimeselect = $("#ctotimeselect").val();
			
			

			// if(etitle.length == 0 || edesc.length == 0 || city.length == 0  || cfromtimeselect.length == 0 ||ctotimeselect.length == 0 ||  date.length == 0 || eftime.length == 0|| ettime.length == 0 || pimage.length == 0)
			// {
			// 	alert("Please enter all fields");
			// }
			var flag=false;
			if (etitle.length == 0)
              {  

	             $('#etitle').siblings('.errormsg').show();
	              flag =true;   
              }
  
            else if (edesc.length==0)
              {  
	             $('#edesc').siblings('.errormsg').show();
	              flag =true;   
              }
              else if (city.length==0)
              {  
	             $('#city').siblings('.errormsg').show();
	              flag =true;   
              }

              else if (date.length==0)
              {  
	             $('#date').siblings('.errormsg').show();
	              flag =true;   
              }

              else if (eftime.length==0)
              {  
	             $('#eftime').siblings('.errormsg').show();
	              flag =true;   
              }

              else if (ettime.length==0)
              {  
	             $('#ettime').siblings('.errormsg').show();
	              flag =true;   
              }
              else if (pimage.length==0 || !val.match(/(?:gif|jpg|png|bmp)$/) || file.size>1024000)
              {  
	             $('#pimage').siblings('.errormsg').show();
	              flag =true;   
              }
              else if (cfromtimeselect.length==0)
              {  
	             $('#cfromtimeselect').siblings('.errormsg').show();
	              flag =true;   
              }
              else if (ctotimeselect.length==0)
              {  
	             $('#ctotimeselect').siblings('.errormsg').show();
	              flag =true;   
              }

			else
			{

			$.ajax({

				url:"api/add_events.php",
				type:"post",
				data:{etitle:etitle,edesc:edesc,city:city,date:date,ettime:ettime,eftime:eftime,cfromtimeselect:cfromtimeselect,ctotimeselect:ctotimeselect},

				success:function(data)
				{
					var jsondata = JSON.parse(data);

					if(jsondata.status == 1)
					{
						
						var slide_id=jsondata.slno;
						if(pimage.length==0){
												$('.successmessage').css('color','green').html('<i class="fa fa-check"><b>Successfully Added Redirecting..</b>');

									  		  setTimeout(function(){
									  		  	window.location="events.php";


									  		}, 1000);
									  			
									  		}
						else{
						submitinfo(slide_id);
					}
					
					}

					
					else
					{
						alert("Error");
					}
				}

			});
		}
		
			});

		function submitinfo(slide_id)
		{
		    var form_data = new FormData();  
		    var file_data = $("#pimage").prop("files")[0]; 
		    for(var loopvar=0;loopvar<$("#pimage").prop("files").length;loopvar++)
		    {
		       var file_data = $("#pimage").prop("files")[loopvar]; 
		      form_data.append("file[]", file_data);
		    }
		    if($("#pimage").prop("files").length==0)
		      form_data.append("file[]", "");
		  
		  
		      form_data.append("productid", slide_id);
		      
		      $.ajax({
		            url: "api/addevent_image.php",
		            dataType: 'text',  // what to expect back from the PHP script, if anything
		            cache: false,
		            contentType: false,
		            processData: false,
		            data: form_data,                         
		            type: 'post',
		            success: function(php_script_response){
		              var jsondata=JSON.parse(php_script_response);
		                if(jsondata.status==1)
		                {
		                	$('.successmessage').css('color','green').html('<i class="fa fa-check"><b>Successfully Added Redirecting..</b>');

									  		  setTimeout(function(){
									  		  	window.location="events.php";


									  		}, 1000);
		                }
		                else
		                {
		            	    alert("Failed");
		                }
		        }
		      });
		}


	</script>

	<script type="text/javascript">
            
            function isNumberKey(evt){
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
                return true;
            }

        </script>

       <!--  <script>
document.getElementById('add_user_menu').classList.add('active'); //add
</script> -->

</body>
</html>

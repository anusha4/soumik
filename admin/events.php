<?php include('addons/header.php'); ?>

</head>

<body>

	<!-- Main navbar -->
	<?php include('addons/navbar.php'); ?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			
			<!-- /main sidebar -->
			<?php include('addons/left_side_bar.php'); ?>

			<!-- Main content -->
			<div class="content-wrapper">
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><span class="text-semibold"></span>EVENTS</h4>
						</div>
					</div>
				</div>
				<!-- Content area -->
				<div class="content">

					<!-- Main charts -->
					
					<!-- /main charts -->


					<!-- Dashboard content -->
					<div class="row">
						<div class="col-lg-12">

						
							<div class="panel panel-flat">
								<div class="panel-heading">
									<!-- <h3 class="panel-title">EVENTS</h3> -->
									<div class="heading-elements">
										<!-- <ul class="icons-list">
					                		<li><a data-action="collapse"></a></li>
					                		<li><a data-action="reload"></a></li>
					                	</ul> -->
				                	</div>
			                	</div>
			                	<div class="col-lg-12 col-md-12 text-right"><a href="addevent.php"><button type="button" class="btn border-slate text-slate-800 add_btn btn-flat"><i class="fa fa-plus"> Add</i></button></a></div>

			                	<div class="clear"></div>
								<div class="panel-body">
									<div class="row table-responsive" id="event">
									
										
									</div>
								</div>
							</div>
							<!-- /latest posts -->

						</div>

						
					</div>
					<!-- /dashboard content -->


					<!-- Footer -->
					<?php include("addons/footer.php") ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
<div class="modal fade" id="deleteEventModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	 <div class="modal-dialog" role="document">
			<div class="modal-content">
				
					<div class="modal-header">						
						<h4 class="modal-title">Delete Events</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <i class="fa fa-times-circle"></i><span aria-hidden="true"></span>
				        </button>
					</div>
					<div class="modal-body">					
						<p >Are you sure you want to delete <b class="text-warning "s><span id="get_name_here"></span>?</b></p>
						<!-- <p ></p> -->
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
						<input type="submit" class="btn btn-danger" data-dismiss="modal" value="Delete" onclick="deleteevent()">
					</div>
				
			</div>
		</div>
	</div>
	<script type="text/javascript">
	
	 $(document).ready(function() {

	 	    $.ajax({    //create an ajax request to load_page.php
        type: "POST",
        url: "api/getevents.php", 
        //data: {readpro : 1} ,
        // dataType: 'json' ,         
          //expect html to be returned                
        success: function(data){  

            console.log(data);
            	dataa=JSON.parse(data).causes;
            var causes1 = JSON.parse(data);
            var causes = causes1.causes;

            var str='<table class="table table-bordered"  id="event_table"><thead><tr><th>SNo</th><th> Title</th><th> Desc</th><th> Image</th><th class="text-center">Actions</th></tr></thead><tbody class="list_ads">';

            for(var i=0; i<causes.length; i++ ){

              	var j =i+1;
              	str+="<tr><td>" + j + "</td>" +
              		"<td>" + causes[i].title + "</td>" +  
              		"<td>" + causes[i].desc + "</td>" + 
              		"<td>" + causes[i].image + "</td>" + 
              		

              		"<td style='white-space: nowrap;'>" + 
              		"<a href='event_details.php?causeid=" + causes[i].id + "''><i  class='table_action bg-orange fa fa-eye' title='view'></i></a> "+
              		 "<a href='edit_event.php?causeid=" + causes[i].id + "''><i class='table_action bg-blue fa fa-edit' title='Edit'></i></a> "+"<a href='#deleteEventModal' class='delete' data-toggle='modal'><i class='table_action bg-danger fa fa-trash deleteevent' data-id="+causes[i].id+" title='Delete'onclick='deleteEvent("+causes[i].id+")'></i></a>"+"</td>"
              		// "<td>" + "<button class='view_btn'>View </button> <button class='edit_btn'>Edit<button> <button class='delete_btn'>Delete </button>" + "</td>"
              	}

              	$("#event").html(str+"</tbody></table>");
				 $('#event_table').dataTable({
      			//paging: false
    		});
    		}
				});
	 	   });

            </script>

            <script>
document.getElementById('products').classList.add('active'); //add
</script>

</body>
</html>

<script type="text/javascript">
	function deleteEvent(delid){
		
		console.log("data is ",dataa);
		for(i=0;i<dataa.length;i++){
			if(dataa[i].id==delid){
				 // reqNameId=dataa[i].page;
				 reqNameId=document.getElementById("get_name_here").innerText=dataa[i].title;
				 reqSeoId=dataa[i].id;
				console.log("the page name is ",dataa[i].title)
			}
		}
	}
	function deleteevent(){
		console.log("delete is ",reqSeoId);
		window.location.href='api/deleteevent.php?del_id=' +reqSeoId+'';
		
	}
</script>


<!-- <script type="text/javascript">
	
	$(document).on('click', '.deleteevent', function(){

		//$(this).parent().parent().parent().remove();
		 var el = $(this);
		
		 var bannerid=$(this).data('id');
		 $.ajax({    //create an ajax request to load_page.php
        type: "POST",
        url: "api/deleteevent.php",
        data:{bannerid:bannerid},
                     
        success: function(data){ 

         var jsondata = JSON.parse(data);
         if(jsondata.status==1)
         {
			alert("Successfully Deleted");
         	// location.reload();
         	el.parent().parent().parent().remove();
         
         }
         else
         {
         	alert("Failure");
         }

        }
        }); 
		
	});
</script> -->

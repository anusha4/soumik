<?php include('addons/header.php'); ?>

</head>

<body>

	<!-- Main navbar -->
	<?php include('addons/navbar.php'); ?>
	<!-- /main navbar -->
	

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			
			<!-- /main sidebar -->
			<?php include('addons/left_side_bar.php'); ?>

			<!-- Main content -->
			<div class="content-wrapper">
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><span class="text-semibold"></span>SUBSCRIBERS</h4>
						</div>
					</div>
				</div>
				<!-- Content area -->
				<div class="content">

					<!-- Main charts -->
					
					<!-- /main charts -->


					<!-- Dashboard content -->
					<div class="row">
						<div class="col-lg-12">

						
							<div class="panel panel-flat">
								
			                	
			                	<div class="clear"></div>
								<div class="panel-body">
									<div class="display row table-responsive" id="subscribers_table">
							
									</div>
								</div>
							</div>
							<!-- /latest posts -->

						</div>

						
					</div>
					<!-- /dashboard content -->


					<!-- Footer -->
					<?php include("addons/footer.php") ?>
					<!-- /footer -->
					

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
<!-- Delete Modal HTML -->
<div class="modal fade" id="deleteSubscribeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	 <div class="modal-dialog" role="document">
			<div class="modal-content">
				
					<div class="modal-header">						
						<h4 class="modal-title">Delete Subscriber</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
					</div>
					<div class="modal-body">					
						<p >Are you sure you want to delete <b class="text-warning "s><span id="get_name_here"></span>?</b></p>
						<!-- <p ></p> -->
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
						<input type="submit" class="btn btn-danger" data-dismiss="modal" value="Delete" onclick="deletesubscribe()">
					</div>
				
			</div>
		</div>
	</div>
	<script type="text/javascript">
	
	 $(document).ready(function() {

	 	    $.ajax({    //create an ajax request to load_page.php
        type: "POST",
        url: "api/getsubscribers.php", 
        //data: {readpro : 1} ,
        // dataType: 'json' ,         
          //expect html to be returned                
        success: function(data){  

            console.log(data);
            dataa=JSON.parse(data).subscribe_data;
           	var subs = JSON.parse(data);
            var subscribe_data = subs.subscribe_data;
            var str='<table id="subscribetable" class="table  table-bordered" width="100%" ><thead><tr><th>SNo</th><th >Email</th><th> Status</th><th >AddedOn</th><th class="text-center">Actions</th></tr></thead><tbody class="list_ads">'
            // var str='<table class="table datatable-basic" id="all_donaters_table"><thead><tr><th>SNo</th><th>Slide Name</th><th>Slide Title</th><th>Image</th><th class="text-center">Actions</th></tr></thead><tbody>';

            for(var i=0; i<subscribe_data.length; i++ ){

              	var j =i+1;
              	str+="<tr><td>" + j + "</td>" +
              		"<td>" + subscribe_data[i].email + "</td>" +
              		"<td>" + subscribe_data[i].status + "</td>" +  
              		"<td>" + subscribe_data[i].addedon + "</td>" + 
              		
              		
              		"<td style='white-space: nowrap;'>" +"<a href='#deleteSubscribeModal' class='delete' data-toggle='modal'><i class='table_action fa fa-trash bg-danger' data-id="+subscribe_data[i].id+" title='Delete' onclick='deleteSubsc("+subscribe_data[i].id+")'></i></a>"+"</td>"
              		

              		// "<td>" + "<button class='view_btn'>View </button> <button class='edit_btn'>Edit<button> <button class='delete_btn'>Delete </button>" + "</td>"
              	}

              	$("#subscribers_table").html(str+"</tbody></table>");
				 $('#subscribetable').dataTable({
      			//paging: false
    		});
    		}
				});
	 	   });

            </script>

          <!--   <script>

document.getElementById('products').classList.add('active'); //add
</script> -->
<script type="text/javascript">
	function deleteSubsc(delid){
		
		console.log("data is ",dataa);
		for(i=0;i<dataa.length;i++){
			if(dataa[i].id==delid){
				 // reqNameId=dataa[i].page;
				 reqNameId=document.getElementById("get_name_here").innerText=dataa[i].email;
				 reqSeoId=dataa[i].id;
				console.log("the page name is ",dataa[i].email)
			}
		}
	}
	function deletesubscribe(){
		console.log("delete is ",reqSeoId);
		window.location.href='api/deletesubscribe.php?del_id=' +reqSeoId+'';
		
	}
</script>
</body>
</html>
<script type="text/javascript">
	$(document).ready(function () {
  $('#subscribetable').DataTable();
  $('.dataTables_length').addClass('bs-select');
});
	$(document).ready(function () {
  $('#subscribetable').DataTable({
    "paging": false // false to disable pagination (or any other option)
  });
  $('.dataTables_length').addClass('bs-select');
});
	// Basic example
$(document).ready(function () {
  $('#subscribetable').DataTable({
    "pagingType": "simple" // "simple" option for 'Previous' and 'Next' buttons only
  });
  $('.dataTables_length').addClass('bs-select');
});
</script>


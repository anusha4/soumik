<?php include('addons/header.php'); ?>
<?php
include("./api/db.php");

$causeid = $_GET['causeid'];

$query = mysqli_query($con,"SELECT * FROM home_events WHERE events_id = '$causeid'");

if(mysqli_num_rows($query)>0)
{
	$data = mysqli_fetch_assoc($query);


	$id = $data['events_id'];
	$title = $data['events_name'];
	$desc = $data['events_description'];
	$image = $data['events_image'];
	$events_fromtime = $data['events_fromtime'];
	$events_totime = $data['events_totime'];
	$events_city = $data['events_city'];
	$events_date = $data['events_date'];

	

}


?>


</head>

<body>

	<!-- Main navbar -->
    <?php include('addons/navbar.php'); ?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			
			<!-- /main sidebar -->
			<?php include('addons/left_side_bar.php'); ?>

			<!-- Main content -->
			<div class="content-wrapper">
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><span class="text-semibold"></span>Edit Event</h4>
						</div>
						<div class="heading-elements">
						<a href="events.php"><button type="button" class="btn border-slates bg-coral btn-flats"><i class="fas fa-arrow-left"></i> Back</button></a>
						</div>
					</div>
				</div>
				<!-- Content area -->
				<div class="content">

					<!-- Main charts -->
					
					<!-- /main charts -->


					<!-- Dashboard content -->
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12">

						
							<div class="panel panel-flat">
								<div class="panel-heading">
									<!-- <h6 class="panel-title">Edit Event</h6> -->
									<div class="heading-elements">
										<ul class="icons-list">
											<!-- <a href="events.php"><button type="button" class="btn border-slates text-slate-800 btn-flats">Back</button></a> -->
					                		
					                		<!-- <li><a data-action="collapse"></a></li>
					                		<li><a data-action="reload"></a></li> -->
					                	</ul>
				                	</div>
			                	</div>

								<div class="panel-body">
									<div class="row add_user_form">
									<div class="col-lg-2 col-md-2 col-sm-2"></div>
									<div class="col-lg-8 col-md-8 col-sm-8">
										<div class="form-group">
										<label class="control-label col-lg-3"> Title</label>
										<div class="col-lg-9">
											<input type="text" id="ctitle" class="form-control" placeholder="Enter Event Name" value="<?php echo $title; ?>">
											<span class="errormsg" style="display:none">Enter Event Title</span>
										</div>
										<div class="clear"></div>
										</div>

										<div class="form-group">
										<label class="control-label col-lg-3"> Description</label>
										<div class="col-lg-9">
											<textarea id="cdesc" class="form-control" placeholder="Enter event desc" value="<?php echo $desc; ?>"><?php echo $desc; ?>	</textarea>
											<span class="errormsg" style="display:none">Enter Event Description</span>
										</div>
										<div class="clear"></div>
										</div>

										<div class="form-group">
										<label class="control-label col-lg-3"> City</label>
										<div class="col-lg-9">
											<input type="text" id="ccity" class="form-control" placeholder="Enter event city" value="<?php echo $events_city; ?>">
											<span class="errormsg" style="display:none">Enter Event City</span>
										</div>
										<div class="clear"></div>
										</div>
										<div class="form-group">
										<label class="control-label col-lg-3"> Date</label>
										<div class="col-lg-9">
											<input type="date" id="cdate" class="form-control" value="<?php echo $events_date; ?>">
											<span class="errormsg" style="display:none">Enter Event Date</span>
										</div>
										<div class="clear"></div>
										</div>

										<div class="form-group">
										<label class="control-label col-lg-3"> FromTime</label>

										<div class="col-lg-6">
											<input type="text" id="cfromtime" class="form-control" value="<?php echo $events_fromtime; ?>">
											<span class="errormsg" style="display:none">Enter FromTime</span>
										</div>
										<!-- <div class="col-lg-3">
											<select id="cfromtimeselect">
												<option value="Am">am</option>
												<option value="Pm">pm</option>
											</select>
											<span class="errormsg" style="display:none">Select any One</span>
										</div> -->
										<div class="clear"></div>
										</div>

										<div class="form-group">
										<label class="control-label col-lg-3"> Totime</label>
										<div class="col-lg-6">
											<input type="text" id="ctotime" class="form-control" value="<?php echo $events_totime; ?>">
											<span class="errormsg" style="display:none">Enter Totime</span>
										</div>
										<!-- <div class="col-lg-3">
											<select id="ctotimeselect">
												<option value="Am">am</option>
												<option value="Pm">pm</option>
											</select>
											<span class="errormsg" style="display:none">Select any One</span>
										</div> -->
										<div class="clear"></div>
										</div>

										


										<div class="form-group">
										<label class="control-label col-lg-3"> Image</label>
										<div class="col-lg-9">
											<!-- <input type="file" id="pimage" class="form-control" placeholder="upload Image"> -->
											<input type="file" style="display: none;" name="file[]" id="pimage" class="form-control" onchange="document.getElementById('profile_pic').src = window.URL.createObjectURL(this.files[0])" name="profilepic[]" accept="image/* ">

											<?php
									if($image!='')
									{

									?>
											<img src="../images/resource/<?php echo $image; ?>" width="200px" height="220px" id="profile_pic"><br><br>
											

											<?php
										}
										else
										{
										?>
										<img src="../images/resource/avatar-img.png" width="200px" height="220px" id="profile_pic"><br><br>

										<?php
									}
									?>
									<span class ="uploaded_file_name"></span>

											<script type="text/javascript">
												$('.form-group img').on('click',function(){
													$(this).siblings('input:file').click();
												});
											</script>
											<span class="errormsg" style="display:none">Please Select Image and please give valid size 1mb</span>
									
											
											<span class="successmessage"></span>




										</div>
										<div class="clear"></div>
										</div>

										<div class="col-md-12 text-center"><button type="button" class="btn bg-darkcyan" id="add_new_pro"><i class="fa fa-check"></i> Update </button></div>

									</div>
									<div class="col-lg-2 col-md-2 col-sm-2"></div>
									</div>
								</div>
							</div>
							<!-- /latest posts -->

						</div>

						
					</div>
					<!-- /dashboard content -->


					<!-- Footer -->
					<?php include("addons/footer.php") ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

	<script type="text/javascript">
		
		$("#add_new_pro").click(function(event)
		{
			 $('.errormsg').hide();
			var id = "<?php echo $causeid; ?>";
			var ctitle = $("#ctitle").val();
			var cdesc = $("#cdesc").val();
			var ccity = $("#ccity").val();
			var cfromtime = $("#cfromtime").val();
			// var cfromtimeselect = $("#cfromtimeselect").val();
			var ctotime = $("#ctotime").val();
			var cdate = $("#cdate").val();
			// var ctotimeselect = $("#ctotimeselect").val();
			var image = $("#pimage").val();
			var url = $('#profile_pic').attr('src');
			var isLastSlash = (url[url.length -1]=="/")? true: false;
			var url= url.split("/");
			var avatorimage = url[url.length - (isLastSlash? 2: 1)];
			// alert(id);
			var flag=false;
			if (ctitle.length == 0)
              {  

	             $('#ctitle').siblings('.errormsg').show();
	              flag =true;   
              }
  
            else if (cdesc.length==0)
              {  
	             $('#cdesc').siblings('.errormsg').show();
	              flag =true;   
              }
              else if (ccity.length==0)
              {  
	             $('#ccity').siblings('.errormsg').show();
	              flag =true;   
              }

              else if (cdate.length==0)
              {  
	             $('#cdate').siblings('.errormsg').show();
	              flag =true;   
              }

              else if (cfromtime.length==0)
              {  
	             $('#cfromtime').siblings('.errormsg').show();
	              flag =true;   
              }

              // else if (cfromtimeselect.length==0)
              // {  
	             // $('#cfromtimeselect').siblings('.errormsg').show();
	             //  flag =true;   
              // }
              else if(avatorimage=='avatar-img.png'){
              	$('#pimage').siblings('.errormsg').show();
             	flag =true;   
              }
              else if (ctotime.length==0)
              {  
	             $('#ctotime').siblings('.errormsg').show();
	              flag =true;   
              }
              // else if (ctotimeselect.length==0)
              // {  
	             // $('#ctotimeselect').siblings('.errormsg').show();
	             //  flag =true;   
              // }

			else
			{


			//alert(product_id);

			$.ajax({
                              type:"post",
                              url:"api/edit_event.php",
                              data:{id:id,ctitle:ctitle,cdesc:cdesc,ccity:ccity,cfromtime:cfromtime,ctotime:ctotime,cdate:cdate},

                              success:function(data){
                              	jsondata = JSON.parse(data);


									  	if(jsondata.status == 1)
									  	{

									  		if(image.length==0)
									  		{
									  			$('.successmessage').css('color','green').html('<i class="fa fa-check"><b>Successfully updated Redirecting..</b>');

									  		  setTimeout(function(){
									  		  	window.location="events.php";


									  		}, 1000);
									  		}

									  		else
									  		{
									  			// alert("image selected");
									  			submitinfo(id);
									  		}

									  		

									  		//alert("Product Added Successfully");
									  		
											
									  	}

									  	else
									  	{
									  		alert("Error");
									  	}

                               
                            }


                        });
		}
		});



		function submitinfo(slide_id)
		{
		    var form_data = new FormData();  
		    var file_data = $("#pimage").prop("files")[0]; 
		    for(var loopvar=0;loopvar<$("#pimage").prop("files").length;loopvar++)
		    {
		       var file_data = $("#pimage").prop("files")[loopvar]; 
		      form_data.append("file[]", file_data);
		    }
		    if($("#pimage").prop("files").length==0)
		      form_data.append("file[]", "");
		  
		  
		      form_data.append("productid", slide_id);
		      
		      $.ajax({
		            url: "api/editevent_image.php",
		            dataType: 'text',  // what to expect back from the PHP script, if anything
		            cache: false,
		            contentType: false,
		            processData: false,
		            data: form_data,                         
		            type: 'post',
		            success: function(php_script_response){
		              var jsondata=JSON.parse(php_script_response);
		                if(jsondata.status==1)
		                {
		                	$('.successmessage').css('color','green').html('<i class="fa fa-check"><b>Successfully updated Redirecting..</b>');

									  		  setTimeout(function(){
									  		  	window.location="events.php";


									  		}, 1000);	
		                }
		                else
		                {
		            	    alert("Failed");
		                }
		        }
		      });
		}


	</script>

	<script type="text/javascript">
            
            function isNumberKey(evt){
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
                return true;
            }

        </script>

</body>
</html>

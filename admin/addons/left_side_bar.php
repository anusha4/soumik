
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">
<!-- <link rel="stylesheet" type="text/css" href="assets/css/extrastyles.css"> -->
<div class="sidebar sidebar-main">
				<div class="sidebar-content">

						<!-- User menu -->
					<div class="sidebar-user">
						<div class="category-content">
							<div class="media">
								<a class="media-left"  href="index.html"><img src="assets/images/soumiklogo.png"  alt=""></a>
								<!-- <a href="#" class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a> -->
								<!-- <div class="media-body">
									<span class="media-heading text-semibold">Victoria Baker</span>
									<div class="text-size-mini text-muted">
										<i class="icon-pin text-size-small"></i> &nbsp;Santa Ana, CA
									</div>
								</div> -->

								<div class="media-right media-middle">
									<ul class="icons-list">
										<li>
											<a href="#"><i class="icon-cog3"></i></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- /user menu -->
					


					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">

								<!-- Main -->
								
								<!-- <li id="dashboard"><a href="dashboard.php"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
								<li id="menu2"><a href="users.php"><i class="icon-users"></i> <span>Home</span></a>
									<ul>
										<li id="users"><a href="users.php">All Banners</a></li>
										<li id="add_user_menu"><a href="addnewuser.php">Add User</a></li>
									</ul>
								</li> -->
								<li id="menu1"><a href="user_table.php"><i class="fa fa-users"></i> <span>Users</span></a>
								<li id="menu2"><a href="slides.php"><i class="fab fa-slideshare"></i><span>All Banners</span></a>



								</li>
								<li id="menu3"><a href="causes.php"><i class="fa fa-hand-holding-heart"></i><span>causes</span></a>

									<!-- <ul>
										<li id="coupons"><a href="coupons.php">All Coupons</a></li>
										<li id="addnewcoupon"><a href="addnewcoupon.php">Add Coupon</a></li>
										<li id="claimed_coupons"><a href="claimed_coupons.php">Claimed Coupons</a></li>
										<li id="unclaimed_coupons"><a href="unclaimed_coupons.php">Unclaimed Coupons</a></li>
										<li id="check_coupon"><a href="check_coupon.php">Check Coupons</a></li>
										<li id="check_coupon_availability"><a href="check_coupon_availability.php">Check Availabiity</a></li>
										<li id="add_coupon_availability"><a href="add_coupon_availability.php">Add hospital Availabiity</a></li>
									</ul> -->

								</li>
								<li id="menu4"><a href="aboutus.php"><i class="fa fa-home"></i> <span>About Us</span></a>
								</li>
								</li>
								<li id="menu5"><a href="services.php"><i class="fa fa-cogs"></i> <span>Services</span></a>
								</li>
								<li id="menu6"><a href="valuntories.php"><i class="fa fa-hands-helping"></i> <span>volunteers</span></a>
								</li>
								<li id="menu7"><a href="latestnews.php"><i class="fa fa-newspaper-o"></i> <span>latest news</span></a>
								</li>
								<li id="menu8"><a href="events.php"><i class="fa fa-calendar-plus-o"></i> <span>Events</span></a>
								</li>
								<li id="menu9"><a href="workflow.php"><i class="fa fa-male"></i><span>Who We Are</span></a>
								</li>
								<li id="menu10"><a href="donaters.php"><i class="fa fa-hand-holding-usd" style="transform:rotate(180deg);"></i> <span>Donaters</span></a>
								</li>
								<li id="menu11"><a href="seo.php"><i class="fa fa-chart-line"></i> <span>Seo</span></a>
								</li>
								<li id="menu10"><a href="contact-us.php"><i class="fa fa-phone" aria-hidden="true" style="transform:rotate(90deg);"></i><span>Contact Us</span></a>
								</li>
								<li id="menu10"><a href="subscribers.php"><i class="fas fa-mail-bulk"></i> <span>Subscribers</span></a>
								</li>
								
								
								
								<!-- <li id="menu5"><a href="orders.php"><i class="icon-list-ordered"></i> <span>Orders</span></a>
									<ul>
										<li id="orders"><a href="orders.php">All Orders</a></li>
										<li id="delivered_orders"><a href="delivered_orders.php">Delivered Orders</a></li>
										<li id="pending_orders"><a href="pending_orders.php">Undelivered Orders</a></li>
										
										
									</ul>

								</li>

								<li id="menu5"><a href="orders.php"><i class="icon-list-ordered"></i> <span>Delivery</span></a>
								<ul>
									<li id="updatedelivery"><a href="updatedelivery.php?oid=">Update Delivery Status</a></li>
									<li id="deliveryboyslist"><a href="deliveryboys.php">Delivery Boys list</a></li>
									</ul>


								<li id="menu6"><a href="hospitals.php"><i class="icon-aid-kit"></i> <span>Hospitals</span></a>
									<ul>
										<li id="hospitals"><a href="hospitals.php">All Hospitals</a></li>
										<li id="addnewhospital"><a href="addnewhospital.php">Add Hospital</a></li>

										
									</ul>

								</li>

								<li id="menu6"><a href=""><i class="icon-mobile"></i> <span>Mobile</span></a>
									<ul>
										<li id="addmobilebanner"><a href="addmobilebanner.php">Add Banner</a></li>
										<li id="mobilebanners"><a href="mobilebanners.php">View Banners</a></li>

										
									</ul>

								</li>

								<li id="menu6"><a href=""><i class="icon-pushpin"></i><span>Pincodes</span></a>
									<ul>
										<li id="addpincode"><a href="addpincode.php">Add Pincode</a></li>
										<li id="pincodes"><a href="pincodes.php">Available Pincodes</a></li>

										
									</ul>

								</li>

								<li id="menu6"><a href=""><i class="icon-person"></i><span>Events</span></a>
									<ul>
										<li id="addevent"><a href="addevent.php">Add Event</a></li>
										<li id="events"><a href="events.php">All Events</a></li>

										
									</ul>

								</li>

								<li id="statistics"><a href="statistics.php"><i class="fa fa-line-chart" aria-hidden="true"></i><span>Statistics</span></a>
								</li> -->

								
								
								<!-- Forms -->
								
								<!-- /forms -->

								<!-- Appearance -->
								
								<!-- /appearance -->

								<!-- Layout -->
						

								<!-- Page kits -->
								
								<!-- /page kits -->

							</ul>
						</div>
					</div>
					<!-- /main navigation -->

				</div>
			</div>

			<script type="text/javascript">
				$('.count').each(function () {
					    $(this).prop('Counter',0).animate({
					        Counter: $(this).text()
					    }, {
					        duration: 4000,
					        easing: 'swing',
					        step: function (now) {
					            $(this).text(Math.ceil(now));
					        }
					    });
					});
			</script>
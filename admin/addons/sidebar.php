<style type="text/css">
	.logo > img {
       margin-top: -11px;
    height: 35px;
   margin-left: -13px;
}

	
</style>
	<!-- Main sidebar -->
			<div class="sidebar sidebar-main">
				<div class="sidebar-content">

					<!-- User menu -->
					<div class="sidebar-user">
						<div class="category-content">
							<div class="media">
								<a class="navbar-brand logo" href="index.php"><img src="assets/images/tkt-logo.png" alt=""></a>
								<div class="media-body">


									<span class="media-heading text-semibold">The King's Temple</span>
									<div class="text-size-mini text-muted">
										<i class="icon-pin text-size-small"></i> &nbsp;Hyderabad,India
									</div>
								</div>

								<div class="media-right media-middle">
									<ul class="icons-list">
										<li>
											<a href="#"><i class="icon-cog3"></i></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- /user menu -->


					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">

								<!-- Main -->
								<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
								<li class="active"><a href="index.php"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
								<li><a href="#"><i class="fa fa-users"></i> <span>Admin Users</span></a>
									<ul>
										<li><a href="addUsers.php">Add User</a></li>
										<li><a href="user_Rolechange.php">Role Change</a></li>
										<li><a href="registeredUserreport.php">Registered Users</a></li>
										<li><a href="pendingApprovals.php">Pending Approvals</a></li>
										<li><a href="pendingRegistrations.php">Pending Registrations</a></li>
									</ul>
								</li>
								<li><a href="#"><i class="fa fa-globe"></i> <span>Website Contents</span></a>
									<ul>
										<li><a href="#">Add Content</a>
										<ul>
											<li><a href="addContent_news.php">News</a></li>
											<li><a href="#">Events</a></li>
											<li><a href="#">Videos</a></li>
										</ul>
										<li><a href="#">Approve Contents</a>
										<ul>
											<li><a href="#">News</a></li>
											<li><a href="#">Events</a></li>
											<li><a href="#">Videos</a></li>
										</ul>
									</li>
										<li><a href="#">View Contents</a>
											<ul>
											<li><a href="#">News</a></li>
											<li><a href="#">Events</a></li>
											<li><a href="#">Videos</a></li>
										</ul>
										</li>
									</ul>
								</li>
								<li><a href="#"><i class="fa fa-briefcase"></i> <span>Careers</span></a>
									<ul>
										<li><a href="careerApprovedjobs.php">Approve Jobs</a></li>
										<li><a href="careerPendingjobsJobs.php">Pending Jobs</a></li>
										<li><a href="careerRejectedjobs.php">Rejected Jobs</a></li>
										<li><a href="careerDeletedjobs.php">Deleted Jobs</a></li>
										
									</ul>
								</li>
								<li><a href="#"><i class="fa fa-bullhorn"></i> <span>Financial Ads</span></a></li>
								<li><a href="#"><i class="fa fa-envelope"></i> <span>SMS/Email</span></a></li>
								<li><a href="#"><i class="fa fa-comments"></i> <span>Forum</span></a></li>
								<li><a href="#"><i class="fa fa-question-circle"></i> <span>Ask 4 Help</span></a></li>
								
								<!-- /main -->

								<!-- Forms -->
								
								
								
								
								<!-- /extensions -->

								<!-- Tables -->
								
							</ul>
						</div>
					</div>
					<!-- /main navigation -->

				</div>
			</div>
			<!-- /main sidebar -->
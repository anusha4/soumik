<?php
session_start();
include('admin/api/db.php');  ?>
<!DOCTYPE html> 
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<title>Soumik Charity | Confirm Donation</title>

<!-- Stylesheets -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">
<link href="css/forms.css" rel="stylesheet">
<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
<link rel="icon" href="images/favicon.png" type="image/x-icon">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" >
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

<style>
.donat{
    background-color: #f2f2f2;
    padding: 25px 0;
}
.trustdonation{
   
background: linear-gradient(45deg,#ea7325 1%,#fcc005 100%);
border-color:orange;
margin: auto ;
display:block;

}
.btn{
background: linear-gradient(45deg,#ea7325 1%,#fcc005 100%);
margin:orange; 
color:white;
}
.trustdonation:hover{
    background: linear-gradient(45deg,#ea7325 1%,#fcc005 100%);
    border-color:orange;
}

.p{
    padding:10px;
}
.text-center{
    font-size:15px;
    background-color:linear-gradient(45deg,#ea7325 1%,#fcc005 100%);
    margin-bottom:10px;
}
.h3-space{margin:50px auto;}
.pa{
    font-size:20px;
    color:linear-gradient(45deg,#ea7325 1%,#fcc005 100%);
    padding-left:10px;
    color: orange;
    }
.top-left {
    position: absolute;
    top: 39px;
    left: 27px;
}
.bottom-left{
    position: absolute;
    top:200px;
    left:35px;
    background-color:#f2f2f2;
    padding:4px;
    color:#337ab7;
    word-wrap: break-word;
    border-radius: 12px;
}
.bottom-left:hover{
    text-decoration:none;
}
.bottom-right{
    position: absolute;
    top:200px;
    right:45px;
    background-color:#f2f2f2;
    padding:4px;
    color:black;
     word-wrap: break-word;
     border-radius: 12px;
}
.bottom-right:hover{
    text-decoration:none;
}
.donationleft{
background-color: #f2f2f2;
    height: 350px;
    overflow: auto;
}
.fb{
    padding:5px;
    background-color:white;
    margin:5px;
}
.p1{
    padding:5px;
    background-color:white;
    margin:5px;
     color:linear-gradient(45deg,#ea7325 1%,#fcc005 100%);

}
.img-res{width:100%;}
.fontw label{font-weight:normal;padding-top:15px;}
.card{background-color: white;
    padding: 9px 30px;
    margin: 64px 16px;
    border-radius: 4px;
    border-color: #f2f2f2;
    margin-top: 25px;
    margin-bottom: 10px;

}
.paytext{
    background-color:#f2f2f2;
    padding:10px;
    border-radius:4px;
    margin:4px;

}
.donation-top-text{
    padding:0px;
    margin-bottom:20px;
    margin-left:0px;
    color:orange;
    font-size:18px;

}

</style>

      
        <div class="popup_overlay"></div>
    <div class="validatiin_opopip" style="display:none">
        <button class="conformation_msg_close_btn">X</button>
        <p id="popupquestion" class="text-center">Please select Registration Type</p>
        <button class="conformation_cncl" style=""  onclick="closepopup()">Ok</button>
    </div> 
<style type="text/css">
       
            .conformation_msg_close_btn {
                position: absolute;
                right: 0;
                top: 0;
                width: 30px;
                height: 30px;
                text-align: center;
                border-radius: 0% 0% 0% 50%;
                box-shadow: -1px 1px 2px #444;
                background-color: #ea7325;
                border-radius: 5px;
                color: #fff;
                padding: 5px;
                padding-right: 8px;
                border:0;
                font-family:roboto !important;
            }
            .validatiin_opopip
            {
                position: fixed;
                z-index: 101;
                top: 50%;
                left: 50%;
                transform: translate(-50%,-50%);
                -moz-transform: translate(-50%,-50%);
                -webkit-transform: translate(-50%,-50%);
                -ms-transform: translate(-50%,-50%);
                -o-transform: translate(-50%,-50%);
                background-color: #fff;
                width: 500px;
                padding: 42px;
                text-align: center;
                border: 4px solid #D7D7D7;
                border-radius: 7px;
                font-family: "Open Sans";
                display: none;
            }
            .popup_overlay{
                position: fixed;
                top:0;
                width: 100%;
                height: 100%;
                background-color: rgba(0,0,0,0.7);
                z-index: 100;
                display: none;
            }
            .conformation_cncl {
                padding-left: 20px;
                padding-right: 20px;
                margin-top: 55px;
               background-color: #ea7325;
                border-radius: 5px;
                color: #fff;
                width: auto;
                border: 0;
                padding-top: 5px;
                padding-bottom: 5px;
                 font-family:roboto !important;
            }
    </style>


</head>

<body>

<div class="page-wrapper">
    <!-- Preloader -->
    <div class="preloader"></div>

    <!-- Main Header-->
    <header class="main-header header-style-two">
        <!--Header Top-->
        <div class="header-top">
            <div class="auto-container">
                <div class="inner-container clearfix">
                    <div class="top-left pull-left">
                       <ul class="contact-info">
                           <li><a href="#"><span class="fa fa-envelope"></span>info@soumikcharitabletrust.org</a></li>
                       </ul>
                    </div>
                    <div class="top-left pull-right">
                       <ul class="contact-info">
                           <li><a href="#"><span class="fa fa-phone"></span> +91 9000302235 / 9010942738</a></li>
                       </ul>
                    </div>
                   
                </div>
            </div>
        </div>
        <!-- End Header Top -->

        <!-- Header Lower -->
        <div class="header-lower">
            <div class="auto-container">
               <div class="main-box clearfix">
                    <!--Logo Box-->
                    <div class="logo-box">
                        <div class="logo"><a href="index.php"><img src="images/logo-2.png" alt=""></a></div>
                    </div>

                    <!--Nav Outer-->
                    <div class="nav-outer clearfix">
                        <!-- Main Menu -->
                        <nav class="main-menu">
                            <div class="navbar-header">
                                <!-- Toggle Button -->
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>

                            <div class="navbar-collapse collapse clearfix">
                                <ul class="navigation clearfix">
                              
                               <li><a href="index.php">Home</a></li>
                                    <li><a href="about-us.php">About us</a></li>
                                    <li class="dropdown"><a href="services.php" >Services</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="support-for-education.php"> Education </a></li>    
                                            <li><a href="support-for-health.php"> Health </a></li>
                                            <li><a href="helping-poor-people.php"> Helping People </a></li>
                                        </ul>
                                    </li>
                                    <li><a href="causes-list.php">Causes</a></li> 
                                    <li><a href="event-list.php">Events</a></li>                              
                                    <li><a href="contact-us.php">Contact Us</a></li>
                                <?php
                                if(isset($_SESSION['username'])=='')
                                {
                                ?>
                                <li><a href="signin-registration.php">Login</a></li>
                                <?php
                            }
                            else
                            {
                            ?>

                                <li class="dropdown"><a href="services.php" ><?php echo $_SESSION['fullname'];?></a>
                                    <ul class="dropdown-menu">
                                        <?php
                                if(isset($_SESSION['user_usertype'])=='Donaters')
                                {
                                ?>
                                       
                                        <li><a href="profile.php"> My Profile </a></li>
                                         <li><a href="my-donations.php"> My Donations </a></li>
                                        <li><a href="logout.php"> Logout </a></li>
                                    </ul>
                                </li>
                                <?php
                                }
                                else
                                {
                                ?>
                                 <li><a href="profile.php"> My Profile </a></li>
                                         
                                        <li><a href="logout.php"> Logout </a></li>
                                    </ul>
                                </li>
                                <?php
                                }
                            }
                            ?>
                            </ul>
                            </div>
                        </nav>
                        <!-- Main Menu End-->

                       

                    </div>
                </div>
            </div>
        </div>
        <!-- End Header Lower -->

        <!-- Sticky Header -->
        <div class="sticky-header">
            <div class="auto-container clearfix">
                <!--Logo-->
                <div class="logo pull-left">
                    <a href="index.php" title=""><img src="images/logo-small.png" alt="" title=""></a>
                </div>
                <!--Right Col-->
                <div class="pull-right">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li><a href="index.php">Home</a></li>
                                    <li><a href="about-us.php">About us</a></li>
                                    <li class="dropdown"><a href="services.php" >Services</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="support-for-education.php"> Education </a></li>    
                                            <li><a href="support-for-health.php"> Health </a></li>
                                            <li><a href="helping-poor-people.php"> Helping People </a></li>
                                        </ul>
                                    </li>
                                    <li><a href="causes-list.php">Causes</a></li> 
                                    <li><a href="event-list.php">Events</a></li>                              
                                    <li><a href="contact-us.php">Contact Us</a></li>
                                 <?php
                                if(isset($_SESSION['username'])=='')
                                {
                                ?>
                                <li><a href="signin-registration.php">Login</a></li>
                                <?php
                            }
                            else
                            {
                            ?>

                                <li class="dropdown"><a href="services.php" ><?php echo $_SESSION['fullname'];?></a>
                                    <ul class="dropdown-menu">
                                        <?php
                                if(isset($_SESSION['user_usertype'])=='Donaters')
                                {
                                ?>
                                       
                                        <li><a href="profile.php"> My Profile </a></li>
                                         <li><a href="my-donations.php"> My Donations </a></li>
                                        <li><a href="logout.php"> Logout </a></li>
                                    </ul>
                                </li>
                                <?php
                                }
                                else
                                {
                                ?>
                                 <li><a href="profile.php"> My Profile </a></li>
                                         
                                        <li><a href="logout.php"> Logout </a></li>
                                    </ul>
                                </li>
                                <?php
                                }
                            }
                            ?>
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>
            </div>
        </div>
    </header>
    <!--End Main Header -->
  
   
        <div class="container">
            <div class="row">
               
            </div><br>


            <div class="row">
                <div class="col-md-4">
                    <div class="col-md-12 ">
                     <p class="pa"><a href="https://www.facebook.com/soumikcharitabletrust/" target="_blank"style="color: #23527c;"><i class="fab fa-facebook-square"></i></a> FIND US ON <span style="color: black">FB</span></p>
                        <div class="desc">
                                <img src="images/payimg1.png?soumikversion=1.1" class="img-res" >
                                <img src="images/logo-small.png" class="top-left">

                                 <a href="https://www.facebook.com/soumikcharitabletrust/" target="_blank"role="button" class="bottom-right" target="_blank"><i class="far fa-share-square"></i> share</a>
                            <div class="donationleft">
                                <p class="fb">Be the first of your friends to like this</p>
                                <p class="p1">Chairman of Soumik Charitable Trust #billuriramamohanreddy had conducted many development programs for progress in villages of tanakallu mandalam. He has given an inspirational speech on the development of villages for motivating every person to come across every hurdle.😊<br></p>
                                <img src="images/fb.png" class="img-res" >
                                <p class="p1">The effort of helping the needy people shows the determination of #BilluriRamaMohanReddy chairman of #SoumikCharitableTrust. He is giving 10 thousand rupees to the old-aged women in thanakallu mandalam who is in need of financial help for survival.</p>
                                <img src="images/fb1.png" class="img-res">
                             </div>
                         </div>
                
                    </div>
                </div>
                <div class="col-md-4 donation-top-text">
                         <h3>Donate</h3>
                         <hr style="width: 97px;border-top: 2px solid orange;margin-top: 9px;border: -1;">
                    </div>
                <div class="col-md-8  donat">
                   

                    <div class="col-md-10 col-md-offset-1 ">

                        <form action="#" onsubmit="return false" class="fontw">
                            <div class="form-group ">
                                <div class="col-xs-6">
                              <label>Name:</label>
                              <input type="text" class="form-control name" placeholder="Enter Name" >
                              </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-6">
                              <label>Email</label>
                              <input type="text" class="form-control email" placeholder="Enter Email">
                            </div>
                        </div>
                            <div class="form-group">
                                <div class="col-xs-6">
                              <label>Phone Number</label>
                              <input type="text" class="form-control phonenumber" placeholder="Enter Phone Number" maxlength="10" onkeypress="return isNumberKey(event)">
                            </div>
                        </div>
                             <div class="form-group">
                                <div class="col-xs-6">
                              <label >Address Line1</label>
                              <input type="text" class="form-control address1" placeholder="Enter Address">
                            </div>
                        </div>
                            <div class="form-group">
                                <div class="col-xs-6">
                              <label >Address Line2</label>
                              <input type="text" class="form-control address2" placeholder="Enter Address">
                            </div>
                        </div>
                            <div class="form-group">
                                <div class="col-xs-6">
                              <label >Town/City</label>
                              <input type="text" class="form-control city" placeholder="Enter Town/City">
                            </div>
                        </div>
                            <div class="form-group">
                                <div class="col-xs-6">
                              <label >Postal Code</label>
                              <input type="text" class="form-control postalcode" placeholder="Enter Postal Code" onkeypress="return isNumberKey(event)" maxlength="6">
                            </div>
                        </div>
                            <div class="form-group">
                                <div class="col-xs-6">
                              <label >Country</label>
                              <input type="text" class="form-control country" placeholder="Enter Country">
                            </div>
                        </div>
                         <div class="form-group">
                                <div class="col-xs-6">
                              <label >Fund Category</label>
                            <select class="form-control fundcategory">
                                <option value=''>Select</option>
                                <option value="trustdevelopment">Donation For Charitable / Development purpose</option>
                                <option value="forchildrens">Donation for Childrens</option>
                                <option value="forpoorpeople">Donation For Poor People</option>
                                <option value="forhealth">Donation For Health</option>
                            </select>
                               <span class="error"></span>
                            </div>
                        </div>
                       
                        <div class="form-group">
                            <div class="col-xs-6">
                              <label >Donate Amount</label>
                              <input type="text" class="form-control amount" onkeypress="return isNumberKey(event)" placeholder="Enter Donation Amount">
                            </div>
                        </div>
                        <div style="clear: both"></div>
                        <div class="card">
                        <input type="radio" name="pay" value="checkpayment">Check Payments<br>
                        <div  class="paytext checkpaymentdiv" style="display: none;">Please send a check to Store Name, Store Street, Store Town, Store State / County, Store Postcode.</div>
                        <input type="radio" name="pay" value="paywithpaypol">Pay with Paypal Account<img src="https://i2.wp.com/www.paypalobjects.com/webstatic/en_US/i/buttons/pp-acceptance-small.png?w=1170&#038;ssl=1" style="padding-left:10px;">
                        <div class="paytext paywithpaypoldiv" style="display: none;">Pay using either your PayPal account or credit card. All credit card payments will be processed by PayPal.</div>
                        </div>
                        <!-- <div style="clear: both"></div> -->
                         <div>
                          <p class="errors" style="text-align:center;padding-top: 25px;color:red;"></p>
                      </div>

                        <div class="col-md-12 text-center" style="margin:15px 0; background-color:linear-gradient(45deg,#ea7325 1%,#fcc005 100%);">
                            <button type="submit" class="btn trustdonation"><i class="fa fa-hand-holding-usd"></i> Pay Now</button>
                        </div>   
                        </form>
                        <div class="row">
                        <div><strong>State Bank of India (SBI)</strong>
                             <p>Account Name: Soumik Charitable Trust</p>
                             <p>Account No: 35999662561</p>
                            <p>IFSC Code: SBIN0015613</p>
                        </div>
                        <!--&#8212; &#8212; &#8212; &#8212; &#8212; &#8212; &#8212; &#8212; &#8212; &#8212; &#8212; &#8212; &#8212;<br />-->
                        <!--<div><strong>Paytm</strong>-->
                        <!--    <p>Paytm Number:###########</p>-->
                        <!--</div>-->
                        <!--    &#8212; &#8212; &#8212; &#8212; &#8212; &#8212; &#8212; &#8212; &#8212; &#8212; &#8212; &#8212; &#8212;<br />-->
                        <!--<div><strong>Google Pay</strong>-->
                        <!--    <p>Google Pay Number:###########</p>-->
                        <!--</div>-->
                    </div>
                    </div>
                </div>
                <div class="col-md-2">
                   
                    <div class="col-md-10 col-md-offset-1">

                    </div>
                </div>
              
            </div>

        </div>
        
   


         
    <div class="" style="margin-top:50px"></div>     
    <!--Main Footer-->
    <footer class="main-footer" style="background-image: url(images/background/2.jpg);">
        <!--Upper-->
        <div class="footer-upper">
            <div class="auto-container">
                <div class="outer-box">
                    <!--Footer Logo-->
                    <div class="footer-logo"><a href="index.php"><img src="images/footer-logo.png" alt=""></a></div>

                    <div class="row clearfix">
                        <div class="big-column col-md-4 col-sm-12 col-xs-12">
                            <div class="row clearfix">
                                <!--Footer Column-->
                                <h3>Menu</h3>
                                <ul class="footer-links">
                                     <li><a href="index.php">Home</a></li>
                                    <li><a href="about.php">About us</a></li>
                                    <li><a href="causes-list.php">Causes</a></li>
                                    <li><a href="event-list.php">Events</a></li>                             
                                    <li><a href="contact.php">Contact Us</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="big-column col-md-4 col-sm-12 col-xs-12">
                            <div class="row clearfix">
                                <!--Footer Column-->
                               <h3>Contact </h3>
                                <ul class="footer-links contact-links">
                                    <li>D.No: 28-4-797-2, State Bank Colony <br>
                                     JNTU Road, Ananthapuram - 515002 (AP)</li>
                                    <li>+91 9000302235 / 9010942738</li>
                                    <li><a href="#">info@soumikcharitabletrust.org</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="big-column col-md-4 col-sm-12 col-xs-12">
                            <div class="row clearfix">   
                                <!--Footer Column-->
                                <h3>About</h3>
                                <div class="text">
                                    <p>Our charity lends the helping hand to the people in desperate situations. When their life becomes the destiny of the darkest path.</p>
                                </div>
                                <br>
                                <h3>Follow</h3>
                                <ul class="footer-social-links">
                                    <li><a href="https://www.facebook.com/soumikcharitabletrust/" target="_blank"><i class="fab fa-facebook-square"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                   
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--Bottom-->
        <div class="footer-bottom">
            <div class="auto-container">
                <div class="copyright">Copyright <a href="index.php">Soumik Charitable Trust</a> © 2018. All Rights Reserved.</div>
            </div>
        </div>
    </footer>
    <!-- End Footer -->

</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-angle-double-up"></span></div>

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/jquery.fancybox.js"></script>
<script src="js/owl.js"></script>
<script src="js/wow.js"></script>
<script src="js/appear.js"></script>
<script src="js/script.js"></script>
<script type="text/javascript">
     
      function isNumberKey(evt){
          var charCode = (evt.which) ? evt.which : event.keyCode
          if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
          return true;
      }

   
    </script>

<script type="text/javascript">
    $("input[name='pay']").change(function(){
      var paymentmode=$(this).val();
      //alert(paymentmode);
      if(paymentmode=='checkpayment')
      {
        $(this).siblings('.checkpaymentdiv').slideDown();
        $(this).siblings('.paywithpaypoldiv').slideUp();
      }
      else
      {
        $(this).siblings('.paywithpaypoldiv').slideDown();
        $(this).siblings('.checkpaymentdiv').slideUp();

      }
});
</script>
    

<script type="text/javascript">
    $('.trustdonation').on('click',function()
    {
        var name=$('.name').val();
        var email=$('.email').val();
        var phonenumber=$('.phonenumber').val();
        var address1=$('.address1').val();
        var address2=$('.address2').val();
        var city=$('.city').val();
        var postalcode=$('.postalcode').val();
        var country=$('.country').val();
        var amount=$('.amount').val();
        var atpos = email.indexOf("@");
        var dotpos = email.lastIndexOf(".");
        var fundcategory=$('.fundcategory').val();
        var paymentmode=$('input[name=pay]:checked').val();

        //alert(paymentmode);
       
        var flag=true;
        if(name.length==0)
        {
        //alert('Please Enter Name');

       
        $('.errors').html('<b>Enter Name</b>');

        }

        else if(email.length==0)
        {
            $('.errors').html('<b>Enter Email</b>');
             
        }
       

        else if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length)
        {
            $('.errors').html('<b>Enter valid EmailId</b>');
            
      
         }
        else if(phonenumber.length==0)
        {
            $('.errors').html('<b>Enter Mobileno</b>');
           

        }
        else if(phonenumber.length>10 || phonenumber<10)
        {
            $('.errors').html('<b>Enter Enter valid Mobileno</b>');
           

        }
        
        else if(address1.length==0)
        {
             $('.errors').html('<b>Enter Address</b>');
              

        }
        else if(address2.length==0)
        { 
            $('.errors').html('<b>Enter Alternate Address</b>');
            

        }
        else if(city.length==0)
        {
           
            $('.errors').html('<b>Please Enter City</b>');
           
        }
        else if(postalcode.length==0)
        {
            $('.errors').html('<b>Please Enter Postalcode</b>');
              
        }
        else if(country.length==0)
        {

            $('.errors').html('<b>Please Enter Country</b>');
           
        }
        else if(fundcategory.length=='')
        {
          
            $('.errors').html('<b>Please Select Fundcategory</b>');
            
                  
                  
        }
        else if(amount.length==0)
        {
            $('.errors').html('<b>Please Enter Amount</b>');
            
        }
       else if($('input[name=pay]:checked').length == 0)
        {
   
        $('.errors').html('<b>Please Select Payment Mode</b>');

        }
       


        
        else
        {
            $.ajax({
                
                              type:"post",
                              url:"payutableapi.php",
                              data:{name:name,email:email,phonenumber:phonenumber,address1:address1,address2:address2,city:city,postalcode:postalcode,country:country,amount:amount,fundcategory:fundcategory,paymentmode:paymentmode},
                              success:function(data){
                                var jsondata=JSON.parse(data);
                                if(jsondata.status==1)
                                {


                                    $('.validatiin_opopip').show();
                                    $('.popup_overlay').show();
                                   $('#popupquestion').html('<b class="text-center">Donation Successful</b>');

                                     if(paymentmode=='checkpayment')
                                     {
                                         setTimeout(function(){ 
                                              window.location='donatesuccess.php';
                                             }, 3000);
                                        

                                     }
                                     else
                                     {
                                          window.location='https://www.paypal.com/in/webapps/mpp/home';
                                     
                                     }


                                }
                                else
                                {
                                   // window.location='orderfailed.php';
                                
                                }
                            }
                 });
        }




    });
</script>
</body>
</html>
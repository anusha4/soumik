<?php include('addons/header.php'); ?>

    <section class="container" style="margin-bottom: 120px">
    	<div class="row">
    		 <h1 class="text-center" style="font-size: 25px;margin-bottom: 5px"> Profile Information</h1>
	    	<div class="col-md-3">
	    		<div class="image-box text-center">
	    			<img src="images/resource/profile-pic.jpg" alt="profile image" class="text-center"  />
	    			 
	    		</div>
			</div>


             <?php
                  $username=$_SESSION['username'];
                 $query124 = "SELECT * FROM users where user_email='$username'";
                $result125=mysqli_query($con,$query124);
                $i=1;
       
               if($data = mysqli_fetch_assoc($result125))
               {
                ?>
			<div class="col-md-9">
				<table >
				   
				    <hr>
				    <div class="text-right">
				   		<a href="#" class=" btn btn-primary" data-toggle="modal" data-target="#myModal1" >Edit</a>
					</div>
				    <tbody>
				      	<tr>
					        <td style="width:50%;line-height: 40px; font-weight: bold">Name :</td>
					        <td><?php echo $data['user_name']; ?></td>
					    </tr>

				     	<tr>
					        <td style="width:50%;line-height: 40px; font-weight: bold">Email Id:</td>
					        <td><?php echo $data['user_email']; ?></td>
					    </tr>
					    <tr>
					        <td style="width:50%;line-height: 40px; font-weight: bold">Mobileno:</td>
					        <td><?php echo $data['user_mobile']; ?></td>
					    </tr>
                         <tr>
                            <td style="width:50%;line-height: 40px; font-weight: bold">UserType:</td>
                            <td><?php echo $data['user_usertype']; ?></td>
                        </tr>
                         <tr>
                            <td style="width:50%;line-height: 40px; font-weight: bold">Added Date:</td>
                            <td><?php echo $data['user_addedon']; ?></td>
                        </tr>
				      	
				    </tbody>
		  		</table>
		  		<hr>
		  		
		  		<div class="">
				 
				 <!--  <a href="#" class=" btn btn-success" data-toggle="modal" data-target="#myModal2">Add Address</a> -->
				  <a href="#" class=" btn btn-warning" data-toggle="modal" data-target="#myModal3">Reset Password</a>
				</div>
			</div>

            <?php
        }
            ?>

		</div>

    </section>

    <!-- Subscribe section -->
    <section class="subscribe-section">
        <div class="auto-container">
            <div class="inner-container">
                <div class="row clearfix">
                    <div class="title-column col-md-6 col-sm-12 col-xs-12">
                        <div class="title">
                            <h2>Donation Hotline :  03 2689254 - 03 2685987</h2>
                        </div>
                    </div>
                    <div class="form-column col-md-6 col-sm-12 col-xs-12">
                        <div class="subscribe-form">
                            <form method="post" action="contact.html">
                                <div class="form-group">
                                    <input type="email" name="email" value="" placeholder="Enter Your email" required="">
                                    <button type="submit" class="theme-btn btn-style-three">Subscribe</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Subscribe section -->

    <!--Main Footer-->
    <footer class="main-footer" style="background-image: url(images/background/2.jpg);">
        <!--Upper-->
        <div class="footer-upper">
            <div class="auto-container">
                <div class="outer-box">
                    <!--Footer Logo-->
                    <div class="footer-logo"><a href="index.php"><img src="images/footer-logo.png" alt=""></a></div>
                    
                    <div class="row clearfix">
                        <div class="big-column col-md-5 col-sm-12 col-xs-12">
                            <div class="row clearfix">
                                <!--Footer Column-->
                                <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                    <h3>Menu</h3>
                                    <ul class="footer-links">
                                        <li><a href="#">Home</a></li>
                                        <li><a href="#">About Us</a></li>
                                        <li><a href="#">Causes</a></li>
                                        <li><a href="#">Events</a></li>
                                        <li><a href="#">Contact Us</a></li>
                                    </ul> 
                                </div>

                                <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                    <h3>Follow</h3>
                                    <ul class="footer-links">
                                        <li><a href="#">Facebook</a></li>
                                        <li><a href="#">Twitter</a></li>
                                        <li><a href="#">Google+</a></li>
                                        
                                    </ul> 
                                </div>
                            </div>
                        </div>

                        <div class="big-column col-md-7 col-sm-12 col-xs-12">
                            <div class="row clearfix">
                                <!--Footer Column-->
                                <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                    <h3>Contact </h3>
                                    <ul class="footer-links contact-links">
                                        <li>121 King Street, Melbourne, <br> India</li>
                                        <li><a href="#">+91 123456789</a></li>
                                        <li><a href="#">info@soumikcharitabletrust.org</a></li>
                                    </ul> 
                                </div>

                                <!--Footer Column-->
                                <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                    <h3>About</h3>
                                    <div class="text">
                                        <p>Capitalize on low hanging fruit to identify value added activityto beta test. Override the digital divide with additional clickthroughs from</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!--Bottom-->
        <div class="footer-bottom">
            <div class="auto-container">
                <div class="copyright">Copyright <a href="index.php">Soumik Charitable Trust</a> © 2018. All Rights Reserved.</div>
            </div>
        </div>
    </footer>
    <!-- End Footer -->
</div>  
<!--End pagewrapper-->
	
	<!-- Modal -->
    <div id="myModal1" class="modal fade" role="dialog">
        <div class="modal-dialog">
			<!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                  <!--   <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                    <h4 class="modal-title">
                        <div class="text-center">
                            <img src="images/logo-2.png" alt="logo soumik" />
                        </div>
                    </h4>
                </div>

                 <?php
                  $username=$_SESSION['username'];
                 $query124 = "SELECT * FROM users where user_email='$username'";
                $result125=mysqli_query($con,$query124);
                $i=1;
       
               if($data = mysqli_fetch_assoc($result125))
               {
                ?>
                <div class="modal-body text-center">
                    <div class="text-center" style="margin-bottom: 20px">
                        <h2>Update Your Information</h2>
                       
                     </div>
                    <form class="form-horizontal" action="#" onsubmit="return false">
                        <div class="form-group">
                           <!--  <label class="control-label col-sm-2" for="password">New Password:</label> -->
                            <div class="col-sm-10 col-md-offset-1">
                                 <input type="hidden"  class="form-control" value="<?php echo $data['user_slno']; ?>" id="id" placeholder="Your Name">
                              <input type="Your Name" class="form-control" value="<?php echo $data['user_name']; ?>" id="name" placeholder="Your Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <!-- <label class="control-label col-sm-2" for="password">Confirm Password:</label> -->
                            <div class="col-sm-10 col-md-offset-1">
                              <input type="Your Email" readonly class="form-control" value="<?php echo $data['user_email']; ?>" id="email" placeholder="Your email">
                            </div>
                        </div>
                         <div class="form-group">
                            <!-- <label class="control-label col-sm-2" for="password">Confirm Password:</label> -->
                            <div class="col-sm-10 col-md-offset-1">
                              <input type="Your Email" value="<?php echo $data['user_mobile']; ?>" class="form-control" id="mobile" placeholder="Your Mobile">
                               <span class="error"></span>
                            </div>
                        </div>
                        <div class="form-group"> 
                            <div class="col-sm-12">
                              <button type="submit" id="updateaddress" class="btn btn-warning">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
                <?php
            }
            ?>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
    $('#updateaddress').on('click',function()
    {
        //alert('hi');
            var id=$('#id').val();
         var name=$('#name').val();
          var email=$('#email').val();
           var mobile=$('#mobile').val();
             var atpos = email.indexOf("@");
        var dotpos = email.lastIndexOf(".");

      
         if(name.length==0)
         {
            $('.error').css('color','red').html('Enter Name');
         }
         else if(email.length==0)
         {
           $('.error').css('color','red').html('Enter Email');
         }
          else if(mobile.length==0)
         {
           $('.error').css('color','red').html('Enter Mobile');
         }
          else if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) 
                          {
                           
                          $('.error').css('color','red').html('Enter Valid EmailId');
                                  
                           }
                           else
                           {

                $.ajax({
                              type:"post",
                              url:"profileapi.php",
                              data:{id:id,name:name,email:email,mobile:mobile},
                              success:function(data){
                                var jsondata=JSON.parse(data);
                                if(jsondata.status==1)
                                {
                                $('.error').css('color','green').html('Successfully Updated Your Details');
                                 //alert("Successfully Updated Your Details");
                                 setTimeout(function(){ 
                                      location.reload();
                                     }, 3000);
                                


                                }
                               
                                else
                                {
                                  alert("Failed To Update");
                                }
                            }
                 });
            }

    });
</script>
	<!-- Modal -->
    <div id="myModal3" class="modal fade" role="dialog">
        <div class="modal-dialog">
			<!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                   <!--  <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                    <h4 class="modal-title">
                        <div class="text-center">
                            <img src="images/logo-2.png" alt="logo soumik" />
                        </div>
                    </h4>
                </div>
                <div class="modal-body text-center">
                    <div class="text-center" style="margin-bottom: 20px">
                        <h2>Reset Passwrod</h2>
                        <p class="text-center">You have requested to reset the password. </p>
                     </div>
                    <form class="form-horizontal" action="#" onsubmit="return false">
                        <div class="form-group">
                           <!--  <label class="control-label col-sm-2" for="password">New Password:</label> -->
                            <div class="col-sm-10 col-md-offset-1">
                              <input type="password" class="form-control" id="password" placeholder="New Password">
                            </div>
                        </div>
                        <div class="form-group">
                            <!-- <label class="control-label col-sm-2" for="password">Confirm Password:</label> -->
                            <div class="col-sm-10 col-md-offset-1">
                              <input type="password" class="form-control" id="confirmpassword" placeholder="Confirm Password">
                            </div>
                        </div>
                        <div class="form-group"> 
                            <span id="errorspan" style="color: red;"></span>
                            <div class="col-sm-12">
                              <button type="submit" id="resetpassword" class="btn btn-warning">Send</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-angle-double-up"></span></div>

<script src="js/jquery.js"></script> 
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-ui.js"></script> 
<script src="js/jquery.fancybox.js"></script>
<script src="js/owl.js"></script>
<script src="js/wow.js"></script>
<script src="js/appear.js"></script>
<script src="js/script.js"></script>
</body>
</html>
<script type="text/javascript">
    $('#resetpassword').on('click',function()
    {
        var id=$('#id').val();
        var password=$('#password').val();
        var confirmpassword=$('#confirmpassword').val();

        if(password.length==0)
        {
            $('#errorspan').html('<b>Enter Password</b>');

        }
        else if(confirmpassword.length==0)
        {
            $('#errorspan').html('<b>Enter ConfirmPassword</b>');

        }
        else if(password!=confirmpassword)
        {
            $('#errorspan').html('<b>Password Missmatch</b>');

        }
        else
        {
             $.ajax({
                              type:"post",
                              url:"resetmail.php",
                              data:{id:id,password:password},
                              success:function(data){
                                var jsondata=JSON.parse(data);
                                if(jsondata.status==1)
                                {
                            
                                 alert("Successfully Reset your Password");
                                 location.reload();


                                }
                               
                                else
                                {
                                  alert("Failed To Reset Password");
                                }
                            }
                 });
        }


    });
</script>
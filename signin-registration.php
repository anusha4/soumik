<?php include('addons/header.php'); ?>

        
               <div class="sticky-header">
            <div class="auto-container clearfix">
                <!--Logo-->
                <div class="logo pull-left">
                    <a href="index.php" title=""><img src="images/logo-small.png" alt="" title=""></a>
                </div>
                <!--Right Col-->
                <div class="pull-right">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="about-us.php">About us</a></li>
                                <li class="dropdown"><a href="services.php" >Services</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="support-for-education.php"> Support for Education </a></li>    
                                        <li><a href="support-for-health.php"> Support for Health </a></li>
                                        <li><a href="helping-poor-people.php"> Helping Poor People </a></li>
                                    </ul>
                                </li>
                                <li><a href="causes-list.php">Causes</a></li> 
                                <li><a href="event-list.php">Events</a></li>                              
                                <li><a href="contact-us.php">Contact Us</a></li>
                               <?php
                                if(isset($_SESSION['username'])=='')
                                {
                                ?>
                                <li><a href="signin-registration.php">Login</a></li>
                                <?php
                            }
                            else
                            {
                            ?>

                                <li class="dropdown"><a href="services.php" ><?php echo $_SESSION['fullname'];?></a>
                                    <ul class="dropdown-menu">
                                       
                                        <li><a href="profile.php"> My Profile </a></li>
                                        <li><a href="logout.php"> Logout </a></li>
                                    </ul>
                                </li>
                                <?php
                            }
                            ?>
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>
            </div>
        </div>
    </header>   <!--End Main Header -->

    <div class="container login-register-main" >
            <div class="row">
              <div class="col-md-6 col-sm-12 col-xs-12 content-part" >
                 <div class="content-part-bg">
                    <div class="content-padding text-center">
                      <h1>Welcome to our <span class="soumik-text-reg"> Soumik charity </span> please rise your helping hand</h1>
                      <hr class="hr-only">
                      <p>Our charity lends the helping hand to the people in desperate situations. When their life becomes the destiny of the darkest path.</p>
                      <ul class="text-left">
                        <li>Education</li>
                        <li>Health</li>
                        <li>Helping Poor People</li>
                        <li>Donation</li>
                        <li>Volunteers</li>
                      </ul>
                    </div>
                 </div>
              </div>
              <div class="col-md-6 col-sm-12 col-xs-12 login-part"  >
                <!--<div class="text-center login-img" >-->
                <!--  <img src="images/logo-small.png" alt="soumik Logo" />-->
                <!--</div>-->
                <div class="login-register-form-section">
                    
                  <ul class="nav nav-tabs">
                      
                    <li class="active"><a data-toggle="tab" href="#login">Login </a></li>
                    <li style=""><a data-toggle="tab" href="#register">Registration</a></li>
                    
                  </ul>

                  <!-- <script type="text/javascript">
                      $('#activetab').on('click',function()
                      {
                        alert('hi');

                      });
                  </script> -->

                  <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="login">
                        <div class="errormessage">
                         <span class="Error_msgs"></span>
                            <span class="sucess_msgs"></span>
                           </div> 
                        <form  method="post" action="#" id="loginform" onsubmit="return false">
                            <div class="form-group" >
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                    <input type="text" name="login_email" class="form-control" placeholder="Username or email" id="login_email"  value="">
                                </div>
                            </div>
                            <div class="form-group ">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-key"></i></div>
                                    <input type="password" id="login_password" name="login_password" class="form-control" placeholder="Password">
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="checkbox" id="rememberMe" value="remember-me" class="r_checkbox">
                                <label for="rememberMe">Remember Me</label>
                                 <a href="#" class="pull-right" data-toggle="modal" data-target="#myModal">Forgot password?</a>
                            </div>  
                            
                            <input type="button" id="loginuser" value="Login" class="btn btn-success btn-custom">
                        </form>
                        <div class="logindata">
                            Don't have an account yet?
                            <a class=""data-toggle="tab" href="#register">
                                Create an account
                            </a>
                        </div>
                      </div>
                  
                      <div role="tabpanel" class="tab-pane fade" id="register">
                          <div class="errormessage">
                           <span class="Error_msg"></span>
                            <span class="sucess_msg"></span>
                            </div>
                          <!--<p id="Error_msg" style="text-align: center;"></p> -->
                        <form class="form-horizontal" id="myform" method="post" action="#" onsubmit="return false">
                            <!-- <div class="form-group ">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                    <input type="text" name="register_username" class="form-control" placeholder="Username" required="required" value="">
                                </div>
                            </div> -->
                            <div class="form-group ">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-male"></i></div>
                                    <input type="text" name="register_fullname" id="register_fullname" class="form-control" placeholder="Full name" required="required" value="">
                                </div>
                            </div>
                            <div class="form-group ">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                                    <input type="email" name="register_email" id="register_email" class="form-control" placeholder="Email" required="required" value="">
                                </div>
                            </div>
                            <div class="form-group ">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-phone"></i></div>
                                    <input type="text" name="register_mobileno" id="register_mobileno" class="form-control" placeholder="Mobileno" required="required" value="" onkeypress="return isNumberKey(event)" maxlength="10">
                                </div>
                            </div>
                            <div class="form-group ">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-lock"></i></div>
                                    <input type="password" name="register_password" id="register_password" class="form-control" placeholder="Password" required="required">
                                </div>
                            </div>
                            <div class="form-group ">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-lock"></i></div>
                                    <input type="password" name="register_cpassword" class="form-control" placeholder="Confirm Password" id="register_cpassword" required="required">
                                </div>
                            </div>
                            <div class="form-group ">
                            <label for="selectone">User Type</label>
                                <select class="form-control" name="selectusertype" id="selectusertype"  required>
                                  <option value="0">Select UserType</option>
                                  <option value="Donaters">Donaters</option>
                                  <option value="Volunteers">Volunteers</option>
                                  <option value="Others">Others</option>
                                </select>
                            </div>
                            <!--<div class="form-group" id="address" style="display: none;">-->
                            <!--    <div class="input-group">-->
                            <!--        <div class="input-group-addon"><i class="fa fa-address-book"></i></div>-->
                            <!--        <textarea type="text" name="register_address" class="form-control" placeholder="Enter Address" id="register_address" required="required"></textarea>-->
                            <!--    </div>-->
                            <!--</div>-->
                            <!--<div class="form-group" id="files" style="display: none;">-->
                            <!--    <div class="input-group">-->
                            <!--        <input type="file" name="file" id="file"><br>-->
                            <!--    </div>-->
                            <!--</div>-->
                            
                            <input type="submit" value="Register" id="userregistration" class="btn btn-success btn-custom" onclick="Validateregister()">

                           
                        </form>
                            <div class="registration">
                                Already Registered.

                                <a data-toggle="tab" href="#login">
                                    Login
                                </a>
                            </div>
                      </div>

                    </div>
                    
                    </div>
                    </div>
                  </div>
            </div>
           </div>   
          </div>
    <div class="" style="margin-top:50px"></div>      
    <!--Main Footer-->
    
    
   <!--Main Footer-->
    <footer class="main-footer" style="background-image: url(images/background/2.jpg);">
        <!--Upper-->
        <div class="footer-upper">
            <div class="auto-container">
                <div class="outer-box">
                    <!--Footer Logo-->
                    <div class="footer-logo"><a href="index.php"><img src="images/footer-logo.png" alt=""></a></div>

                    <div class="row clearfix">
                        <div class="big-column col-md-4 col-sm-12 col-xs-12">
                            <div class="row clearfix">
                                <!--Footer Column-->
                                <h3>Menu</h3>
                                <ul class="footer-links">
                                     <li><a href="index.php">Home</a></li>
                                    <li><a href="about-us.php">About us</a></li>
                                    <li><a href="causes-list.php">Causes</a></li> 
                                    <li><a href="event-list.php">Events</a></li>                              
                                    <li><a href="contact-us.php">Contact Us</a></li>
                                </ul> 
                            </div>
                        </div>

                        <div class="big-column col-md-4 col-sm-12 col-xs-12">
                            <div class="row clearfix">
                                <!--Footer Column-->
                               <h3>Contact </h3>
                                <ul class="footer-links contact-links">
                                    <li>D.No: 28-4-797-2, State Bank Colony <br>
                                     JNTU Road, Ananthapuram - 515002 (AP)</li>
                                    <li>+91 9000302235 / 9010942738</li>
                                    <li><a href="#">info@soumikcharitabletrust.org</a></li>
                                </ul> 
                            </div>
                        </div>
                        <div class="big-column col-md-4 col-sm-12 col-xs-12">
                            <div class="row clearfix">    
                                <!--Footer Column-->
                                <h3>About</h3>
                                <div class="text">
                                    <p>Our charity lends the helping hand to the people in desperate situations. When their life becomes the destiny of the darkest path.</p>
                                </div>
                                <br>
                                <h3>Follow</h3>
                                <ul class="footer-social-links">
                                    <li><a href="https://www.facebook.com/soumikcharitabletrust/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="https://twitter.com/SoumikTrust" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                    <!-- <li><a href="#"><i class="fa fa-google-plus"></i></a></li> -->
                                </ul>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--Bottom-->
        <div class="footer-bottom">
            <div class="auto-container">
                <div class="copyright">Copyright <a href="index.php">Soumik Charitable Trust</a> © 2018. All Rights Reserved.</div>
            </div>
        </div>
    </footer>
    <!-- End Footer -->
<!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">
                        <div class="text-center">
                            <img src="images/logo-2.png" alt="logo soumik" />
                        </div>
                    </h4>
                </div>
                <div class="modal-body text-center">
                    <div class="text-center" style="margin-bottom: 20px">
                        <h2>Forget Passwrod</h2>
                        <p class="text-center">Please enter your email address to request a password reset. </p>
                     </div>
                    <form class="form-horizontal" action="#">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="email">Email:</label>
                            <div class="col-sm-8">
                              <input type="email" class="form-control email_forgot_pwd" id="email" placeholder="Enter email" required="required">
                            </div>
                        </div>

                        <div class="form-group"> 
                            <div class="col-sm-12">
                              
                          <button class="btn btn-success forgot_send_btn" type="button">Submit</button>
                              <!-- <button type="submit" class="btn btn-default">Submit</button> -->
                            </div>
                        </div>
                         <span class="Error_msg1"></span>
                          <span class="sucess_msg1"></span>
                    </form>
                </div>
                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End pagewrapper-->
</div>
<!--End pagewrapper-->



<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-angle-double-up"></span></div>

<script src="js/jquery.js?soumikversion=1"></script>
<script src="js/bootstrap.min.js?soumikversion=1"></script>
<script src="js/jquery-ui.js?soumikversion=1"></script>
<script src="js/jquery.fancybox.js?soumikversion=1"></script>
<script src="js/owl.js?soumikversion=1"></script>
<script src="js/wow.js?soumikversion=1"></script>
<script src="js/appear.js?soumikversion=1"></script>
<script src="js/script.js?soumikversion=1"></script>
</body>
</html>

   <script type="text/javascript">

   function Validateregister() 
    {
        var passwords = document.getElementById("register_password");
        var register_fullname=$('#register_fullname').val();
        var register_email=$('#register_email').val();
        var register_mobileno=$('#register_mobileno').val();
        var register_password=$('#register_password').val();
        var register_cpassword=$('#register_cpassword').val();
         var selectusertype=$('#selectusertype').val();
        var atpos = register_email.indexOf("@");
        var dotpos = register_email.lastIndexOf(".");
        var regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{8,10}$/;

        if(register_fullname.length==0 || register_email.length==0 || register_mobileno.length==0 || register_password.length==0 || register_cpassword.length==0){
            $('.Error_msg').fadeIn().html("<i class='fa fa-times'></i> Please Fill All Fields!");
                        
                        $('.sucess_msg').hide();

        }
        else if(register_mobileno.length!=10){
                       $('.Error_msg').fadeIn().html("<i class='fa fa-phone'></i> required 10 digits, match requested format!");
                         $('.sucess_msg').hide(); 

                    }
                     else if(register_password.length<8)
                    {
                         $('.Error_msg').fadeIn().html("<i class='fa fa-times'></i> Password Minimum Length 8 Characters!");
                         $('.sucess_msg').hide(); 
                       
                    }
        else if(selectusertype==0){
            $('.Error_msg').fadeIn().html("<i class='fa fa-times'></i> Please Select any one");
                        
                        $('.sucess_msg').hide();

        }

       else if (atpos<1 || dotpos<atpos+2 || dotpos+2>=register_email.length) 
                          {
                           
                            $('.Error_msg').fadeIn().html("<i class='fa fa-times'></i> Please Enter Valid EmailId!");
                            $('.sucess_msg').hide(); 
                                  
                          }
                    //     
                    
                     else if (!regex.test(passwords.value))
                     {

                         $('.Error_msg').fadeIn().html("<i class='fa fa-times'></i> Password must be 8 characters including 1 uppercase letter, 1 special character, alphanumeric characters! and spaces are not allowed");
                         $('.sucess_msg').hide(); 

                     
                   }

        else if(register_password!=register_cpassword){
             $('.Error_msg').fadeIn().html("<i class='fa fa-times'></i> Password miss match");
                         $('.sucess_msg').hide(); 
        }
       
        
        else{
           
                        $.ajax({
                              type:"post",
                              url:"registrationapi.php",
                              data:{register_fullname:register_fullname,register_email:register_email,register_mobileno:register_mobileno,register_password:register_password,register_cpassword:register_cpassword,selectusertype:selectusertype},
                              success:function(data){
                                var jsondata=JSON.parse(data);
                                if(jsondata.status==1)
                                {
                            
                                  $('.Error_msg').hide(); 
                                     $('.sucess_msg').fadeIn().html("<i class='fa fa-check'></i> Account Created Successfully!");
                                      window.location="signin-registration.php";
                                     // location.reload();
                                     // $("#login")[0].onclick();
                                    // $( "#loginuser" ).trigger( "click" );


                                }
                                else if(jsondata.status==2)
                                {
                            
                                  $('.Error_msg').hide(); 
                                     $('.Error_msg').fadeIn().html("<i class='fa fa-times'></i> Account Already Exists!");
                                    


                                }
                                else
                                {
                                  //alert("failure");
                                   $('.Error_msg').fadeIn().html("<i class='fa fa-times'></i> Failed!");
                                }
                            }
                 });
                       
    }
    
    };

   </script>

<script type="text/javascript">
      $('#loginuser').on('click',function()
      {
         
        var login_email = $('#login_email').val();
        var login_password = $('#login_password').val();

        if(login_email.length==0)
        {
            $('.Error_msgs').fadeIn().html("<i class='fa fa-user'></i> Please Enter Username!");
        }
        else if(login_password.length==0)
        {
            $('.Error_msgs').fadeIn().html("<i class='fa fa-key'></i> Please Enter Password!");
        }
        else if($('.r_checkbox').prop("checked") == false)
        {
            $('.Error_msgs').fadeIn().html("<i class='fa fa-check'></i> Please check rememberMe!");
            

        }
        else{


          $.ajax({
                type : "post",
                url : "soumik_login_api.php",
                data : {login_email:login_email,login_password:login_password},
                success:function(data)
                {
                 jsondata = JSON.parse(data);
                 if(jsondata.status == 1)
                 {
                     
                // setTimeout(function(){ 
                   $('.Error_msgs').hide(); 
                 $('.sucess_msgs').fadeIn().html("<i class='fa fa-check'></i> Login  Successful Redirecting!");
                    //  }, 3000);
                window.location="index.php";
                 
                 
                 }
               
             
                 else
                 {
                      $('.Error_msgs').fadeIn().html("<i class='fa fa-times'></i> Please Enter valid Username And Password!");
                }
            }
          });
        
        }
 // document.getElementById("loginform").reset();
      });
        
     $('.forgot_send_btn').on('click',function(){
        var email_forgot_pwd = $('.email_forgot_pwd').val();
        var atpose = email_forgot_pwd.indexOf("@");
        var dotpose = email_forgot_pwd.lastIndexOf(".");
        if (email_forgot_pwd.length == 0) {
        $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Please Enter Email");
        }
        else if (atpose<1 || dotpose<atpose+2 || dotpose+2>=email_forgot_pwd.length) 
                          {
                           
                            $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Please Enter Valid EmailId!");
                            $('.sucess_msg1').hide(); 
                                  
                          }
        else
        {
          $.ajax({
                type : "post",
                url : "checkoutemail_api.php",
                data : {email_forgot_pwd:email_forgot_pwd},
                success:function(data)
                {
                 jsondata = JSON.parse(data);
                 if(jsondata.status == 1)
                 {
                     
                
                 $('.Error_msg1').hide(); 
                 $('.sucess_msg1').fadeIn().html("<i class='fa fa-check'></i> Forgot Password link send to your registered Email!");
                 
                 }
               
             
                 else
                 {
                      $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Please Enter valid Email!");
                }
            }
          });
        }
      }); 
    
  
   </script>
   <script type="text/javascript">
      
      function isNumberKey(evt){
          var charCode = (evt.which) ? evt.which : event.keyCode
          if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
          return true;
      }

    </script>
<!--   <script>-->
<!--    function yesnoCheck(that) {-->
<!--        if (that.value == "Volunteers") {-->
          
<!--            document.getElementById("address").style.display = "block";-->
<!--            document.getElementById("files").style.display = "block";-->
<!--        } else {-->
<!--            document.getElementById("address").style.display = "none";-->
<!--            document.getElementById("files").style.display = "none";-->
<!--        }-->
<!--    }-->
<!--</script>-->
<?php include('addons/header.php'); ?>

        <!-- Sticky Header -->
        <div class="sticky-header">
            <div class="auto-container clearfix">
                <!--Logo-->
                <div class="logo pull-left">
                    <a href="index.php" title=""><img src="images/logo-small.png" alt="soumik-scroll-logo" title="soumik-scroll-logo"></a>
                </div>
                <!--Right Col-->
                <div class="pull-right">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="about-us.php">About us</a></li>
                                <li class="dropdown"><a href="services.php" >Services</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="support-for-education.php"> Education </a></li>    
                                        <li><a href="support-for-health.php"> Health </a></li>
                                        <li><a href="helping-poor-people.php"> Helping People </a></li>
                                    </ul>
                                </li>
                                <li><a href="causes-list.php">Causes</a></li> 
                                <li><a href="event-list.php">Events</a></li>                              
                                <li><a href="contact-us.php">Contact Us</a></li>
                                <?php
                                if(isset($_SESSION['username'])=='')
                                {
                                ?>
                                <li><a href="signin-registration.php">Login</a></li>
                                <?php
                            }
                            else
                            {
                            ?>

                                <li class="dropdown"><a href="services.php" ><?php echo $_SESSION['fullname'];?></a>
                                    <ul class="dropdown-menu">
                                       
                                        <li><a href="profile.php"> My Profile </a></li>
                                        <li><a href="logout.php"> Logout </a></li>
                                    </ul>
                                </li>
                                <?php
                            }
                            ?>
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>
            </div>
        </div>
    </header>
    <!--End Main Header -->

    <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/events-bg.jpg);">
        <div class="auto-container">
            <h1>Event List View</h1>
            <ul class="bread-crumb clearfix">
                <li><a href="index.php">Home </a></li>
                <li>Event List View</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->

    <!-- Event Section -->
    <section class="event-section" style="background-image:url(images/background/14.jpg);">
        <div class="auto-container">
            <div class="event-list">
                <div class="row clearfix">
                    <!-- Event Block -->
                        <!-- Event Block -->
                        <?php
                 $query2 = "SELECT * FROM home_events" ;
                $result2=mysqli_query($con,$query2);
       
               while($data = mysqli_fetch_assoc($result2))
               {
                ?>
                        <div class="event-block col-md-6 col-sm-6 col-xs-12">
                            <div class="inner-box">
                                <div class="image-box-event">
                                    <span><i class="far fa-calendar-alt"></i></span>
                                    <!--<img src="images/resource/<?php echo $data['events_image']; ?>" alt="<?php echo $data['events_name']; ?>" title="<?php echo $data['events_name']; ?>">-->
                                   
                                    <div class="date"><span>
                                        <?php
                                        $originalDate = $data['events_date'];
                             $newDate = date("M-d", strtotime($originalDate));
                             echo $newDate;;
                             ?>
                                    </span></div>
                                </div>
                                <div class="content-box">
                                    <h4><a href="event-grid.php"><?php echo $data['events_name']; ?></a></h4>
                                    <ul class="info">
                                        <li><i class="fa fa-clock-o"></i>at <?php echo $data['events_fromtime']; ?> - <?php echo $data['events_totime']; ?></li>
                                        <li><i class="fa fa-map-marker"></i><?php echo $data['events_city']; ?></li>
                                    </ul>
                                    <p><?php echo $data['events_description']; ?></p>
                                </div>
                            </div>
                        </div>

                        <?php
                        }

                        ?>
                 </div>
             </div>
         </div>
                 <!-- Styled Pagination -->
            <!-- <div class="styled-pagination text-center light">
                <ul class="clearfix">
                    <li class="prev"><a href="#">Prev</a></li>
                    <li><a href="#"class="active">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#" >3</a></li>
                    <li class="next"><a href="#">Next</a></li>
                </ul>
            </div> -->

    </section>
    <!-- End Event Section -->

   

    <?php include('addons/footer.php'); ?>
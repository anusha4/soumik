<?php 

$MERCHANT_KEY = "EGeRfdkf";
$SALT = "wFLry8Jy6I";
// Merchant Key and Salt as provided by Payu.



$PAYU_BASE_URL = "https://sandboxsecure.payu.in";   // For Sandbox Mode
//$PAYU_BASE_URL = "https://secure.payu.in";      // For Production Mode

$action = '';

$posted = array();
if(!empty($_POST)) {
    //print_r($_POST);
  foreach($_POST as $key => $value) {  
    $posted[$key] = $value;

  }
}

$formError = 0;

if(empty($posted['txnid'])) {
  // Generate random transaction id
  $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
} else {
  $txnid = $posted['txnid'];
}
$hash = '';
// Hash Sequence
$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
if(empty($posted['hash']) && sizeof($posted) > 0) {
  if(
          empty($posted['key'])
          || empty($posted['txnid'])
          || empty($posted['amount'])
          || empty($posted['firstname'])
          || empty($posted['email'])
          || empty($posted['phone'])
          || empty($posted['productinfo'])
          || empty($posted['surl'])
          || empty($posted['furl'])
      || empty($posted['service_provider'])
  ) {
    $formError = 1;
  } else {
    //$posted['productinfo'] = json_encode(json_decode('[{"name":"tutionfee","description":"","value":"500","isRequired":"false"},{"name":"developmentfee","description":"monthly tution fee","value":"1500","isRequired":"false"}]'));
  $hashVarsSeq = explode('|', $hashSequence);
    $hash_string = '';
  foreach($hashVarsSeq as $hash_var) {
      $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
      $hash_string .= '|';
    }

    $hash_string .= $SALT;


    $hash = strtolower(hash('sha512', $hash_string));
    $action = $PAYU_BASE_URL . '/_payment';
  }
} elseif(!empty($posted['hash'])) {
  $hash = $posted['hash'];
  $action = $PAYU_BASE_URL . '/_payment';
}
?>
<html>
  <head>
     <style>
table {
  font-family:Arial, Helvetica, sans-serif;
  font-size:15px;
 
  height: 550px;
}
input{width:60%;}
textarea{width:60%;}

th{ font-size:16px;background:#015289;color:#FFFFFF;font-weight:bold; }
td{ font-size:16px;background:#DDE8F3; margin-top:10px; }
{padding-right:20px;}

.error {color:#FF0000; font-weight:bold;}
.fieldName
{

  text-align: right;
  padding-right:20px;
}
input{height: 30px; border-radius: 6px}
.payuform
{
  width: 60%;
  margin: 0 auto 40px;
}
.donation-form-top-text{text-align: center}
</style>
  <script>
    var hash = '<?php echo $hash ?>';
    function submitPayuForm() {
      if(hash == '') {
        return;
      }
      var payuForm = document.forms.payuForm;
      payuForm.submit();
    }
  </script>

<link href="css/forms.css?soumikversion=1" rel="stylesheet">
<link href="css/bootstrap.css?soumikversion=1" rel="stylesheet">
<link href="css/style.css?soumikversion=1" rel="stylesheet">
<link href="css/responsive.css?soumikversion=1" rel="stylesheet">
<link href="css/forms.css?soumikversion=1" rel="stylesheet">
  </head>
  <body onload="submitPayuForm()">
      <!-- Main Header-->
    <header class="main-header header-style-two">
        <!--Header Top-->
        <div class="header-top">
            <div class="auto-container">
                <div class="inner-container clearfix">
                    <div class="top-left pull-left">
                       <ul class="contact-info">
                           <li><a href="#"><span class="fa fa-envelope"></span>info@soumikcharitabletrust.org</a></li>
                       </ul>
                    </div>
                    <div class="top-left pull-right">
                       <ul class="contact-info">
                           <li><a href="#"><span class="fa fa-phone"></span> +91 9000302235 / 9010942738</a></li>
                       </ul>
                    </div>
                  
                </div>
            </div>
        </div>
        <!-- End Header Top -->

        <!-- Header Lower -->
        <div class="header-lower">
            <div class="auto-container">
               <div class="main-box clearfix">
                    <!--Logo Box-->
                    <div class="logo-box">
                        <div class="logo"><a href="index.php"><img src="images/logo-2.png" alt=""></a></div>
                    </div>

                    <!--Nav Outer-->
                    <div class="nav-outer clearfix">
                        <!-- Main Menu -->
                        <nav class="main-menu">
                            <div class="navbar-header">
                                <!-- Toggle Button -->
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>

                            <div class="navbar-collapse collapse clearfix">
                                <ul class="navigation clearfix">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="about-us.php">About us</a></li>
                                <li class="dropdown"><a href="services.php" >Services</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="support-for-education.php">Education </a></li>  
                                        <li><a href="support-for-health.php">Health </a></li>
                                        <li><a href="helping-poor-people.php"> Helping People </a></li>
                                    </ul>
                                </li>
                                <li><a href="causes-list.php">Causes</a></li>
                                <li><a href="event-list.php">Events</a></li>                            
                                <li><a href="contact-us.php">Contact Us</a></li>
                                 <!-- <li><a href="contact.php">Login</a></li> -->

                                <!-- <li class="dropdown"><a href="services.php" >Koty</a>
                                    <ul class="dropdown-menu">
                                       
                                        <li><a href="#"> My Profile </a></li>
                                        <li><a href="#"> Logout </a></li>
                                    </ul>
                                </li> -->
                            </ul>
                            </div>
                        </nav>
                        <!-- Main Menu End-->

                      

                    </div>
                </div>
            </div>
        </div>
        <!-- End Header Lower -->

        <!-- Sticky Header -->
        <div class="sticky-header">
            <div class="auto-container clearfix">
                <!--Logo-->
                <div class="logo pull-left">
                    <a href="index.php" title=""><img src="images/logo-small.png" alt="" title=""></a>
                </div>
                <!--Right Col-->
                <div class="pull-right">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="about-us.php">About us</a></li>
                                <li class="dropdown"><a href="services.php" >Services</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="support-for-education.php"> Education </a></li>  
                                        <li><a href="support-for-health.php"> Health </a></li>
                                        <li><a href="helping-poor-people.php"> Helping People </a></li>
                                    </ul>
                                </li>
                                <li><a href="causes-list.php">Causes</a></li>
                                <li><a href="event-list.php">Events</a></li>
                                <li><a href="contact-us.php">Contact Us</a></li>
                                <li class="dropdown"><a href="services.php" >Login</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#"> Name </a></li>  
                                        <li><a href="#"> My Profile </a></li>
                                        <li><a href="#"> Logout </a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>
            </div>
        </div>
    </header>
    <!--End Main Header -->
   <!--  <h2 style="text-align: center;">PayU Form</h2> -->
 
    <br/>
    <?php if($formError) { ?>

  


    <?php } ?>
     <form action="<?php echo $action; ?>" method="post" name="payuForm">

      <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
      <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
      <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />
      <input type="hidden" name="surl" value="http://soumikcharitabletrust.org/donatesuccess.php" size="64" />
      <input type="hidden" name="furl" value="http://soumikcharitabletrust.org/orderfailed.php" size="64" />
      <input type="hidden" name="service_provider" value="payu_paisa" size="64" />
      <input type="hidden" name="udf1" value="<?php echo (empty($posted['udf1'])) ? '' : $posted['udf1']; ?>" />
      <input type="hidden" name="udf2" value="<?php echo (empty($posted['udf2'])) ? '' : $posted['udf2']; ?>" />
      <input type="hidden" name="udf3" value="<?php echo (empty($posted['udf3'])) ? '' : $posted['udf3']; ?>" />
      <input type="hidden" name="udf4" value="<?php echo (empty($posted['udf4'])) ? '' : $posted['udf4']; ?>" />
      <input type="hidden" name="udf5" value="<?php echo (empty($posted['udf5'])) ? '' : $posted['udf5']; ?>" />
      <input type="hidden" name="pg" value="<?php echo (empty($posted['pg'])) ? '' : $posted['pg']; ?>" />
      <input type="hidden" name="curl" value="" />

    
    </div>
      <div class="payuform">
         <h3 class="text-center donation-form-top-text">Pay U Form </h3>
          <p style="color:red;text-align: center;"><b>Please Fill All Mandatory Fields</b></p>

      <table width="100%" cellpadding="2" cellspacing="2" border="0">
      
         <tr>
          <!--   <th colspan="2">Transaction Details</th> -->

        </tr>
        <tr>
         <td class="fieldName" ><span class="error">*</span> Full name </td>
          <td ><input name="firstname" id="firstname" value="<?php echo (empty($posted['firstname'])) ? '' : $posted['firstname']; ?>" /></td>
        </tr>
        <tr>
      
             <td class="fieldName"><span class="error">*</span> Amount </td>
             <td > <input name="amount" value="<?php echo (empty($posted['amount'])) ? '' : $posted['amount'] ?>" /></td>
                
        
        </tr>
        <tr>
       
           <td class="fieldName" ><span class="error">*</span> Email </td>
          <td><input name="email" id="email" value="<?php echo (empty($posted['email'])) ? '' : $posted['email']; ?>" /></td>
        </tr>
         
        <tr>

          <td class="fieldName" ><span class="error">*</span> Phone </td>
          <td><input name="phone" value="<?php echo (empty($posted['phone'])) ? '' : $posted['phone']; ?>" /></td>
        </tr>
        <tr>
         <!--  <td>Product Info: </td> -->
           <td class="fieldName" ><span class="error">*</span> Product Info </td>
          <td colspan="3"><textarea width="100%"  name="productinfo"><?php echo (empty($posted['productinfo'])) ? '' : $posted['productinfo'] ?></textarea></td>
        </tr>
    


     
     
        <tr>
          <td class="fieldName"><span class="error">*</span>Address1: </td>
          <td><input name="address1" value="<?php echo (empty($posted['address1'])) ? '' : $posted['address1']; ?>" /></td>
        </tr>
        <tr>
         <td class="fieldName"><span class="error">*</span>Address2: </td>
          <td><input name="address2" value="<?php echo (empty($posted['address2'])) ? '' : $posted['address2']; ?>" /></td>
        </tr>
       
        <tr>
          <td class="fieldName"><span class="error">*</span>City: </td>
          <td><input name="city" value="<?php echo (empty($posted['city'])) ? '' : $posted['city']; ?>" /></td>
       
        </tr>
          <tr>
           <td class="fieldName"><span class="error">*</span>State:</td>
          <td><input name="state" value="<?php echo (empty($posted['state'])) ? '' : $posted['state']; ?>" /></td>
        </tr>
        <tr>
          <td class="fieldName"><span class="error">*</span>Country: </td>
          <td><input name="country" value="<?php echo (empty($posted['country'])) ? '' : $posted['country']; ?>" /></td>
        </tr>
        <tr>
          <td class="fieldName"><span class="error">*</span>Zipcode: </td>
          <td><input name="zipcode" value="<?php echo (empty($posted['zipcode'])) ? '' : $posted['zipcode']; ?>" /></td>
        </tr>
    
        <tr>
          <?php if(!$hash) { ?>
            <td colspan="4" style="text-align: center;"><input type="submit" style="background-color: #337ab7;border-color: #337ab7;" value="Submit" class="btn btn-primary" /></td>
          <?php } ?>
        </tr>
      </table>
    </div>
    </form>
  
      <!--Main Footer-->
    <footer class="main-footer" style="background-image: url(images/background/2.jpg);">
        <!--Upper-->
        <div class="footer-upper">
            <div class="auto-container">
                <div class="outer-box">
                    <!--Footer Logo-->
                    <div class="footer-logo"><a href="index.php"><img src="images/footer-logo.png" alt=""></a></div>

                    <div class="row clearfix">
                        <div class="big-column col-md-4 col-sm-12 col-xs-12">
                            <div class="row clearfix">
                                <!--Footer Column-->
                                <h3>Menu</h3>
                                <ul class="footer-links">
                                     <li><a href="index.php">Home</a></li>
                                    <li><a href="about.php">About us</a></li>
                                    <li><a href="causes-list.php">Causes</a></li>
                                    <li><a href="event-list.php">Events</a></li>                            
                                    <li><a href="contact.php">Contact Us</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="big-column col-md-4 col-sm-12 col-xs-12">
                            <div class="row clearfix">
                                <!--Footer Column-->
                               <h3>Contact </h3>
                                <ul class="footer-links contact-links">
                                    <li>D.No: 28-4-797-2, State Bank Colony <br>
                                     JNTU Road, Ananthapuram - 515002 (AP)</li>
                                    <li>+91 9000302235 / 9010942738</li>
                                    <li><a href="#">info@soumikcharitabletrust.org</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="big-column col-md-4 col-sm-12 col-xs-12">
                            <div class="row clearfix">  
                                <!--Footer Column-->
                                <h3>About</h3>
                                <div class="text">
                                    <p>Our charity lends the helping hand to the people in desperate situations. When their life becomes the destiny of the darkest path.</p>
                                </div>
                                <br>
                                <h3>Follow</h3>
                                <ul class="footer-social-links">
                                    <li><a href="https://www.facebook.com/soumikcharitabletrust/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--Bottom-->
        <div class="footer-bottom">
            <div class="auto-container">
                <div class="copyright">Copyright <a href="index.php">Soumik Charitable Trust</a> © 2018. All Rights Reserved.</div>
            </div>
        </div>
    </footer>
    <!-- End Footer -->

</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-angle-double-up"></span></div>

<script src="js/jquery.js?soumikversion=1"></script>
<script src="js/bootstrap.min.js?soumikversion=1"></script>
<script src="js/jquery-ui.js?soumikversion=1"></script>
<script src="js/jquery.fancybox.js?soumikversion=1"></script>
<script src="js/owl.js?soumikversion=1"></script>
<script src="js/wow.js?soumikversion=1"></script>
<script src="js/appear.js?soumikversion=1"></script>
<script src="js/script.js?soumikversion=1"></script>
</body>
</html>
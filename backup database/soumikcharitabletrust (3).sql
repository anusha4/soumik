-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 09, 2018 at 11:05 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `soumikcharitabletrust`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_login`
--

CREATE TABLE `admin_login` (
  `admin_id` int(11) NOT NULL,
  `admin_uname` varchar(100) NOT NULL,
  `admin_password` varchar(100) NOT NULL,
  `admin_addeddate` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_login`
--

INSERT INTO `admin_login` (`admin_id`, `admin_uname`, `admin_password`, `admin_addeddate`) VALUES
(1, 'admin', 'admin', '');

-- --------------------------------------------------------

--
-- Table structure for table `donations`
--

CREATE TABLE `donations` (
  `donations_id` int(11) NOT NULL,
  `donations_name` varchar(100) NOT NULL,
  `donations_description` varchar(1000) NOT NULL,
  `donations_typepageredirect` varchar(100) NOT NULL,
  `donations_addedon` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `donations`
--

INSERT INTO `donations` (`donations_id`, `donations_name`, `donations_description`, `donations_typepageredirect`, `donations_addedon`) VALUES
(1, 'Education', ' Soumik charity stood strong to help the students who are looking for an assistance to continue their education at any level. ', 'support-for-education\r\n', '0000-00-00 00:00:00.000000'),
(2, 'Health', 'Objectively innovate empowered tured products whereas parallel platforms. the Holisticly predominate1', 'support-for-health', '0000-00-00 00:00:00.000000'),
(3, 'Helping People ', 'Soumik Charity provides free food and clothing to the families that are going through tough times in their life.', 'helping-poor-people', '0000-00-00 00:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `home_aboutus`
--

CREATE TABLE `home_aboutus` (
  `aboutus_id` int(11) NOT NULL,
  `aboutus_title` varchar(100) NOT NULL,
  `aboutus_description` varchar(1000) NOT NULL,
  `aboutus_donationtype` varchar(100) NOT NULL,
  `aboutus_image1` varchar(100) NOT NULL,
  `aboutus_image2` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_aboutus`
--

INSERT INTO `home_aboutus` (`aboutus_id`, `aboutus_title`, `aboutus_description`, `aboutus_donationtype`, `aboutus_image1`, `aboutus_image2`) VALUES
(1, 'Welcome to our  Soumik charity please rise your helping hand1', 'Integer et diam libero. Praesent quis varius nisi. Nunc vitae est soda2les, tincidunt augue ac, bland1', '', 'Prod_1_09092018_1138146bbc36.1', 'Prod_1_09092018_1138149b9724.1');

-- --------------------------------------------------------

--
-- Table structure for table `home_causes`
--

CREATE TABLE `home_causes` (
  `slno` int(11) NOT NULL,
  `cause_image` varchar(100) NOT NULL,
  `cause_title` varchar(100) NOT NULL,
  `cause_desc` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_causes`
--

INSERT INTO `home_causes` (`slno`, `cause_image`, `cause_title`, `cause_desc`) VALUES
(1, 'causes-single-1.jpg', 'Your helping hand', 'Integer et diam libero. Praesent quis varius nisi. Nunc vitae est sodales'),
(2, 'causes-single-2.jpg', 'Rise For Life', 'Integer et diam libero. Praesent varius nisi. Nunc vitae est sodales.'),
(3, 'causes-single-3.jpg', 'Heart To Heart', 'Integer et diam libero. Praesent varius nisi. Nunc vitae est sodales.'),
(4, 'causes-single-4.jpg', 'Causes', 'Integer et diam libero. Praesent varius nisi. Nunc vitae est sodales.');

-- --------------------------------------------------------

--
-- Table structure for table `home_contactus`
--

CREATE TABLE `home_contactus` (
  `contact_id` int(11) NOT NULL,
  `contact_name` varchar(100) NOT NULL,
  `contact_email` varchar(100) NOT NULL,
  `contact_mobileno` varchar(100) NOT NULL,
  `contact_message` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_contactus`
--

INSERT INTO `home_contactus` (`contact_id`, `contact_name`, `contact_email`, `contact_mobileno`, `contact_message`) VALUES
(1, '', '', '', ''),
(2, '', '', '', ''),
(3, 'koti', 'kotymca199@gmail.com', '7799679638', 'sample message'),
(4, 'koti', 'kotymca199@gmail.com', '7799679638', 'sample message'),
(5, 'malli', 'kotymca199@gmail.com', '7799679638', 'sample message'),
(6, 'koty', 'kotymca199@gmail.com', '7799679638', 'sample message'),
(7, 'koty', 'kotymca199@gmail.com', '7799679638', 'sample message'),
(8, '', '', '', ''),
(9, '', '', '', ''),
(10, '', '', '', ''),
(11, '', '', '', ''),
(12, '', '', '', ''),
(13, 'koti', 'kotymca199@gmail.com', '7799679638', 'sample message');

-- --------------------------------------------------------

--
-- Table structure for table `home_donaters`
--

CREATE TABLE `home_donaters` (
  `donaters_slno` int(11) NOT NULL,
  `donaters_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `donaters_mobile` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `donaters_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `donaters_state` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `donaters_district` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `donaters_address` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `donaters_pincode` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `donaters_amount` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `donaters_donatersid` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `donaters_addedon` datetime(6) NOT NULL,
  `donaters_updatedon` datetime(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `home_events`
--

CREATE TABLE `home_events` (
  `events_id` int(11) NOT NULL,
  `events_name` varchar(100) NOT NULL,
  `events_city` varchar(100) NOT NULL,
  `events_description` varchar(100) NOT NULL,
  `events_image` varchar(100) NOT NULL,
  `events_fromtime` varchar(100) NOT NULL,
  `events_totime` varchar(100) NOT NULL,
  `events_date` date NOT NULL,
  `events_addedon` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_events`
--

INSERT INTO `home_events` (`events_id`, `events_name`, `events_city`, `events_description`, `events_image`, `events_fromtime`, `events_totime`, `events_date`, `events_addedon`) VALUES
(1, 'Gear up for giving', 'Hyderabad', 'Bring to the table win-win survival strategies to ensure proactive domination2', 'cause-1.jpg', '9:00Am', '08:00Am', '2018-09-09', '2018-09-17 18:20:48.000000'),
(2, 'Sponsor a child today', '25 Newyork City', 'Bring to the table win-win survival strategies to ensure proactive domination.', 'cause-1.jpg', '11:00Am', '1:00Am', '2018-09-06', '2018-09-17 18:21:11.000000');

-- --------------------------------------------------------

--
-- Table structure for table `home_latestnews`
--

CREATE TABLE `home_latestnews` (
  `latestnews_id` int(11) NOT NULL,
  `latestnews_name` varchar(200) NOT NULL,
  `latestnews_description` varchar(100) NOT NULL,
  `latestnews_image` varchar(100) NOT NULL,
  `latestnews_thumbnail` varchar(100) NOT NULL,
  `latestnews_addedon` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_latestnews`
--

INSERT INTO `home_latestnews` (`latestnews_id`, `latestnews_name`, `latestnews_description`, `latestnews_image`, `latestnews_thumbnail`, `latestnews_addedon`) VALUES
(1, 'Donated books for helping a child to prosper in her education.', 'Capitalize on low hanging fruit to identify value added activity to beta test. Override the digital ', 'press-release-thumb-1.jpg', 'press-release-1.jpg', ''),
(2, 'Provided financial assistance for a dengue patient. We are happy that the child we helped  recovered fastly.  ', 'Capitalize on low hanging fruit to identify value added activity to beta test. Override the digital ', 'press-release-thumb-2.jpg', 'press-release-3.jpg', ''),
(3, 'Provided financial assistance for a patient suffering from kidney related issues.', 'Capitalize on low hanging fruit to identify value added activity to beta test. Override the digital ', 'press-release-thumb-3.jpg', 'press-release-2.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `home_milestone`
--

CREATE TABLE `home_milestone` (
  `milestone_id` int(11) NOT NULL,
  `milestone_name` varchar(100) NOT NULL,
  `milestone_candidatename` varchar(100) NOT NULL,
  `milestone_designation` varchar(100) NOT NULL,
  `milestone_amount` varchar(100) NOT NULL,
  `milestone_receiveddate` datetime(6) NOT NULL,
  `milestone_icon` varchar(100) NOT NULL,
  `milestone_addedon` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_milestone`
--

INSERT INTO `home_milestone` (`milestone_id`, `milestone_name`, `milestone_candidatename`, `milestone_designation`, `milestone_amount`, `milestone_receiveddate`, `milestone_icon`, `milestone_addedon`) VALUES
(1, 'School', 'Koti', 'Developer', '10', '2018-09-07 00:00:00.000000', 'flaticon-classroom', '2018-09-07 00:00:00.000000'),
(2, 'Books', 'Nani', 'Desiner', '200', '2018-09-07 00:00:00.000000', 'flaticon-study', '2018-09-07 00:00:00.000000'),
(3, 'Doctors', 'Manikyam', 'Specialist', '50', '2018-09-07 00:00:00.000000', 'flaticon-doctor', '2018-09-07 00:00:00.000000'),
(4, 'Teachers', 'Indra', 'Headmaster', '40', '2018-09-07 00:00:00.000000', 'flaticon-people', '2018-09-07 00:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `home_reviews`
--

CREATE TABLE `home_reviews` (
  `reviews_id` int(11) NOT NULL,
  `reviews_name` varchar(100) NOT NULL,
  `reviews_designation` varchar(100) NOT NULL,
  `reviews_description` varchar(200) NOT NULL,
  `reviews_image` varchar(100) NOT NULL,
  `reviews_addedon` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_reviews`
--

INSERT INTO `home_reviews` (`reviews_id`, `reviews_name`, `reviews_designation`, `reviews_description`, `reviews_image`, `reviews_addedon`) VALUES
(1, 'From Tanakallu Village', 'Ananthapur', 'The trust members of soumik charitable trust had provided me the textbooks for my academic studies. Their help towards children like me shows their effort in bringing out the talents.', 'thumb-1.jpg', '0000-00-00 00:00:00.000000'),
(2, 'Narasimha Reddy\' Mother', 'Kadavalavaripalli Village', 'My son was hospitalized because of dengue fever. We received financial assistance from soumik charitable trust for treatment.', 'thumb-2.jpg', '0000-00-00 00:00:00.000000'),
(3, 'Najurulla', 'Chikatimanipalli', 'From few days I was suffering from kidney related disease. The financial help from soumik charitable trust and their concern towards us shows their dedication towards providing service to the people.', 'thumb-3.jpg', '0000-00-00 00:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `home_seo`
--

CREATE TABLE `home_seo` (
  `seo_slno` int(11) NOT NULL,
  `seo_page` varchar(100) NOT NULL,
  `seo_title` varchar(100) NOT NULL,
  `seo_keywords` varchar(1000) NOT NULL,
  `seo_description` varchar(10000) NOT NULL,
  `seo_addedon` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_seo`
--

INSERT INTO `home_seo` (`seo_slno`, `seo_page`, `seo_title`, `seo_keywords`, `seo_description`, `seo_addedon`) VALUES
(1, 'index.php', 'Soumik Charitable Trust-Fund raising|Hyderabad|Andhra Pradesh-India', 'fundraising online,best child sponsor organizations india,donate to help the poor,fundraising website, Online dontation, health charity organizations, sponsor a child for education, children donation for education,Non profit for poor in ananthapur,charitable trust in andhra pradesh,ngos in andhra pradesh,ngos in ananthapur,charitable trust in anantapur,ngos in india,fund raising ngos in india', 'Soumik Charitable Trust is located in kadiri for providing education,scholorships,food and clothing and donate to help the poor those who are backward for their survival.', '2018-11-07 02:07:12.000000'),
(2, 'about-us.php', 'Help for Education|Health|Food-NGO-Soumik Charitable Trust', 'best child sponsor organizations,non profit organizations,health charity organizations,ngo working for education,ngo for child education,fundraising website, Online Dontation,Non profit for poor in ananthapur,charitable trust in andhra pradesh,ngos in andhra pradesh,ngos in ananthapur,charitable trust in anantapur,ngos in india,fund raising ngos in india', 'Soumik Charitable Trust is a non-profit organization working for underprivileged people to provide education for children and endless service or for helpless people', '2018-11-09 03:06:09.000000'),
(3, 'support-for-education.php', 'Sponsor a child for education|Scholorships|NGO in Hyderabad|Anantpur-India', 'sponsor a child for education, child education,best child sponsor organizations,children donation for education,ngo working for education,ngo for child education,sponsor a child india, Online dontation,Non profit for poor in anantapur,charitable trust in andhra pradesh,ngos in andhra pradesh,ngos in anantapur,charitable trust in anantapur,ngos in india,fund raising ngos in india', 'It is the mission of Soumik Charitable Trust to ensure that no talent goes waste. We Sponsor a child for education who is unable to pay the school fees and we povide scholorships', '2018-11-09 02:11:08.141250'),
(4, 'support-for-health.php', 'Charity help for health|NGO in Hyderabad|Ananthapur-India', 'non profit organizations in hyderabad,health charity organizations, best health charities, Online dontation,Non profit for poor in ananthapur,charitable trust in andhra pradesh,ngos in andhra pradesh,ngos in ananthapur,charitable trust in anantapur,ngos in india,fund raising ngos in india', 'Soumik Charitable Trust provides help for health. Our charity lends the helping hand to the people in desperate situations.', '2018-11-09 02:14:11.109148'),
(5, 'helping-poor-people.php', 'Donate to help the poor|Hyderabad-Soumik charitable Trust', 'help the poor,non profit organizations,donate to help the poor,Helping the Poor, Online dontation', 'Donate to help the poor people who is in need of help.Soumik Charity provides free food and clothing to the families who are in need of  basic needs.', '0000-00-00 00:00:00.000000'),
(6, 'contact-us.php', 'Contact Us-Non-Profit Organization|Soumik Charitable Trust', 'best child sponsor organizations,non profit organizations,health charity organizations in hyderabad,health charity organizations in hyderabad,ngo working for education,ngo for child education,fundraising website, online donation,Non profit for poor in ananthapur,charitable trust in andhra pradesh,ngos in andhra pradesh,ngos in ananthapur,charitable trust in anantapur,ngos in india,fund raising ngos in india', 'Soumik Charitable Trust is a non profit organization open for 24/7 to provide service. Contact us anytime to sponcer a child and become an volunteer mail us at info@soumikcharitabletrust.org', '2018-11-09 03:13:12.055141');

-- --------------------------------------------------------

--
-- Table structure for table `home_slider`
--

CREATE TABLE `home_slider` (
  `slider_slno` int(10) NOT NULL,
  `slider_title` varchar(100) NOT NULL,
  `slider_imgname` varchar(100) NOT NULL,
  `slider_img` varchar(100) NOT NULL,
  `slider_video` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_slider`
--

INSERT INTO `home_slider` (`slider_slno`, `slider_title`, `slider_imgname`, `slider_img`, `slider_video`) VALUES
(1, 'Secure the future of India by helping <br>\r\nthe poor children to get education', 'slide1', 'image-1.jpg', 'https://www.youtube.com/watch?v=Fvae8nxzVz4'),
(2, 'Save the faith and trust of <br>\r\nhelpless patients looking out of treatment', 'slide2', 'image-2.jpg', 'https://www.youtube.com/watch?v=Fvae8nxzVz4'),
(4, 'Live and let live by sharing <br>\r\nfood and clothes with the poor', 'slide4', 'image-3.jpg', 'https://www.youtube.com/watch?v=Fvae8nxzVz4'),
(5, 'Help poor people life <br>and their <span class=\"style-font\">formation</span>', 'slide3', 'image-3.jpg', 'http://pahani.in/upload/banners/pahani123.mp4');

-- --------------------------------------------------------

--
-- Table structure for table `home_volunteers`
--

CREATE TABLE `home_volunteers` (
  `volunteers_id` int(11) NOT NULL,
  `volunteers_name` varchar(100) NOT NULL,
  `volunteers_designation` varchar(100) NOT NULL,
  `volunteers_image` varchar(200) NOT NULL,
  `volunteers_addedon` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_volunteers`
--

INSERT INTO `home_volunteers` (`volunteers_id`, `volunteers_name`, `volunteers_designation`, `volunteers_image`, `volunteers_addedon`) VALUES
(1, 'Dhanujay', '', 'pic5.png', '2018-09-06 03:08:08.136143'),
(2, 'Radhika', '', 'pic6.png', '2018-09-06 03:08:07.000000'),
(3, 'Santhosh', '', 'pic6.png', '2018-09-06 03:07:07.000000'),
(4, 'Sreenath', '', 'pic6.png', '2018-09-06 03:07:09.000000');

-- --------------------------------------------------------

--
-- Table structure for table `home_workflow`
--

CREATE TABLE `home_workflow` (
  `workflow_id` int(11) NOT NULL,
  `workflow_title` varchar(100) NOT NULL,
  `workflow_description` varchar(100) NOT NULL,
  `workflow_process1title` varchar(100) NOT NULL,
  `workflow_process1description` varchar(1000) NOT NULL,
  `workflow_process2title` varchar(100) NOT NULL,
  `workflow_process2description` varchar(1000) NOT NULL,
  `workflow_process3title` varchar(100) NOT NULL,
  `workflow_process3description` varchar(1000) NOT NULL,
  `workflow_backgroundimage` varchar(100) NOT NULL,
  `workflow_backgroundvideo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_workflow`
--

INSERT INTO `home_workflow` (`workflow_id`, `workflow_title`, `workflow_description`, `workflow_process1title`, `workflow_process1description`, `workflow_process2title`, `workflow_process2description`, `workflow_process3title`, `workflow_process3description`, `workflow_backgroundimage`, `workflow_backgroundvideo`) VALUES
(1, '<h2>We <span>help</span> thousands of children <br> to get their <span>education</span></h2>', 'Podcasting operational change management inside of workflows to establish a framework. Taking seamle', 'Make Donation', 'Keeping your eye on the ball while performing a deep dive on the start-up mentality to derive convergence', 'Become a Volunteer', 'Keeping your eye on the ball while performing a deep dive on the start-up mentality to derive convergence1', 'Give Scholarship', 'Keeping your eye on the ball while performing a deep dive on the start-up mentality to derive convergence2', 'image-1.jpg', 'https://www.youtube.com/watch?v=Fvae8nxzVz4');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_slno` int(11) NOT NULL,
  `user_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_mobile` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_confirmpassword` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_usertype` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `user_addedon` datetime(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_slno`, `user_name`, `user_email`, `user_mobile`, `user_password`, `user_confirmpassword`, `user_usertype`, `user_addedon`) VALUES
(1, 'anu', 'anushareddy4.m@gmail.com', '9705573760', 'Koti@123', 'Koti@123', 'Donaters', '2018-10-24 19:44:37.000000'),
(2, 'anu', 'anushareddy@gmail.com', '9705573765', 'Anusha@4', 'Anusha@4', 'Volunteers', '2018-10-25 12:44:21.000000'),
(3, 'anu', 'anusha.cenit@gmail.com', '9705573765', 'Anusha@4', 'Anusha@4', 'Volunteers', '2018-10-25 13:20:45.000000'),
(5, 'soumya', 'sowmya@gmail.com', '8547963254', 'Sowmya@1', 'Sowmya@1', 'Donaters', '2018-10-25 15:40:43.000000'),
(6, 'Santosh Kothakota', 'santosharena153@gmail.com', '9052156254', '$@Ntosh123', '$@Ntosh123', 'Volunteers', '2018-10-27 21:25:23.000000'),
(7, 'koti', 'kotymca199@gmail.com', '7799670638', 'Koti@123', 'Koti@123', 'Donaters', '2018-11-01 13:01:03.000000');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_login`
--
ALTER TABLE `admin_login`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `donations`
--
ALTER TABLE `donations`
  ADD PRIMARY KEY (`donations_id`);

--
-- Indexes for table `home_aboutus`
--
ALTER TABLE `home_aboutus`
  ADD PRIMARY KEY (`aboutus_id`);

--
-- Indexes for table `home_causes`
--
ALTER TABLE `home_causes`
  ADD PRIMARY KEY (`slno`);

--
-- Indexes for table `home_contactus`
--
ALTER TABLE `home_contactus`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `home_donaters`
--
ALTER TABLE `home_donaters`
  ADD PRIMARY KEY (`donaters_slno`);

--
-- Indexes for table `home_events`
--
ALTER TABLE `home_events`
  ADD PRIMARY KEY (`events_id`);

--
-- Indexes for table `home_latestnews`
--
ALTER TABLE `home_latestnews`
  ADD PRIMARY KEY (`latestnews_id`);

--
-- Indexes for table `home_milestone`
--
ALTER TABLE `home_milestone`
  ADD PRIMARY KEY (`milestone_id`);

--
-- Indexes for table `home_reviews`
--
ALTER TABLE `home_reviews`
  ADD PRIMARY KEY (`reviews_id`);

--
-- Indexes for table `home_seo`
--
ALTER TABLE `home_seo`
  ADD PRIMARY KEY (`seo_slno`);

--
-- Indexes for table `home_slider`
--
ALTER TABLE `home_slider`
  ADD PRIMARY KEY (`slider_slno`);

--
-- Indexes for table `home_volunteers`
--
ALTER TABLE `home_volunteers`
  ADD PRIMARY KEY (`volunteers_id`);

--
-- Indexes for table `home_workflow`
--
ALTER TABLE `home_workflow`
  ADD PRIMARY KEY (`workflow_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_slno`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_login`
--
ALTER TABLE `admin_login`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `donations`
--
ALTER TABLE `donations`
  MODIFY `donations_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `home_aboutus`
--
ALTER TABLE `home_aboutus`
  MODIFY `aboutus_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `home_causes`
--
ALTER TABLE `home_causes`
  MODIFY `slno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `home_contactus`
--
ALTER TABLE `home_contactus`
  MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `home_donaters`
--
ALTER TABLE `home_donaters`
  MODIFY `donaters_slno` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `home_events`
--
ALTER TABLE `home_events`
  MODIFY `events_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `home_latestnews`
--
ALTER TABLE `home_latestnews`
  MODIFY `latestnews_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `home_milestone`
--
ALTER TABLE `home_milestone`
  MODIFY `milestone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `home_reviews`
--
ALTER TABLE `home_reviews`
  MODIFY `reviews_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `home_seo`
--
ALTER TABLE `home_seo`
  MODIFY `seo_slno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `home_slider`
--
ALTER TABLE `home_slider`
  MODIFY `slider_slno` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `home_volunteers`
--
ALTER TABLE `home_volunteers`
  MODIFY `volunteers_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `home_workflow`
--
ALTER TABLE `home_workflow`
  MODIFY `workflow_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_slno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

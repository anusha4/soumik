<?php include('addons/header.php'); ?>
  <style>
.donat{
    background-color: #f2f2f2;
    padding: 50px 0;
}
.trustdonation{
   
background: linear-gradient(45deg,#ea7325 1%,#fcc005 100%);
border-color:orange;
margin: auto ;
display:block;

}
.trustdonation:hover{
    background: linear-gradient(45deg,#ea7325 1%,#fcc005 100%);
    border-color:orange;
}

.p{
    padding:10px;
}
.h3-space{margin:50px auto;}
</style>
   
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1  donation-form-top-text ">
                    <h3 class="text-center" >Donate For Trust </h3>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10 col-md-offset-1 donat" >
                    <div class="col-md-10 col-md-offset-1 ">

                        <form action="#" onsubmit="return false">
                            <div class="form-group">
                              <label >Name:</label>
                              <input type="text" class="form-control name" placeholder="Enter Name" >
                              <span class="error"></span>
                            </div>
                            <div class="form-group">
                              <label>Email</label>
                              <input type="text" class="form-control email" placeholder="Enter Email">
                            </div>
                            <div class="form-group">
                              <label>Phone Number</label>
                              <input type="text" class="form-control phonenumber" placeholder="Enter Phone Number" maxlength="10" onkeypress="return isNumberKey(event)">
                            </div>
                             <div class="form-group">
                              <label >Address Line1</label>
                              <input type="text" class="form-control address1" placeholder="Enter Address">
                            </div>
                            <div class="form-group">
                              <label >Address Line2</label>
                              <input type="text" class="form-control address2" placeholder="Enter Address">
                            </div>
                            <div class="form-group">
                              <label >Town/City</label>
                              <input type="text" class="form-control city" placeholder="Enter Town/City">
                            </div>
                            <div class="form-group">
                              <label >Postal Code</label>
                              <input type="text" class="form-control postalcode" placeholder="Enter Postal Code" onkeypress="return isNumberKey(event)" maxlength="6">
                            </div>
                            <div class="form-group">
                              <label >Country</label>
                              <input type="text" class="form-control country" placeholder="Enter Country">
                            </div>
                            <div class="form-group">
                              <label >Donate Amount</label>
                              <input type="text" class="form-control amount" onkeypress="return isNumberKey(event)" placeholder="Enter Donation Amount">
                            </div>
                            <button type="submit" class="btn btn-danger trustdonation">Donation Amount</button>
                        </form>
                        <hr>
                        <div><strong>State Bank of India (SBI)</strong>
                            <p>Account Name: Soumik Charity Trust</p>
                             <p>Account No: 35999662561</p>
                            <p>IFSC Code: SBIN0015613</p>
                        </div>
                        &#8212; &#8212; &#8212; &#8212; &#8212; &#8212; &#8212; &#8212; &#8212; &#8212; &#8212; &#8212; &#8212;<br />
                        <div><strong>Paytm</strong>
                            <p>Paytm Number:##########</p>
                        </div>
                            &#8212; &#8212; &#8212; &#8212; &#8212; &#8212; &#8212; &#8212; &#8212; &#8212; &#8212; &#8212; &#8212;<br />
                            <div><strong>Google Pay</strong>
                            <p>Paytm Number:##########</p>
                            </div>
                            
                            &#8212; &#8212; &#8212; &#8212; &#8212; &#8212; &#8212; &#8212; &#8212; &#8212; &#8212; &#8212; &#8212;<br />
                        
                        <div>
                            <p>For Soumikcharitabletrust acknowledgement, please call us on +91 8008008005, +91 40 3001 3001</p>
                        </div>
                        
                        
                        
                        
                    </div>
                </div>
            </div>

        </div>

         
    <div class="" style="margin-top:50px"></div>     
    <!--Main Footer-->
   <?php include('addons/footer.php'); ?>
     
      function isNumberKey(evt){
          var charCode = (evt.which) ? evt.which : event.keyCode
          if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
          return true;
      }

   
    </script>



<script type="text/javascript">
    $('.trustdonation').on('click',function()
    {
        var name=$('.name').val();
        var email=$('.email').val();
        var phonenumber=$('.phonenumber').val();
        var address1=$('.address1').val();
        var address2=$('.address2').val();
        var city=$('.city').val();
        var postalcode=$('.postalcode').val();
        var country=$('.country').val();
        var amount=$('.amount').val();
        var atpos = email.indexOf("@");
        var dotpos = email.lastIndexOf(".");
       
       
        if(name.length==0)
        {
        alert('Please Enter Name')
        }

        else if(email.length==0)
        {
            alert('Please Enter Email')
        }
        else if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length)
           {
            alert('Please Enter valid Email')
         
                
         }
        else if(phonenumber.length==0)
        {
            alert('Please Enter Phone Number')
        }
        else if(address1.length==0)
        {
            alert('Please Enter Address')
        }
        else if(address2.length==0)
        {
            alert('Please Enter Address')
        }
        else if(city.length==0)
        {
            alert('Please Enter City')
        }
        else if(postalcode.length==0)
        {
            alert('Please Enter Postal Code')
        }
        else if(country.length==0)
        {
            alert('Please Enter Country')
        }
        else if(amount.length==0)
        {
            alert('Please Enter Amount')
        }




    });
</script>
</body>
</html>

<?php include('addons/header.php'); ?>

        <!-- Sticky Header -->
        <div class="sticky-header">
            <div class="auto-container clearfix">
                <!--Logo-->
                <div class="logo pull-left">
                    <a href="index.php" title=""><img src="images/logo-small.png" alt="soumik-scroll-logo" title="soumik-scroll-logo"></a>
                </div>
                <!--Right Col-->
                <div class="pull-right">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="about-us.php">About us</a></li>
                                <li class="dropdown"><a href="services.php" >Services</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="support-for-education.php"> Education </a></li>    
                                        <li><a href="support-for-health.php"> Health </a></li>
                                        <li><a href="helping-poor-people.php"> Helping People </a></li>
                                    </ul>
                                </li>
                                <li><a href="causes-list.php">Causes</a></li> 
                                <li><a href="event-list.php">Events</a></li>                              
                                <li><a href="contact-us.php">Contact Us</a></li>
                                <?php
                                if(isset($_SESSION['username'])=='')
                                {
                                ?>
                                <li><a href="signin-registration.php">Login</a></li>
                                <?php
                            }
                            else
                            {
                            ?>

                                <li class="dropdown"><a href="services.php" ><?php echo $_SESSION['fullname'];?></a>
                                    <ul class="dropdown-menu">
                                       
                                        <li><a href="profile.php"> My Profile </a></li>
                                        <li><a href="logout.php"> Logout </a></li>
                                    </ul>
                                </li>
                                <?php
                            }
                            ?>
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>
            </div>
        </div>
    </header>
    <!--End Main Header -->

     <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/poor-people-bg.jpg);">
        <div class="auto-container">
            <h1>Helping Poor People</h1>
            <ul class="bread-crumb clearfix">
                <li><a href="index.php">Home </a></li>
                <!--   <li><a href="services.php">Services</li> -->
                <li>Helping Poor People</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    <section class="work-section style-two alternate">
        <div class="auto-container">
            <!-- Work Steps -->
            <div class="steps-outer">
                <div class="row clearfix">
                    <hr>
                   <div class="sec-title text-center">
               <h2> Food & clothing <span> Distribution </span></h2>
                <div class="separator-two"></div>
            </div>
            <br>
                    

                    <div class="work-step poor-inner col-md-12 col-sm-12 col-xs-12">
                        <div class="inner-box">
                            <h3 style="color: orange;text-align: center;"><span style="font-weight: bold;font-size: 30px;">“</span> Live and Let Live <span style="font-weight: bold;font-size: 30px;">”</span></h3><br>
                            <p>Food, shelter and clothing are the prime aspects to lead a peaceful life. There are many families and individuals who are not able to cope up with the living cost in the society. Most of them are living under poverty line and are not able to make sufficient money from their daily wages.</p><br>
                            <p>Soumik Charity provides free food and clothing to the families that are going through tough times in their life. We feel that it is our responsibility to share food and clothing with our fellow individuals rather than explaining it as a “Help”. Being a Human is all about experiencing the difficulties that are faced by the fellow individuals.</p><br>
                                <p><b>Soumik Charity helps the families and individuals in different ways:</b></p><br>
                                   <ul>
                                <li><b>*</b> We provide free food on special occasions to all the poor people in Kadiri Area in Andhra Pradesh state.</li>
                                <li><b>*</b> If we get any notice about the difficulty of any family, our team will approach them and do the needful by extending our help hand.</li>
                                <li><b>*</b>We provide the Food grains and clothes to the orphanages in the area.</li>
                                <li><b>*</b> We will take care of the beggars in Kadiri by joining them in the orphanages and care centres.</li>
                            </ul><br>
                               <p> Soumik charity listens to your problems and helps to stand on our feet from your rough times.</p>
                            
                             </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
    <!--end work section-->
    
    

   <?php include('addons/footer.php'); ?>
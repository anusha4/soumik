<?php include('addons/header.php'); ?>

     <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/10.jpg);">
        <div class="auto-container">
            <h1>Causes Grid</h1>
            <ul class="bread-crumb clearfix">
                <li><a href="index.php">Home </a></li>
                <li>Causes Grid</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->

     <!-- Causes Section -->
    <section class="causes-section">
        <div class="auto-container">
        	<div class="causes-grid">
	            <div class="row clearfix">
	                <!-- Cause Block -->
	                <div class="cause-block col-md-4 col-sm-6 col-xs-12">
	                    <div class="inner-box">
	                        <div class="image-box">
	                            <figure><img src="images/resource/cause-1.jpg" alt=""></figure>
	                            <div class="overlay-box"><a href="#" class="link">Donate Now ></a></div>
	                        </div>
	                        <div class="lower-content">
	                            <h2><a href="causes-single.php">Back To School</a></h2>
	                            <p>Integer et diam libero. Praesent  varius nisi. Nunc vitae est sodales.</p>
	                            
	                            <div class="progress-bar">
	                                <div class="bar-inner"><div class="bar progress-line" data-width="65"><div class="bar-percentage"><div class="count-box"><span class="count-text" data-speed="2000" data-stop="65">0</span>%</div></div></div></div>
	                            </div>
	                                
	                            <div class="info-box clearfix">
	                                <a href="#" class="raised">Raised: <span>$1000</span></a>
	                                <a href="#" class="goal">Goal: <span>$11000</span></a>
	                            </div>  
	                        </div>
	                    </div>
	                </div>

	                <!-- Cause Block -->
	                <div class="cause-block col-md-4 col-sm-6 col-xs-12">
	                    <div class="inner-box">
	                        <div class="image-box">
	                            <figure><img src="images/resource/cause-2.jpg" alt=""></figure>
	                            <div class="overlay-box"><a href="#" class="link">Donate Now ></a></div>
	                        </div>
	                        <div class="lower-content">
	                            <h2><a href="causes-single.php">Rise For Life</a></h2>
	                            <p>Integer et diam libero. Praesent  varius nisi. Nunc vitae est sodales.</p>
	                            
	                            <div class="progress-bar">
	                                <div class="bar-inner"><div class="bar progress-line" data-width="20"><div class="bar-percentage"><div class="count-box"><span class="count-text" data-speed="2000" data-stop="20">0</span>%</div></div></div></div>
	                            </div>
	                                
	                            <div class="info-box clearfix">
	                                <a href="#" class="raised">Raised: <span>$1000</span></a>
	                                <a href="#" class="goal">Goal: <span>$12000</span></a>
	                            </div>  
	                        </div>
	                    </div>
	                </div>

	                <!-- Cause Block -->
	                <div class="cause-block col-md-4 col-sm-6 col-xs-12">
	                    <div class="inner-box">
	                        <div class="image-box">
	                            <figure><img src="images/resource/cause-3.jpg" alt=""></figure>
	                            <div class="overlay-box"><a href="#" class="link">Donate Now ></a></div>
	                        </div>
	                        <div class="lower-content">
	                            <h2><a href="causes-single.php">Heart To Heart</a></h2>
	                            <p>Integer et diam libero. Praesent  varius nisi. Nunc vitae est sodales.</p>
	                            
	                            <div class="progress-bar">
	                                <div class="bar-inner"><div class="bar progress-line" data-width="80"><div class="bar-percentage"><div class="count-box"><span class="count-text" data-speed="2000" data-stop="80">0</span>%</div></div></div></div>
	                            </div>
	                                
	                            <div class="info-box clearfix">
	                                <a href="#" class="raised">Raised: <span>$1000</span></a>
	                                <a href="#" class="goal">Goal: <span>$13000</span></a>
	                            </div>  
	                        </div>
	                    </div>
	                </div>

	                <!-- Cause Block -->
	                <div class="cause-block col-md-4 col-sm-6 col-xs-12">
	                    <div class="inner-box">
	                        <div class="image-box">
	                            <figure><img src="images/resource/cause-4.jpg" alt=""></figure>
	                            <div class="overlay-box"><a href="#" class="link">Donate Now ></a></div>
	                        </div>
	                        <div class="lower-content">
	                            <h2><a href="causes-single.php">Back To School</a></h2>
	                            <p>Integer et diam libero. Praesent  varius nisi. Nunc vitae est sodales.</p>
	                            
	                            <div class="progress-bar">
	                                <div class="bar-inner"><div class="bar progress-line" data-width="65"><div class="bar-percentage"><div class="count-box"><span class="count-text" data-speed="2000" data-stop="65">0</span>%</div></div></div></div>
	                            </div>
	                                
	                            <div class="info-box clearfix">
	                                <a href="#" class="raised">Raised: <span>$1000</span></a>
	                                <a href="#" class="goal">Goal: <span>$11000</span></a>
	                            </div>  
	                        </div>
	                    </div>
	                </div>

	                <!-- Cause Block -->
	                <div class="cause-block col-md-4 col-sm-6 col-xs-12">
	                    <div class="inner-box">
	                        <div class="image-box">
	                            <figure><img src="images/resource/cause-5.jpg" alt=""></figure>
	                            <div class="overlay-box"><a href="#" class="link">Donate Now ></a></div>
	                        </div>
	                        <div class="lower-content">
	                            <h2><a href="causes-single.php">Education for Child</a></h2>
	                            <p>Integer et diam libero. Praesent  varius nisi. Nunc vitae est sodales.</p>
	                            
	                            <div class="progress-bar">
	                                <div class="bar-inner"><div class="bar progress-line" data-width="20"><div class="bar-percentage"><div class="count-box"><span class="count-text" data-speed="2000" data-stop="20">0</span>%</div></div></div></div>
	                            </div>
	                                
	                            <div class="info-box clearfix">
	                                <a href="#" class="raised">Raised: <span>$1000</span></a>
	                                <a href="#" class="goal">Goal: <span>$12000</span></a>
	                            </div>  
	                        </div>
	                    </div>
	                </div>

	                <!-- Cause Block -->
	                <div class="cause-block col-md-4 col-sm-6 col-xs-12">
	                    <div class="inner-box">
	                        <div class="image-box">
	                            <figure><img src="images/resource/cause-6.jpg" alt=""></figure>
	                            <div class="overlay-box"><a href="#" class="link">Donate Now ></a></div>
	                        </div>
	                        <div class="lower-content">
	                            <h2><a href="causes-single.php">Shelter for Poor Child</a></h2>
	                            <p>Integer et diam libero. Praesent  varius nisi. Nunc vitae est sodales.</p>
	                            
	                            <div class="progress-bar">
	                                <div class="bar-inner"><div class="bar progress-line" data-width="80"><div class="bar-percentage"><div class="count-box"><span class="count-text" data-speed="2000" data-stop="80">0</span>%</div></div></div></div>
	                            </div>
	                                
	                            <div class="info-box clearfix">
	                                <a href="#" class="raised">Raised: <span>$1000</span></a>
	                                <a href="#" class="goal">Goal: <span>$13000</span></a>
	                            </div>  
	                        </div>
	                    </div>
	                </div>

	                <!-- Cause Block -->
	                <div class="cause-block col-md-4 col-sm-6 col-xs-12">
	                    <div class="inner-box">
	                        <div class="image-box">
	                            <figure><img src="images/resource/cause-7.jpg" alt=""></figure>
	                            <div class="overlay-box"><a href="#" class="link">Donate Now ></a></div>
	                        </div>
	                        <div class="lower-content">
	                            <h2><a href="causes-single.php">Sponsor a child today</a></h2>
	                            <p>Integer et diam libero. Praesent  varius nisi. Nunc vitae est sodales.</p>
	                            
	                            <div class="progress-bar">
	                                <div class="bar-inner"><div class="bar progress-line" data-width="65"><div class="bar-percentage"><div class="count-box"><span class="count-text" data-speed="2000" data-stop="65">0</span>%</div></div></div></div>
	                            </div>
	                                
	                            <div class="info-box clearfix">
	                                <a href="#" class="raised">Raised: <span>$1000</span></a>
	                                <a href="#" class="goal">Goal: <span>$11000</span></a>
	                            </div>  
	                        </div>
	                    </div>
	                </div>

	                <!-- Cause Block -->
	                <div class="cause-block col-md-4 col-sm-6 col-xs-12">
	                    <div class="inner-box">
	                        <div class="image-box">
	                            <figure><img src="images/resource/cause-8.jpg" alt=""></figure>
	                            <div class="overlay-box"><a href="#" class="link">Donate Now ></a></div>
	                        </div>
	                        <div class="lower-content">
	                            <h2><a href="causes-single.php">Rise For Life</a></h2>
	                            <p>Integer et diam libero. Praesent  varius nisi. Nunc vitae est sodales.</p>
	                            
	                            <div class="progress-bar">
	                                <div class="bar-inner"><div class="bar progress-line" data-width="20"><div class="bar-percentage"><div class="count-box"><span class="count-text" data-speed="2000" data-stop="20">0</span>%</div></div></div></div>
	                            </div>
	                                
	                            <div class="info-box clearfix">
	                                <a href="#" class="raised">Raised: <span>$1000</span></a>
	                                <a href="#" class="goal">Goal: <span>$12000</span></a>
	                            </div>  
	                        </div>
	                    </div>
	                </div>

	                <!-- Cause Block -->
	                <div class="cause-block col-md-4 col-sm-6 col-xs-12">
	                    <div class="inner-box">
	                        <div class="image-box">
	                            <figure><img src="images/resource/cause-9.jpg" alt=""></figure>
	                            <div class="overlay-box"><a href="#" class="link">Donate Now ></a></div>
	                        </div>
	                        <div class="lower-content">
	                            <h2><a href="causes-single.php">Donation for child</a></h2>
	                            <p>Integer et diam libero. Praesent  varius nisi. Nunc vitae est sodales.</p>
	                            
	                            <div class="progress-bar">
	                                <div class="bar-inner"><div class="bar progress-line" data-width="80"><div class="bar-percentage"><div class="count-box"><span class="count-text" data-speed="2000" data-stop="80">0</span>%</div></div></div></div>
	                            </div>
	                                
	                            <div class="info-box clearfix">
	                                <a href="#" class="raised">Raised: <span>$1000</span></a>
	                                <a href="#" class="goal">Goal: <span>$13000</span></a>
	                            </div>  
	                        </div>
	                    </div>
	                </div>
	            </div>
        	</div>

        	<!-- <div class="styled-pagination text-center">
                <ul class="clearfix">
                    <li class="prev"><a href="#">Prev</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#" class="active">3</a></li>
                    <li class="next"><a href="#">Next</a></li>
                </ul>
            </div> -->
        </div>
    </section>
    <!-- End Causes Section -->

    

   <?php include('addons/footer.php'); ?>
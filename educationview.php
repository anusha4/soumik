<?php  include('admin/api/db.php');  ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Soumik Charity | Services</title>

<!-- Stylesheets -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">

<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
<link rel="icon" href="images/favicon.png" type="image/x-icon">
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>

<body>

<div class="page-wrapper">
    <!-- Preloader -->
    <div class="preloader"></div>
    
    <!-- Main Header-->
    <header class="main-header header-style-two">
        <!--Header Top-->
        <div class="header-top">
            <div class="auto-container">
                <div class="inner-container clearfix">
                    <div class="top-left pull-left">
                       <ul class="contact-info">
                           <li><a href="#"><span class="fa fa-envelope"></span> info@soumikcharitabletrust.org</a></li>
                           <li><a href="#"><span class="fa fa-phone"></span> +91 123456789</a></li>
                       </ul>
                    </div>
                    <!-- <div class="top-right pull-right clearfix">
                        <div class="lang"><a href="#"><span class="fa fa-globe"></span> Eng</a></div>
                        <div class="login-info">
                            <a href="#"><span class="fa fa-sign-in"></span>Donor Login</a>
                            <a href="#">Non-Profit Login</a>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
        <!-- End Header Top -->

        <!-- Header Lower -->
        <div class="header-lower">
            <div class="auto-container">
               <div class="main-box clearfix">
                    <!--Logo Box-->
                    <div class="logo-box">
                        <div class="logo"><a href="index.php"><img src="images/logo-2.png" alt=""></a></div>
                    </div>

                    <!--Nav Outer-->
                    <div class="nav-outer clearfix">
                        <!-- Main Menu -->
                        <nav class="main-menu">
                            <div class="navbar-header">
                                <!-- Toggle Button -->      
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            
                            <div class="navbar-collapse collapse clearfix">
                                <ul class="navigation clearfix">
                                    <li><a href="index.php">Home</a></li>
									<li><a href="about.php">About us</a></li>
									<li><a href="services.php">Services</a></li>
									<li><a href="causes-list.php">Causes</a></li> 
									<li><a href="event-list.php">Events</a></li>                              
									<li><a href="contact.php">Contact Us</a></li>
                                </ul>
                            </div>
                        </nav>
                        <!-- Main Menu End-->

                        

                    </div>
                </div>
            </div>
        </div>
        <!-- End Header Lower -->

        <!-- Sticky Header -->
        <div class="sticky-header">
            <div class="auto-container clearfix">
                <!--Logo-->
                <div class="logo pull-left">
                    <a href="index.php" title=""><img src="images/logo-small.png" alt="" title=""></a>
                </div>
                <!--Right Col-->
                <div class="pull-right">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li><a href="index.php">Home</a></li>
									<li><a href="about.php">About us</a></li>
									<li><a href="services.php">Services</a></li>
									<li><a href="causes-list.php">Causes</a></li> 
									<li><a href="event-list.php">Events</a></li>                              
									<li><a href="contact.php">Contact Us</a></li>
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>
            </div>
        </div>
    </header>
    <!--End Main Header -->

    <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/9.jpg);">
        <div class="auto-container">
            <h1>Education</h1>
            <ul class="bread-crumb clearfix">
                <li><a href="index.php">Home </a></li>
                <li><a href="services.php">Services</li>
                    <li>Education</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    <section class="work-section style-two alternate">
        <div class="auto-container">
            <!-- Work Steps -->
            <div class="steps-outer">
                <div class="row clearfix">
                    <hr>
                   <div class="sec-title text-center">
               <h2> Support for <span> Children Education</span></h2>
                <div class="separator-two"></div>
            </div>
            <br>
                    

                    <div class="work-step education-inner col-md-12 col-sm-12 col-xs-12">
                        <div class="inner-box">
                            <h3 style="color: orange;text-align: center;"><span style="font-weight: bold;font-size: 30px;">“</span> The future vests in the hands of the Younger generation <span style="font-weight: bold;font-size: 30px;">”</span></h3><br>
                            <p>Education is vital aspect to shape the future of the younger generation. But, just question yourself, how many youngsters are able to attend the school and afford the expenses in the education system. There are many bright children who are not able to attend the school or college or higher studies, just because of the financial disability. </p><br>
                                <p><b>Why most of the children are becoming child labour?</b><p><br>
                                   <ul>
                                <li><b>*</b> Lack of financial support</li>
                                <li><b>*</b> Lack of food and clothing</li>
                                <li><b>*</b> Lack of awareness about the education to the family</li>
                                <li><b>*</b> Lack of support from the family</li>
                                <li><b>*</b> Some are becoming orphans at a younger age</li>
                            </ul><br>
                               <p> Despite all the odds, Soumik charity stood strong to help the students who are looking for an assistance to continue their education at any level. Soumik Charity conducted awareness programs and enlightens the parents about the value of education and helped many students to enjoy their schooling.</p><br>
                                <p>Soumik charity has the intent for building a better society and we believe that helping an individual will certainly provide a better future for many individuals in the society.</p><br>

                            <p>    Support the children to continue their education and become a part in building a bright future for the future generations.</p>
                            
                             </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
    <!--end work section-->



    <!-- Subscribe section -->
    <section class="subscribe-section">
        <div class="auto-container">
            <div class="inner-container">
                <div class="row clearfix">
                    <div class="title-column col-md-6 col-sm-12 col-xs-12">
                        <div class="title">
                            <h2>Donation Hotline :  03 2689254 - 03 2685987</h2>
                        </div>
                    </div>
                    <div class="form-column col-md-6 col-sm-12 col-xs-12">
                        <div class="subscribe-form">
                            <form method="post" action="contact.php">
                                <div class="form-group">
                                    <input type="email" name="email" value="" placeholder="Enter Your email" required="">
                                    <button type="submit" class="theme-btn btn-style-three">Subscribe</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Subscribe section -->

    <!--Main Footer-->
    <footer class="main-footer" style="background-image: url(images/background/2.jpg);">
        <!--Upper-->
        <div class="footer-upper">
            <div class="auto-container">
                <div class="outer-box">
                    <!--Footer Logo-->
                    <div class="footer-logo"><a href="index.php"><img src="images/footer-logo.png" alt=""></a></div>
                    
                    <div class="row clearfix">
                        <div class="big-column col-md-5 col-sm-12 col-xs-12">
                            <div class="row clearfix">
                                <!--Footer Column-->
                                <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                    <h3>Menu</h3>
                                    <ul class="footer-links">
                                         <li><a href="index.php">Home</a></li>
                                        <li><a href="about.php">About us</a></li>
                                        <li><a href="causes-list.php">Causes</a></li> 
                                        <li><a href="event-list.php">Events</a></li>                              
                                        <li><a href="contact.php">Contact Us</a></li>
                                    </ul> 
                                </div>

                                <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                    <h3>Follow</h3>
                                    <ul class="footer-links">
                                        <li><a href="#">Facebook</a></li>
                                        <li><a href="#">Twitter</a></li>
                                        <li><a href="#">Google+</a></li>
                                        
                                    </ul> 
                                </div>
                            </div>
                        </div>

                        <div class="big-column col-md-7 col-sm-12 col-xs-12">
                            <div class="row clearfix">
                                <!--Footer Column-->
                                <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                    <h3>Contact </h3>
                                    <ul class="footer-links contact-links">
                                        <li>121 King Street, Melbourne, <br> India</li>
                                        <li><a href="#">+91 123456789</a></li>
                                        <li><a href="#">info@soumikcharitabletrust.org</a></li>
                                    </ul> 
                                </div>

                                <!--Footer Column-->
                                <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                    <h3>About</h3>
                                    <div class="text">
                                        <p>Our charity lends the helping hand to the people in desperate situations. When their life becomes the destiny of the darkest path.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!--Bottom-->
        <div class="footer-bottom">
            <div class="auto-container">
                <div class="copyright">Copyright <a href="index.php">Soumik Charitable Trust</a> © 2018. All Rights Reserved.</div>
            </div>
        </div>
    </footer>
    <!-- End Footer -->

</div>  
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-angle-double-up"></span></div>

<script src="js/jquery.js"></script> 
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-ui.js"></script> 
<script src="js/jquery.fancybox.js"></script>
<script src="js/owl.js"></script>
<script src="js/wow.js"></script>
<script src="js/appear.js"></script>
<script src="js/script.js"></script>
</body>
</html>
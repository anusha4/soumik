<?php  include('admin/api/db.php');  ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Soumik Charity | Contact</title>

<!-- Stylesheets -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">

<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
<link rel="icon" href="images/favicon.png" type="image/x-icon">
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>

<body>

<div class="page-wrapper">
    <!-- Preloader -->
    <div class="preloader"></div>
    
    <!-- Main Header-->
    <header class="main-header header-style-two">
        <!--Header Top-->
        <div class="header-top">
            <div class="auto-container">
                <div class="inner-container clearfix">
                    <div class="top-left pull-left">
                       <ul class="contact-info">
                           <li><a href="#"><span class="fa fa-envelope"></span> info@soumikcharitabletrust.org</a></li>
                           <li><a href="#"><span class="fa fa-phone"></span> +91 123456789</a></li>
                       </ul>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- End Header Top -->

        <!-- Header Lower -->
        <div class="header-lower">
            <div class="auto-container">
               <div class="main-box clearfix">
                    <!--Logo Box-->
                    <div class="logo-box">
                        <div class="logo"><a href="index.php"><img src="images/logo-2.png" alt=""></a></div>
                    </div>

                    <!--Nav Outer-->
                    <div class="nav-outer clearfix">
                        <!-- Main Menu -->
                        <nav class="main-menu">
                            <div class="navbar-header">
                                <!-- Toggle Button -->      
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            
                            <div class="navbar-collapse collapse clearfix">
                                <ul class="navigation clearfix">
                                    <li><a href="index.php">Home</a></li>
                                    <li><a href="about.php">About us</a></li>
                                    <li class="dropdown"><a href="services.php" >Services</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="education.php"> Support for Education </a></li>    
                                            <li><a href="health.php"> Support for Health </a></li>
                                            <li><a href="helpingpoorpeople.php"> Helping Poor People </a></li>
                                        </ul>
                                    </li>
                                    <li><a href="causes-list.php">Causes</a></li> 
                                    <li><a href="event-list.php">Events</a></li>                              
                                    <li><a href="contact.php">Contact Us</a></li>
                                </ul>
                            </div>
                        </nav>
                        <!-- Main Menu End-->

                        
                    </div>
                </div>
            </div>
        </div>
        <!-- End Header Lower -->

        <!-- Sticky Header -->
        <div class="sticky-header">
            <div class="auto-container clearfix">
                <!--Logo-->
                <div class="logo pull-left">
                    <a href="index.php" title=""><img src="images/logo-small.png" alt="" title=""></a>
                </div>
                <!--Right Col-->
                <div class="pull-right">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="about.php">About us</a></li>
                                <li class="dropdown"><a href="services.php" >Services</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="education.php"> Support for Education </a></li>    
                                        <li><a href="health.php"> Support for Health </a></li>
                                        <li><a href="helpingpoorpeople.php"> Helping Poor People </a></li>
                                    </ul>
                                </li>
                                <li><a href="causes-list.php">Causes</a></li> 
                                <li><a href="event-list.php">Events</a></li>                              
                                <li><a href="contact.php">Contact Us</a></li>
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>
            </div>
        </div>
    </header>
    <!--End Main Header -->

    <!-- Contact Page Section -->
    <section class="contact-page-section ">
        <div class="inner-container clearfix ">
            <!-- Form Column -->
            <div class="form-column registration-page-section clearfix">
                <!-- Comment Form -->
                    <div class="contact-form ">
                        <h4>Donor Registration</h4>

                        <!--Contact Form-->
                        <form method="post" action="#" onsubmit="return false">
                            <div class="row clearfix">
                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <input type="text" name="username" id="name" placeholder="Name">
                                </div>
                                
                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <input type="email" name="email" id="email" placeholder="Mail">
                                </div>
                                 <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <input type="text" name="email" id="mobileno" placeholder="Mobileno">
                                </div>
                                
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <textarea name="message" id="comment" placeholder="Message"></textarea>
                                </div>
                                
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group text-center">
                                    <button class="theme-btn btn-style-one" id="submit_contact" type="submit" name="submit-form">Send Now</button>
                                </div>
                            </div>
                        </form>
                    </div><!--endif Contact Form -->
                </div>
            </div>
            
            
        </div>
    </section>
    <!-- End Contact Page Section -->

    <!--Main Footer-->
    <footer class="main-footer" style="background-image: url(images/background/2.jpg);">
        <!--Upper-->
        <div class="footer-upper">
            <div class="auto-container">
                <div class="outer-box">
                    <!--Footer Logo-->
                    <div class="footer-logo"><a href="index.php"><img src="images/footer-logo.png" alt=""></a></div>
                    
                    <div class="row clearfix">
                        <div class="big-column col-md-5 col-sm-12 col-xs-12">
                            <div class="row clearfix">
                                <!--Footer Column-->
                                <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                    <h3>Menu</h3>
                                    <ul class="footer-links">
                                        <li><a href="index.php">Home</a></li>
                                        <li><a href="about.php">About us</a></li>
                                        <li><a href="causes-list.php">Causes</a></li> 
                                        <li><a href="event-list.php">Events</a></li>                              
                                        <li><a href="contact.php">Contact Us</a></li>
                                       
                                    </ul> 
                                </div>

                                <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                    <h3>Follow</h3>
                                    <ul class="footer-links">
                                        <li><a href="#">Facebook</a></li>
                                        <li><a href="#">Twitter</a></li>
                                        <li><a href="#">Google+</a></li>
                                        
                                    </ul> 
                                </div>
                            </div>
                        </div>

                        <div class="big-column col-md-7 col-sm-12 col-xs-12">
                            <div class="row clearfix">
                                <!--Footer Column-->
                                <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                    <h3>Contact </h3>
                                    <ul class="footer-links contact-links">
                                        <li>121 King Street, Melbourne, <br> India</li>
                                        <li><a href="#">+91 123456789</a></li>
                                        <li><a href="#">info@soumikcharitabletrust.org</a></li>
                                    </ul> 
                                </div>

                                <!--Footer Column-->
                                <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                    <h3>About</h3>
                                    <div class="text">
                                        <p>Our charity lends the helping hand to the people in desperate situations. When their life becomes the destiny of the darkest path.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!--Bottom-->
        <div class="footer-bottom">
            <div class="auto-container">
                <div class="copyright">Copyright <a href="index.php">Soumik Charitable Trust</a> © 2018. All Rights Reserved.</div>
            </div>
        </div>
    </footer>
    <!-- End Footer -->

</div>  
<!--End pagewrapper-->


<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-angle-double-up"></span></div>
<script src="js/jquery.js"></script> 
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-ui.js"></script> 
<script src="js/jquery.fancybox.js"></script>
<script src="js/owl.js"></script>
<script src="js/wow.js"></script>
<script src="js/validate.js"></script>
<script src="js/script.js"></script>
<!--Google Map APi Key-->
<!-- <script src="http://maps.google.com/maps/api/js?key=AIzaSyDKU4t6_OPgpV1FjB0yRpGAJLz9ycg4tF8"></script>
 -->
<!-- <script src='https://maps.googleapis.com/maps/api/js?v=3&libraries=places&key=AIzaSyAqV9BGFW-A_WUYrZch4B-UEI3c_9A1j7U'></script> -->

<script src="http://maps.google.com/maps/api/js?key=AIzaSyBKS14AnP3HCIVlUpPKtGp7CbYuMtcXE2o"></script>

<script src="js/map-script.js"></script>
<!--End Google Map APi-->
</body>
</html>
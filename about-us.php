
<?php include('addons/header.php'); ?>
        <!-- Sticky Header -->
        <div class="sticky-header">
            <div class="auto-container clearfix">
                <!--Logo-->
                <div class="logo pull-left">
                    <a href="index.php" title=""><img src="images/logo-small.png" alt="soumik-scroll-logo" title="soumik-scroll-logo"></a>
                </div>
                <!--Right Col-->
                <div class="pull-right">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="about-us.php">About us</a></li>
                                <li class="dropdown"><a href="services.php" >Services</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="support-for-education.php"> Education </a></li>    
                                        <li><a href="support-for-health.php"> Health </a></li>
                                        <li><a href="helping-poor-people.php"> Helping People </a></li>
                                    </ul>
                                </li>
                                <li><a href="causes-list.php">Causes</a></li> 
                                <li><a href="event-list.php">Events</a></li>                              
                                <li><a href="contact-us.php">Contact Us</a></li>
                               <?php
                                if(isset($_SESSION['username'])=='')
                                {
                                ?>
                                <li><a href="signin-registration.php">Login</a></li>
                                <?php
                            }
                            else
                            {
                            ?>

                                <li class="dropdown"><a href="services.php" ><?php echo $_SESSION['fullname'];?></a>
                                    <ul class="dropdown-menu">
                                       
                                        <li><a href="profile.php"> My Profile </a></li>
                                        <li><a href="logout.php"> Logout </a></li>
                                    </ul>
                                </li>
                                <?php
                            }
                            ?>
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>
            </div>
        </div>
    </header>
    <!--End Main Header -->

     <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/about-bg-res.jpg);">
        <div class="auto-container">
            <h1>About Us</h1>
            <ul class="bread-crumb clearfix">
                <li><a href="index.php">Home </a></li>
                <li>About Us</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->

    <!-- About Section Two -->
    <section class="about-section-two">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="content-column col-md-6 col-sm-12 col-xs-12">
                    <div class="inner-column">
                        <div class="sec-title">
                            <span class="title">About Us</span>
                            <h2>Welcome to our <span> Soumik charity</span> please <span> rise your</span>  helping hand</h2>
                        </div>
                        <h3 style="color: orange">“Life is all about the serving the needful”<h3><br>
                        <div class="text">Time and now many people are in helpless situations for many reasons. The main aim of Soumik Charity is to help the individuals to stand back on their feet and live their life with a ray of hope.</div>
                        <div class="text">Travelling along the remote villages of India and the experiences of the innocent people facing crisis in those villages provided the intent to come up with a visionary thought in the form of “Soumik Charity”. Our charity lends the helping hand to the people in desperate situations. When their life becomes the destiny of the darkest path, we provide the ray of light to enlighten their life and experience the finest days coming ahead in the future for them.
                        </div>
                        <!-- <div class="sign-img"><img src="images/resource/sign.png" alt=""></div>
                        <a href="#" class="theme-btn btn-style-one"><span>View More Info</span></a> -->
                    </div>
                </div>

                <div class="progress-column col-md-6 col-sm-12 col-xs-12">
                   <!--Progress Bars-->
                    <div class="inner-column">
                        <div class="sec-title">
                            <span class="title">Donation</span>
                            <h2>Improving our <span>donation</span> to <br> fulfill <span>projects</span> of any level</h2>
                        </div>
                        <div class="progress-bars">

                            <!--Skill Item-->
                            <div class="bar-item">
                                <div class="skill-header clearfix">
                                    <div class="skill-title">Sponsor for Food </div>
                                </div>
                                <div class="skill-bar">
                                    <div class="bar-inner"><div class="bar progress-line" data-width="40"><div class="skill-percentage"><div class="count-box"><span class="count-text" data-speed="2000" data-stop="40">0</span> %</div></div></div></div>
                                </div>
                            </div>

                            <!--Skill Item-->
                            <div class="bar-item">
                                <div class="skill-header clearfix">
                                    <div class="skill-title">Sponsor for Water</div>
                                </div>
                                <div class="skill-bar">
                                    <div class="bar-inner"><div class="bar progress-line" data-width="53"><div class="skill-percentage"><div class="count-box"><span class="count-text" data-speed="2000" data-stop="53">0</span> %</div></div></div></div>
                                </div>
                            </div>

                            <!--Skill Item-->
                            <div class="bar-item">
                                <div class="skill-header clearfix">
                                    <div class="skill-title">Sponsor for Book</div>
                                </div>
                                <div class="skill-bar">
                                    <div class="bar-inner"><div class="bar progress-line" data-width="64"><div class="skill-percentage"><div class="count-box"><span class="count-text" data-speed="2000" data-stop="64">0</span> %</div></div></div></div>
                                </div>
                            </div>

                            <!--Skill Item-->
                            <div class="bar-item">
                                <div class="skill-header clearfix">
                                    <div class="skill-title">Sponsor for Dress</div>
                                </div>
                                <div class="skill-bar">
                                    <div class="bar-inner"><div class="bar progress-line" data-width="75"><div class="skill-percentage"><div class="count-box"><span class="count-text" data-speed="2000" data-stop="75">0</span> %</div></div></div></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Help Section -->

    <!-- Work Section -->
    <section class="work-section style-two alternate">
        <div class="auto-container">
            <!-- Work Steps -->
            <div class="steps-outer">
                <div class="row clearfix">
                    <?php
                 $query12 = "SELECT * FROM donations" ;
                $result12=mysqli_query($con,$query12);
                $i=1;
       
               while($data = mysqli_fetch_assoc($result12))
               {
                ?>

                    <div class="work-step col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <h2>0<?php echo $i ?>.</h2>
                            <h4><a href="<?php echo $data['donations_typepageredirect']; ?>.php"><?php echo $data['donations_name']; ?></a></h4>
                            <p><?php echo $data['donations_description']; ?> </p>
                        </div>
                    </div>
                    <?php
                    $i++;
                }
                    ?>
                </div>
            </div>
        </div>
    </section>
    <!-- End Work Section -->

     <!--Fun Facts Section-->
    <div class="fun-fact-section" style="background-image:url(images/background/1.jpg);">
        <div class="auto-container">
            <div class="sec-title light text-center">
                <span class="title">Our Mission</span>
                <h2>Milestone <span>achieved</span></h2>
            </div>
            <div class="fact-counter">
                <div class="row clearfix">
                     <?php
                 $query13 = "SELECT * FROM home_milestone" ;
                $result13=mysqli_query($con,$query13);
                $i=1;
       
               while($data = mysqli_fetch_assoc($result13))
               {
                ?>
                    <!--Column-->
                    <div class="counter-column col-md-3 col-sm-6 col-xs-12">
                        <div class="inner">
                            <div class="count-box">
                                <span class="count-text" data-speed="3000" data-stop="<?php echo $data['milestone_amount']; ?>">0</span>
                                <h4 class="counter-title"><?php echo $data['milestone_name']; ?></h4>
                                <span class="icon <?php echo $data['milestone_icon']; ?>"></span>
                            </div>
                        </div>
                    </div>
                    <?php
                        }
                    ?>
                   
                    

                   
                </div>
            </div>
        </div>
    </div>
    <!--End Fun Facts Section-->

    <!-- Who We Are -->
    <section class="who-we-are style-two">
        <div class="auto-container">

             <?php
                 $query199 = "SELECT * FROM home_workflow" ;
                $result199=mysqli_query($con,$query199);
                $i=1;
       
               while($data = mysqli_fetch_assoc($result199))
               {
                ?>

            <div class="row clearfix">
                <!--Content Column-->
                <div class="content-column col-md-6 col-sm-12 col-xs-12">
                    <div class="inner-box">
                        <div class="sec-title">

                            <span class="title">Who we are</span>
                           <!--  <h2>We are <span>Soumik charity</span> founding network</h2> -->
                            <?php echo $data['workflow_title']; ?>
                            <span class="separator-two"></span>
                        </div>
                        <div class="text">
                            <p> We support “education for the children”, “treatment for the poor”, “social activities to define the responsibilities of the citizens” and many more to improve the life of the poor people in the society. For a better society for the next generation, it is vital to help the poor children and single women to restore the values in the society.
                            </p>
                            <p>To solve the problems for the needy, our teams are just a minute away to approach the individuals and recognize their need. We will do the needful by sharing their problem with our International NGO wings. We believe in lending our helping hand at the right time for the right person.
                            </p>
                        </div>
                        <!-- <div class="text">
                            <p> <?php echo $data['workflow_description']; ?></p>
                        </div> -->
                        <!--  <ul class="choose-info">
                        <li>
                            <span class="icon flaticon-coin"></span>
                            <h3><a href="#"><?php echo $data['workflow_process1title']; ?></a></h3>
                            <p><?php echo $data['workflow_process1description']; ?></p>
                        </li>

                        <li>
                            <span class="icon flaticon-shirt"></span>
                            <h3><a href="#"><?php echo $data['workflow_process2title']; ?></a></h3>
                            <p><?php echo $data['workflow_process2description']; ?></p>
                        </li>

                        <li>
                            <span class="icon flaticon-globe"></span>
                            <h3><a href="#"><?php echo $data['workflow_process3title']; ?></a></h3>
                            <p><?php echo $data['workflow_process3description']; ?></p>
                        </li>
                    </ul> -->
                    </div>
                </div>

                <!-- Video Column -->
                <div class="video-column col-md-6 col-sm-12 col-xs-12">
                    <div class="inner-column">
                        <div class="video-box">
                            <figure class="image">
                                <img src="images/resource/video-2.jpg" alt="help-people-life-and-their-formation" title="help-people-life-and-their-formation">
                                <a href="<?php echo $data['workflow_backgroundvideo']; ?>" class="link" data-fancybox="gallery" data-caption=""><span class="icon fa fa-play"></span></a>
                                <div class="caption-box">
                                    <h3>Help People life and their formation</h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
              <?php
        }
        ?>
        <br>
<div class="auto-container">
        <h3 style="color: orange;text-align: center;line-height: 1;"><span style="font-weight: bold;font-size: 50px;">“</span> When you extend your helping hand to the right person during desperate times, improves their faith in the Humanity and gives them a hope to fight the hardships <span style="font-weight: bold;font-size: 50px; position:absolute;">”</span></h3>
    </div>


        </div>
    </section>
    <!-- <section>
        <div class="auto-container">
        <h3 style="color: orange;text-align: center;line-height: 1;"><span style="font-weight: bold;font-size: 50px;">“</span> When you extend your helping hand to the right person during desperate times, improves their faith in the Humanity and gives them a hope to fight the hardships<span style="font-weight: bold;font-size: 50px;">”</span></h3>
    </div>
    </section> -->
   <!-- End Who We Are -->
<!-- Volunteers Section -->
    <section class="volunteers-section alternate" >
        <div class="auto-container">
            <div class="sec-title text-center">
                <span class="title">OUR VOLUNTEER</span>
                <h2>Our list of <span>volunteers</span> are ready <br> to <span>support</span> the difficulty in the social.</h2>
                <span class="separator-two"></span>
            </div>

            <div class="row clearfix">
              

                <!-- Volunteer Block -->
                <?php
                 $query1 = "SELECT * FROM home_volunteers" ;
                $result1=mysqli_query($con,$query1);
       
               while($data = mysqli_fetch_assoc($result1))
               {
                ?>

                <div class="volunteer-block col-md-3 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image-box-volountries">
                            <figure><img src="images/resource/<?php echo $data['volunteers_image']; ?>" alt="<?php echo $data['volunteers_name']; ?>" title="<?php echo $data['volunteers_name']; ?>"></figure>
                        </div>
                        <div class="overlay-box">
                            <div class="content">
                                <h3 class="name"><?php echo $data['volunteers_name']; ?></h3>
                                <span class="designation"><?php echo $data['volunteers_designation']; ?></span>
                                <!-- <ul class="social-icon">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                </ul> -->
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>



            </div>
        </div>
    </section>
    <!-- End Volunteers Section -->
   

   

  <?php include('addons/footer.php'); ?>
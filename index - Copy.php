<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Soumik Charitable Trust-Fund raising|Hyderabad|Andhra Pradesh-India</title>

<!-- Stylesheets -->
<link href="css/bootstrap.css?soumikversion=1" rel="stylesheet">
<link href="plugins/revolution/css/settings.css?soumikversion=1" rel="stylesheet" type="text/css"><!-- REVOLUTION SETTINGS STYLES -->
<link href="plugins/revolution/css/layers.css?soumikversion=1" rel="stylesheet" type="text/css"><!-- REVOLUTION LAYERS STYLES -->
<link href="plugins/revolution/css/navigation.css?soumikversion=1" rel="stylesheet" type="text/css"><!-- REVOLUTION NAVIGATION STYLES -->
<link href="css/style.css?soumikversion=1" rel="stylesheet">
<link href="css/responsive.css?soumikversion=1" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css">
<link href="css/darkbox.css?soumikversion=1" rel="stylesheet" type="text/css">
<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
<link rel="icon" href="images/favicon.png" type="image/x-icon">

<meta property="og:url"                content="" />
    <meta property="og:type"               content="website" />
    <meta property="og:title"              content="" />
    <meta property="og:tags"               content="" />
    <meta property="og:keywords"           content="" />
    <meta property="og:description"        content="" />
    <meta property="og:image"              content="" />


   <meta name="description" content="Soumik Charitable Trust is located in kadiri for providing education,scholorships,food and clothing and donate to help the poor those who are backward for their survival." />
    <meta name="keywords" content="fundraising online,best child sponsor organizations india,donate to help the poor,fundraising website, Online dontation, health charity organizations, sponsor a child for education, children donation for education,Non profit for poor in ananthapur,charitable trust in andhra pradesh,ngos in andhra pradesh,ngos in ananthapur,charitable trust in anantapur,ngos in india,fund raising ngos in india" />

    <meta name="tags"  content="">
    <meta name="title" content="Soumik Charitable Trust-Fund raising|Hyderabad|Andhra Pradesh-India "/>

<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">


                
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127259927-1"></script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5bdc0946a5b4b87720425e91/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
<script>
window.dataLayer window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-127259927-1');
</script>
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="-1" />
</head>

<body>

        <?php

            include('admin/api/db.php');

            $query = "SELECT * FROM home_slider" ;
                $result=mysqli_query($con,$query);
                echo mysqli_error($con);
       
           $response['sliders'] = array();

            while($data = mysqli_fetch_assoc($result))
            {
                
                $slider_data['id'] = $data['slider_slno'];
                $slider_data['name'] = $data['slider_imgname'];
                $slider_data['image'] = $data['slider_img'];
                $slider_data['video'] = $data['slider_video'];
                 $slider_data['slider_title'] = $data['slider_title'];
                
            
                

                array_push($response['sliders'], $slider_data);

            }





        ?>
<div class="page-wrapper">
    <!-- Preloader -->
    <div class="preloader"></div>
    
    <!-- Main Header-->
    <header class="main-header">

        <!-- Header Lower -->
        <div class="header-lower">
            <div class="main-box clearfix">
                <!--Logo Box-->
                <div class="logo-box">
                    <div class="logo"><a href="index.php"><img src="images/logo.png" alt="soumik-logo" title="soumik-logo"></a></div>
                </div>
                <!--Nav Outer-->
                <div class="nav-outer clearfix">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-header">
                            <!-- Toggle Button -->
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="about-us.php">About us</a></li>
                                <li class="dropdown"><a href="services.php" >Services</a>
                                	<ul class="dropdown-menu">
                                		<li><a href="support-for-education.php"> Education </a></li>	
                                		<li><a href="support-for-health.php"> Health </a></li>
                                		<li><a href="helping-poor-people.php"> Helping People </a></li>
                                	</ul>
                                </li>
                                <li><a href="causes-list.php">Causes</a></li> 
                                <li><a href="event-list.php">Events</a></li>                              
                                <li><a href="contact-us.php">Contact Us</a></li>
                                 <?php
                                if(isset($_SESSION['username'])=='')
                                {
                                ?>
                                <li><a href="signin-registration.php">Login</a></li>
                                <?php
                            }
                            else
                            {
                            ?>

                                <li class="dropdown"><a href="services.php" ><?php echo $_SESSION['fullname'];?></a>
                                    <ul class="dropdown-menu">
                                       
                                        <li><a href="profile.php"> My Profile </a></li>
                                        <li><a href="logout.php"> Logout </a></li>
                                    </ul>
                                </li>
                                <?php
                            }
                            ?>
                            </ul>
                        </div>
                    </nav>
                    <!-- Main Menu End-->
                </div>
            </div>
        </div>

        <!-- Sticky Header -->
        <div class="sticky-header">
            <div class="auto-container clearfix">
                <!--Logo-->
                <div class="logo pull-left">
                    <a href="index.php" title=""><img src="images/logo-small.png" alt="soumik-scroll-logo" title="soumik-scroll-logo"></a>
                </div>
                <!--Right Col-->
                <div class="pull-right">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="about-us.php">About us</a></li>
                                <li class="dropdown"><a href="services.php" >Services</a>
                                	<ul class="dropdown-menu">
                                		<li><a href="support-for-education.php"> Education </a></li>	
                                		<li><a href="support-for-health.php"> Health </a></li>
                                		<li><a href="helping-poor-people.php"> Helping People </a></li>
                                	</ul>
                                </li>
                                <li><a href="causes-list.php">Causes</a></li> 
                                <li><a href="event-list.php">Events</a></li>                              
                                <li><a href="contact-us.php">Contact Us</a></li>
                                 <?php
                                if(isset($_SESSION['username'])=='')
                                {
                                ?>
                                <li><a href="signin-registration.php">Login</a></li>
                                <?php
                            }
                            else
                            {
                            ?>

                                <li class="dropdown"><a href="services.php" ><?php echo $_SESSION['fullname'];?></a>
                                    <ul class="dropdown-menu">
                                       
                                        <li><a href="profile.php"> My Profile </a></li>
                                        <li><a href="logout.php"> Logout </a></li>
                                    </ul>
                                </li>
                                <?php
                            }
                            ?>
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>
            </div>
        </div>
    </header>
    <!--End Main Header -->
    
    <!--Main Slider-->
    <section class="main-slider">
        <div class="rev_slider_wrapper fullwidthbanner-container"  id="rev_slider_one_wrapper" data-source="gallery">
            <div class="rev_slider fullwidthabanner" id="rev_slider_one" data-version="5.4.1">
               
                <ul>
                     <!-- Slide 1 -->
                    <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1691" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="images/main-slider/<?php echo $response['sliders'][0]['image'] ?>" data-title="Slide Title" data-transition="parallaxvertical">
                        
                        <img alt="become-a-hope" title="become-a-hope" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="images/main-slider/<?php echo $response['sliders'][0]['image'] ?>"> 
                        
                        <div class="tp-caption" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-width="none"
                        data-hoffset="['0','0','0','0']"
                        data-voffset="['-135','-135','-135','-135']"
                        data-x="['center','center','center','center']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                           <h4>Become A Hope</h4>
                        </div>

                        <div class="tp-caption" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-fontsize="['64','40','36','24']"
                        data-width="auto"
                        data-textalign="center"
                        data-hoffset="['0','0','0','0']"
                        data-voffset="['-30','-30','-30','-30']"
                        data-x="['center','center','center','center']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <h2><?php echo $response['sliders'][0]['slider_title'] ?><!-- Help poor people life <br>and their <span class="style-font">formation</span> --></h2>
                        </div>
                        
                        <div class="tp-caption" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-width="auto"
                        data-hoffset="['0','0','0','0']"
                        data-voffset="['115','105','130','130']"
                        data-x="['center','center','center','center']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <a href="payu-checkout.php" class="theme-btn btn-style-one"><span>Donate Now</span></a>
                            <a href="https://www.youtube.com/watch?v=Fvae8nxzVz4" class="theme-btn play-btn" data-fancybox="gallery" data-caption=""><span class="icon fa fa-play"></span> View Video <span> 4 : 45</span></a>
                        </div>
                    </li>
                    <!-- Slide 2 -->
                    <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1688" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="images/main-slider/<?php echo $response['sliders'][1]['image'] ?>" data-title="Slide Title" data-transition="parallaxvertical">
                        
                        <img alt="build-the-future" title="build-the-future" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="images/main-slider/<?php echo $response['sliders'][1]['image'] ?>"> 
                        
                        <div class="tp-caption" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-width="none"
                        data-hoffset="['0','0','0','0']"
                        data-voffset="['-135','-135','-135','-135']"
                        data-x="['center','center','center','center']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                           <h4>Build the future</h4>
                        </div>

                        <div class="tp-caption" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-fontsize="['64','40','36','24']"
                        data-width="auto"
                        data-textalign="center"
                        data-hoffset="['0','0','0','0']"
                        data-voffset="['-30','-30','-30','-30']"
                        data-x="['center','center','center','center']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <h2><?php echo $response['sliders'][1]['slider_title'] ?><!-- Help poor people life <br>and their <span class="style-font">formation</span> --></h2>
                        </div>
                        
                        <div class="tp-caption" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-width="auto"
                        data-hoffset="['0','0','0','0']"
                        data-voffset="['115','105','130','130']"
                        data-x="['center','center','center','center']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <a href="payu-checkout.php" class="theme-btn btn-style-one"><span>Donate Now</span></a>
                            <a href="https://www.youtube.com/watch?v=Fvae8nxzVz4" class="theme-btn play-btn" data-fancybox="gallery" data-caption=""><span class="icon fa fa-play"></span> View Video <span> 4 : 45</span></a>
                        </div>
                    </li>

                    <!-- Slide 3 -->
                    <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1689" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="images/main-slider/<?php echo $response['sliders'][2]['image'] ?>" data-title="Slide Title" data-transition="parallaxvertical">
                        
                        <img alt="be-their-happiness" title="be-their-happiness" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="images/main-slider/<?php echo $response['sliders'][2]['image'] ?>"> 
                        
                        <div class="tp-caption" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-width="none"
                        data-hoffset="['0','0','0','0']"
                        data-voffset="['-135','-135','-135','-135']"
                        data-x="['center','center','center','center']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                           <h4>Be their happiness</h4>
                        </div>

                        <div class="tp-caption" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-fontsize="['64','40','36','24']"
                        data-width="auto"
                        data-textalign="center"
                        data-hoffset="['0','0','0','0']"
                        data-voffset="['-30','-30','-30','-30']"
                        data-x="['center','center','center','center']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <h2><?php echo $response['sliders'][2]['slider_title'] ?><!-- Help poor people life <br>and their <span class="style-font">formation</span> --></h2>
                        </div>
                        
                        <div class="tp-caption" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-width="auto"
                        data-hoffset="['0','0','0','0']"
                        data-voffset="['115','105','130','130']"
                        data-x="['center','center','center','center']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <a href="payu-checkout.php" class="theme-btn btn-style-one"><span>Donate Now</span></a>
                            <a href="https://www.youtube.com/watch?v=Fvae8nxzVz4" class="theme-btn play-btn" data-fancybox="gallery" data-caption=""><span class="icon fa fa-play"></span> View Video <span> 4 : 45</span></a>
                        </div>
                    </li>

                   
				</ul>
                 
            </div>
        </div>
    </section>
    <!--End Main Slider-->

    <div class="container">
                    <div class="row">
      <div class="col-md-4">
            <div class="flip-box">
             <div class="flip-box-inner">
            <div class="flip-box-front">
           <i class="fa fa-book fa-3x" aria-hidden="true"></i> 
            <h3>EDUCATION</h3>
             <p>Education is vital aspect to shape the future of the younger generation.</p>
            </div>
            <div class="flip-box-back">
            <p class="para">Soumik charity stood strong to help the students who are looking for an assistance to continue their education at any level.</p>
              <button class="btn-bg-back">Read More</button>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
            <div class="flip-box">
             <div class="flip-box-inner">
            <div class="flip-box-front">
            <i class="fa fa-medkit fa-3x" aria-hidden="true"></i>
             <h3>HEALTH CARE</h3>
             <p>Health is a state of complete physical, mental and social well being.</p>
            </div>
            <div class="flip-box-back">
           
             <p class="para">Objectively innovate empowered tured products whereas parallel platforms. the Holisticly predominate1</p>
              <button class="btn-bg-back">Read More</button>
            </div>
          </div>
        </div>
      </div>

            <div class="col-md-4">
            <div class="flip-box">
             <div class="flip-box-inner">
            <div class="flip-box-front">
              <i class="fa fa-handshake fa-3x" aria-hidden="true"></i>
             <h3>HELPING POOR PEOPLE</h3>
             <p>Food, shelter and clothing are the prime aspects to lead a peaceful life.</p>
            </div>
            <div class="flip-box-back">
           
             <p class="para">Soumik Charity provides free food and clothing to the families that are going through tough times in their life..</p>
              <button class="btn-bg-back">Read More</button>
            </div>
          </div>
        </div>
      </div>

    </div>
</div>


    

    <!-- About Section-->
    <section class="about-section">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Content Column-->
                <div class="content-column pull-right col-md-6 col-sm-12 col-xs-12">
                    <div class="inner-column">
                         <?php
                 $query13 = "SELECT * FROM home_aboutus" ;
                $result13=mysqli_query($con,$query13);
                $i=1;
       
               while($data = mysqli_fetch_assoc($result13))
               {
                ?>

                        <div class="sec-title">
                            <span class="title">About Us</span>
                            <h2>
                                Welcome <span> to </span> Soumik charity <span>  please raise your  </span>  helping hand
                                <!-- <?php echo $data['aboutus_title']; ?> -->
                            </h2>
                            <div class="separator"></div>
                        </div>
                        <div class="text">
                                Our charity lends the helping hand to the people in desperate situations. When their life becomes the destiny of the darkest path.

                            <!-- <?php echo $data['aboutus_description']; ?> -->
                                
                            </div>
                        <div class="our-help">
                            <ul>
                                <li><a href="support-for-education.php">Education</a></li>
                                  <li><b style="color:black">Volunteers</b></li>
                                  <li><a href="support-for-health.php">Health</a></li>
                                  <li><b style="color:black">Donation</b></li>
                                <li><a href="helping-poor-people.php">Helping People</a></li>
                                
                            </ul>
                        </div>
                      <!-- <a href="about-us.php" class="theme-btn btn-style-one"><span>View More</span></a> -->
                    </div>
                </div>
               
                
                <!--Image Column-->
                <div class="images-column col-md-6 col-sm-12 col-xs-12">
                    <div class="inner-column">
                        <figure class="image-1"><img src="images/resource/<?php echo $data['aboutus_image1']; ?>" alt="welcome-to-soumik-charity-please-raise-your-helping-hand-img1" title="welcome-to-soumik-charity-please-raise-your-helping-hand-img1"></figure>
                       <!--  <figure class="image-2"><img src="images/resource/<?php echo $data['aboutus_image2']; ?>" alt="welcome-to-soumik-charity-please-raise-your-helping-hand-img2"  title="welcome-to-soumik-charity-please-raise-your-helping-hand-img2"></figure> -->
                    </div>
                </div>
                 <?php
            }
                ?>
            </div>

        </div>
    </section>
    <!--End About Section-->

    <!-- Causes Section -->
    <section class="causes-section">
        <div class="auto-container">
            <div class="sec-title text-center">
                <span class="title">Our Causes</span>
                <h2>Raise your <span>funds</span> for a <span>cause</span> that <br> you care</h2>
                <div class="separator-two"></div>
            </div>

            <div class="row clearfix">
                <!-- Cause Block -->


                <?php
                 $query13 = "SELECT * FROM home_causes" ;
                $result13=mysqli_query($con,$query13);
                $i=1;
       
               while($data = mysqli_fetch_assoc($result13))
               {
                ?>
                <div class="cause-block col-md-3 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image-box-cause">
                            <figure><img src="images/resource/<?php echo $data['cause_image']; ?>" width="250px !important" " title="<?php echo $data['cause_title']; ?>" alt="<?php echo $data['cause_title']; ?>"></figure>
                            <div class="overlay-box"><a href="payu-checkout.php" class="link">Donate Now &gt;</a></div>
                        </div>
                        <div class="lower-content">
                           <h2><a href="causes-single.php?sl_no=<?php echo $data['slno']?>"><?php echo $data['cause_title']; ?></a></h2>
                            <p><?php echo $data['cause_desc']; ?></p>
                            
                            <div class="progress-bar">
                                <div class="bar-inner"><div class="bar progress-line" data-width="0"><div class="bar-percentage"><div class="count-box"><span class="count-text" data-speed="2000" data-stop="0">0</span>%</div></div></div></div>
                            </div>
                                
                            <div class="info-box clearfix">
                                <a href="#" class="raised">Raised: <span><i class="fa fa-rupee"></i> 0 </span></a>
                                <a href="#" class="goal">Goal: <span><i class="fa fa-rupee"></i> 0 </span></a>
                            </div>  
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>

            </div>
        </div>
    </section>
    <!-- End Causes Section -->

    <!--Fun Facts Section-->
    <div class="fun-fact-section" style="background-image:url(images/background/1.jpg);">
        <div class="auto-container">
            <div class="sec-title light text-center">
                <span class="title">Our Mission</span>
                <h2>Milestone <span>achieved</span></h2>
            </div>
            <div class="fact-counter">
                <div class="row clearfix">
                     <?php
                 $query13 = "SELECT * FROM home_milestone" ;
                $result13=mysqli_query($con,$query13);
                $i=1;
       
               while($data = mysqli_fetch_assoc($result13))
               {
                ?>
                    <!--Column-->
                    <div class="counter-column col-md-3 col-sm-6 col-xs-12">
                        <div class="inner">
                            <div class="count-box">
                                <span class="count-text" data-speed="3000" data-stop="<?php echo $data['milestone_amount']; ?>">0</span>
                                <h4 class="counter-title"><?php echo $data['milestone_name']; ?></h4>
                                <span class="icon <?php echo $data['milestone_icon']; ?>"></span>
                            </div>
                        </div>
                    </div>
                    <?php
                        }
                    ?>
                   
                    

                   
                </div>
            </div>
        </div>
    </div>
    <!--End Fun Facts Section-->

    <!-- Who We Are -->
    <section class="who-we-are">
        <div class="outer-container clearfix">
            <!--Content Column-->
             <?php
                 $query199 = "SELECT * FROM home_workflow" ;
                $result199=mysqli_query($con,$query199);
                $i=1;
       
               while($data = mysqli_fetch_assoc($result199))
               {
                ?>
            <div class="content-column">
                <div class="inner-box">
                    <div class="sec-title">
                        <span class="title">Who we are</span>
                        <!-- <h2>We <span>help</span> thousands of children to get<br> their <span>education</span></h2> -->
                        <?php echo $data['workflow_title']; ?>
                        <span class="separator-two"></span>
                    </div>
                    <div class="text">
                        <p> We support “education for the children”, “treatment for the poor”, “social activities to define the responsibilities of the citizens” and many more to improve the life of the poor people in the society. For a better society for the next generation, it is vital to help the poor children and single women to restore the values in the society.
                            </p>
                            <p>To solve the problems for the needy, our teams are just a minute away to approach the individuals and recognize their need. We will do the needful by sharing their problem with our International NGO wings. We believe in lending our helping hand at the right time for the right person.
                            </p>
                        <!-- <p><?php echo $data['workflow_description']; ?></p> -->
                    </div>
                    <!-- <ul class="choose-info">
                        <li>
                            <span class="icon flaticon-coin"></span>
                            <h3><a href="#"><?php echo $data['workflow_process1title']; ?></a></h3>
                            <p><?php echo $data['workflow_process1description']; ?></p>
                        </li>

                        <li>
                            <span class="icon flaticon-shirt"></span>
                            <h3><a href="#"><?php echo $data['workflow_process2title']; ?></a></h3>
                            <p><?php echo $data['workflow_process2description']; ?></p>
                        </li>

                        <li>
                            <span class="icon flaticon-globe"></span>
                            <h3><a href="#"><?php echo $data['workflow_process3title']; ?></a></h3>
                            <p><?php echo $data['workflow_process3description']; ?></p>
                        </li>
                    </ul> -->
                </div>
            </div>

            


            <!--Image Column-->
           <!--  <?php

            ?> -->
            <div class="image-column" style="background-image:url('images/resource/<?php echo $data['workflow_backgroundimage']; ?>');">
                <figure class="image-box"><img src="images/resource/<?php echo $data['workflow_backgroundimage']; ?>" alt="who-we-are" title="who-we-are"></figure>
            </div>

            <?php
        }
        ?>


        </div>
    </section>
    <!-- End Who We Are -->

    <!-- Volunteers Section -->
    <section class="volunteers-section">
        <div class="auto-container">
            <div class="sec-title text-center">
                <span class="title">OUR VOLUNTEER</span>
                <h2>Our list of <span>volunteers</span> are ready <br> to <span>support</span> the difficulty in the social.</h2>
                <span class="separator-two"></span>
            </div>

            <div class="row clearfix">
              

                <!-- Volunteer Block -->
                <?php
                 $query1 = "SELECT * FROM home_volunteers" ;
                $result1=mysqli_query($con,$query1);
       
               while($data = mysqli_fetch_assoc($result1))
               {
                ?>

                <div class="volunteer-block col-md-3 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image-box-volountries">
                            <figure><img src="images/resource/<?php echo $data['volunteers_image']; ?>" alt="<?php echo $data['volunteers_name']; ?>" title="<?php echo $data['volunteers_name']; ?>"></figure>
                        </div>
                        <div class="overlay-box">
                            <div class="content">
                                <h3 class="name"><?php echo $data['volunteers_name']; ?></h3>
                                <span class="designation"><?php echo $data['volunteers_designation']; ?></span>
                                <!--<ul class="social-icon">-->
                                <!--    <li><a href="#"><i class="fab fa-facebook"></i></a></li>-->
                                <!--    <li><a href="#"><i class="fab fa-twitter"></i></a></li>-->
                                <!--    <li><a href="#"><i class="fab fa-google-plus"></i></a></li>-->
                                <!--    <li><a href="#"><i class="fab fa-pinterest-p"></i></a></li>-->
                                <!--</ul>-->
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>



            </div>
        </div>
    </section>
    <!-- End Volunteers Section -->

    <!-- Donation And Events -->
    <section class="donation-and-events" style="background-image: url(images/background/7.jpg);">
        <div class="auto-container">
            <div class="row clearfix">
                <!-- Donation Column -->
                <div class="donation-column col-md-7 col-sm-12 col-xs-12">
                    <div class="inner-column">
                        <div class="sec-title light">
                            <span class="title">Donation</span>
                            <h2>Make a <span>donation</span> now!</h2>
                        </div>

                        <!-- Donation Form -->
                        <div class="donation-form">
                            <div class="donation-info">
                                <p class="">We support “education for the children”, “treatment for the poor”, “social activities to define the responsibilities of the citizens” and many more to improve the life of the poor people in the society. For a better society for the next generation, it is vital to help the poor children and single women to restore the values in the society.</p>
                            </div>
                            <!--<button type="submit" class="theme-btn btn-style-one">-->
                                <a href="payu-checkout.php" class="theme-btn btn-style-one"><span>Donate Now</span></a>
                                <!--</button>-->
                            
                        </div>
                    </div>
                </div>

                <!-- Event Column -->
                <div class="event-column col-md-5 col-sm-12 col-xs-12">
                    <div class="inner-column">
                        <div class="sec-title light">
                            <span class="title">Event</span>
                            <h2>Upcoming <span>events</span></h2>
                        </div>
                        <!-- Event Block -->
                        <?php
                 $query2 = "SELECT * FROM home_events" ;
                $result2=mysqli_query($con,$query2);
       
               while($data = mysqli_fetch_assoc($result2))
               {
                ?>
                        <div class="event-block">
                            <div class="inner-box">
                                <div class="image-box-event">
                                    
                                    <span><i class="far fa-calendar-alt"></i></span>
                                    <!--<img src="images/resource/<?php echo $data['events_image']; ?>" alt="<?php echo $data['events_name']; ?>" title="<?php echo $data['events_name']; ?>">-->
                                    <div class="date"><span>
                                        <?php
                                        $originalDate = $data['events_date'];
                             $newDate = date("d-M", strtotime($originalDate));
                             echo $newDate;;
                             ?>
                                    </span></div>
                                </div>
                                <div class="content-box">
                                    <h4><a href="event-grid.php"><?php echo $data['events_name']; ?></a></h4>
                                    <ul class="info">
                                        <li><a href="event-grid.php"><i class="fa fa-clock-o"></i>at <?php echo $data['events_fromtime']; ?> - <?php echo $data['events_totime']; ?></a></li>
                                        <li><a href="event-grid.php"><i class="fa fa-map-marker"></i><?php echo $data['events_city']; ?></a></li>
                                    </ul>
                                    <p><?php echo $data['events_description']; ?></p>
                                </div>
                            </div>
                        </div>

                        <?php
                        }

                        ?>
                        <!-- Event Block -->
                      
                    </div>
                    
                </div>

            </div>
        </div>
    </section>
    <!-- End Donation And Events -->

    <!-- Testimonial Section -->
    <section class="testimonial-section">
        <div class="auto-container">
            <div class="sec-title text-center">
                <span class="title">Testimonials</span>
                <h2>Our <span>client’s</span> reviews</h2>
                <div class="separator-two"></div>
            </div>
        </div>
        <div class="testimonial-carousel owl-carousel owl-theme">
            <?php
                 $query2 = "SELECT * FROM home_reviews" ;
                $result2=mysqli_query($con,$query2);
       
               while($data = mysqli_fetch_assoc($result2))
               {
                ?>
            <!-- Testimonial Block -->
            <div class="testimonial-block">
                <div class="inner-box">
                    <div class="thumb"><img src="images/resource/<?php echo $data['reviews_image']; ?>" alt="<?php echo $data['reviews_name']; ?>" title="<?php echo $data['reviews_name']; ?>"></div>
                    <p><?php echo $data['reviews_description']; ?></p>
                    <h5 class="name"><a href="#"><?php echo $data['reviews_name']; ?> ,</a></h5> <span class="designation" style="color: orange;"><?php echo $data['reviews_designation']; ?></span>
                </div>
            </div>
             <?php
                        }

                        ?>
            

            
        </div>
    </section>
    <!-- Testimonial Section -->

    <!-- News Section -->
    <section class="news-section">
        <div class="auto-container">
            <div class="sec-title text-center">
                <span class="title">Press Release</span>
                <h2>Our latest <span>news</span></h2>
                <div class="separator-two"></div>
            </div>

             

            <div class="row clearfix">
                <!-- News Blcok -->
               

                <!-- News Blcok -->
                 <?php
                 $query2 = "SELECT * FROM home_latestnews" ;
                $result2=mysqli_query($con,$query2);
       
               while($data = mysqli_fetch_assoc($result2))
               {
                ?>
                <div class="news-block col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image-box-latestnews"><img src="images/resource/<?php echo $data['latestnews_image']; ?>"
                        data-darkbox="images/resource/<?php echo $data['latestnews_thumbnail']; ?>"
                        data-darkbox-group="one"
                        data-darkbox-description="" alt="<?php echo $data['latestnews_name']; ?>" title="<?php echo $data['latestnews_name']; ?>"></div>
                        
                    </div>
                    <div class="lower-content">    
                        <h3><a data-darkbox="images/resource/<?php echo $data['latestnews_thumbnail']; ?>" alt="<?php echo $data['latestnews_name']; ?>" title="<?php echo $data['latestnews_name']; ?>"><?php echo $data['latestnews_name']; ?></a></h3>
                       <!-- <p><?php echo $data['latestnews_description']; ?></p> -->
                       <!-- <a href="blog-single-1.php" class="read-more">Read More</a> -->
                    </div>
                </div>
                <?php
            }
            ?>

                <!-- News Blcok -->
               
            </div>
        </div>
    </section>
    <!-- End News -->

    <!-- Subscribe section -->
    <section class="subscribe-section">
        <div class="auto-container">
            <div class="inner-container">
                <div class="row clearfix">
                    <div class="title-column col-md-6 col-sm-12 col-xs-12">
                        <div class="title">
                            <h2>Donation Hotline : +91 9000302235 / 9010942738</h2>
                        </div>
                    </div>
                    <div class="form-column col-md-6 col-sm-12 col-xs-12">
                        <div class="subscribe-form">
                            <form method="post" action="#" onsubmit="return false">
                                <div class="form-group">
                                    <input type="email" name="subscribeemail" id="subscribeemail" value="" placeholder="Enter Your email" required>
                                    <button type="submit" class="theme-btn btn-style-three subscribe">Subscribe</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Subscribe section -->

    <!--Main Footer-->
    <footer class="main-footer" style="background-image: url(images/background/2.jpg);">
        <!--Upper-->
        <div class="footer-upper">
            <div class="auto-container">
                <div class="outer-box">
                    <!--Footer Logo-->
                <div class="footer-logo"><a href="index.php"><img src="images/footer-logo.png" alt="footer-logo" title="footer-logo"></a></div>
                    
                    <div class="row clearfix">
                        <div class="big-column col-md-4 col-sm-12 col-xs-12">
                            <div class="row clearfix">
                                <!--Footer Column-->
                                <h3>Menu</h3>
                                <ul class="footer-links">
                                     <li><a href="index.php">Home</a></li>
                                    <li><a href="about-us.php">About us</a></li>
                                    <li><a href="causes-list.php">Causes</a></li> 
                                    <li><a href="event-list.php">Events</a></li>                              
                                    <li><a href="contact-us.php">Contact Us</a></li>
                                </ul> 
                            </div>
                        </div>

                        <div class="big-column col-md-4 col-sm-12 col-xs-12">
                            <div class="row clearfix">
                                <!--Footer Column-->
                               <h3>Contact </h3>
                                <ul class="footer-links contact-links">
                                    <li>D.No: 28-4-797-2, State Bank Colony <br>
                                     JNTU Road, Ananthapuram - 515002 (AP)</li>
                                    <li>+91 9000302235 / 9010942738</li>
                                    <li><a href="#">info@soumikcharitabletrust.org</a></li>
                                </ul> 
                            </div>
                        </div>
                        <div class="big-column col-md-4 col-sm-12 col-xs-12">
                            <div class="row clearfix">    
                                <!--Footer Column-->
                                <h3>About</h3>
                                <div class="text">
                                    <p>Our charity lends the helping hand to the people in desperate situations. When their life becomes the destiny of the darkest path.</p>
                                </div>
                                <br>
                                <h3>Follow</h3>
                                <ul class="footer-social-links">
                                    <li><a href="https://www.facebook.com/soumikcharitabletrust/" target="_blank"><i class="fab fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fab fa-google-plus"></i></a></li>
                                </ul> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!--Bottom-->
        <div class="footer-bottom">
            <div class="auto-container">
                <div class="copyright">Copyright <a href="index.php">Soumik Charitable Trust</a> © 2018. All Rights Reserved.</div>
            </div>
        </div>
    </footer>
    <!-- End Footer -->

</div>  
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-angle-double-up"></span></div>

<script src="js/jquery.js?soumikversion=1"></script> 
<script src="js/bootstrap.min.js?soumikversion=1"></script>
<!--Revolution Slider-->
<script src="plugins/revolution/js/jquery.themepunch.revolution.min.js?soumikversion=1"></script>
<script src="plugins/revolution/js/jquery.themepunch.tools.min.js?soumikversion=1"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.actions.min.js?soumikversion=1"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.carousel.min.js?soumikversion=1"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.kenburn.min.js?soumikversion=1"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js?soumikversion=1"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.migration.min.js?soumikversion=1"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.navigation.min.js?soumikversion=1"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.parallax.min.js?soumikversion=1"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.slideanims.min.js?soumikversion=1"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.video.min.js?soumikversion=1"></script>
<script src="js/main-slider-script.js?soumikversion=1"></script>
<!--End Revolution Slider-->
<script src="js/jquery-ui.js?soumikversion=1"></script> 
<script src="js/darkbox.js?soumikversion=1"></script>
<script src="js/jquery.fancybox.js?soumikversion=1"></script>
<script src="js/owl.js?soumikversion=1"></script>
<script src="js/wow.js?soumikversion=1"></script>
<script src="js/appear.js?soumikversion=1"></script>
<script src="js/script.js?soumikversion=1"></script>
</body>
</html>
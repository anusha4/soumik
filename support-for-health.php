<?php include('addons/header.php'); ?>

        <!-- Sticky Header -->
        <div class="sticky-header">
            <div class="auto-container clearfix">
                <!--Logo-->
                <div class="logo pull-left">
                    <a href="index.php" title=""><img src="images/logo-small.png" alt="soumik-scroll-logo" title="soumik-scroll-logo"></a>
                </div>
                <!--Right Col-->
                <div class="pull-right">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="about-us.php">About us</a></li>
                                <li class="dropdown"><a href="services.php" >Services</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="support-for-education.php"> Education </a></li>    
                                        <li><a href="support-for-health.php"> Health </a></li>
                                        <li><a href="helping-poor-people.php"> Helping People </a></li>
                                    </ul>
                                </li>
                                <li><a href="causes-list.php">Causes</a></li> 
                                <li><a href="event-list.php">Events</a></li>                              
                                <li><a href="contact-us.php">Contact Us</a></li>
                                <?php
                                if(isset($_SESSION['username'])=='')
                                {
                                ?>
                                <li><a href="signin-registration.php">Login</a></li>
                                <?php
                            }
                            else
                            {
                            ?>

                                <li class="dropdown"><a href="services.php" ><?php echo $_SESSION['fullname'];?></a>
                                    <ul class="dropdown-menu">
                                       
                                        <li><a href="profile.php"> My Profile </a></li>
                                        <li><a href="logout.php"> Logout </a></li>
                                    </ul>
                                </li>
                                <?php
                            }
                            ?>
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>
            </div>
        </div>
    </header>
    <!--End Main Header -->

     <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/health-bg.jpg);">
        <div class="auto-container">
            <h1>Health</h1>
            <ul class="bread-crumb clearfix">
                <li><a href="index.php">Home </a></li>
                <!-- <li><a href="services.php">Services</li> -->
                <li>Health</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
 <section class="work-section style-two alternate">
        <div class="auto-container">
            <!-- Work Steps -->
            <div class="steps-outer">
                <div class="row clearfix">
                    <hr>
                   <div class="sec-title text-center">
               <h2> Support for <span> Children Health</span></h2>
                <div class="separator-two"></div>
            </div>
            <br>
                    

    <div class="work-step health-inner col-md-12 col-sm-12 col-xs-12">
       <div class="inner-box">
         <h3 style="color: orange;text-align: center;">
             <span style="font-weight: bold;font-size: 30px;">“</span>Good health is real wealth <span style="font-weight: bold;font-size: 30px;">”</span></h3><br>
          <p>Health is essential aspect for everyone. But there are lot of people who are not able to afford for 
basic medical treatment because of their financial disability. </p><br>
              <p>Despite of all the odds, Soumik Charity strongly supporting the people who are in need of 
medical treatment for any cause.</p><br>
             <p> Soumik Charity providing medical funding for a range of purposes and assist the financial help, donations to individuals or groups. We conducted many camps in various villages to provide free medical treatment and distribute medicines.</p><br>
          <p>Soumik Charity has the aim of helping helpless people to meet their minimum needs.</p>
       <p>Support helping the needy people for health to live healthier life.</p>
                            
                             </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
    <!--end work section-->
    

    
   <?php include('addons/footer.php'); ?>
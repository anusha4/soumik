<?php include('addons/header.php'); ?>

        <!-- Sticky Header -->
        <div class="sticky-header">
            <div class="auto-container clearfix">
                <!--Logo-->
                <div class="logo pull-left">
                    <a href="index.php" title=""><img src="images/logo-small.png" alt="" title=""></a>
                </div>
                <!--Right Col-->
                <div class="pull-right">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="about-us.php">About us</a></li>
                                <li class="dropdown"><a href="services.php" >Services</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="support-for-education.php"> Education </a></li>    
                                        <li><a href="support-for-health.php"> Health </a></li>
                                        <li><a href="helping-poor-people.php"> Helping People </a></li>
                                    </ul>
                                </li>
                                <li><a href="causes-list.php">Causes</a></li> 
                                <li><a href="event-list.php">Events</a></li>                              
                                <li><a href="contact.php">Contact Us</a></li>
                                <li><a href="signin-registration.php">Login</a></li>
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>
            </div>
        </div>
    </header>
    <!--End Main Header -->

    <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/causes-signle-bg.jpg);">
        <div class="auto-container">
            <h1>Causes Single</h1>
            <ul class="bread-crumb clearfix">
                <li><a href="index.php">Home </a></li>
                <li>Causes Single</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->

    <!-- Sidebar Page Container -->
    <div class="sidebar-page-container">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Content Side-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="cause-single">
                        <!-- Cause Block -->
                        <?php
                         $sl_no=$_GET['sl_no'];
                 $query13 = "SELECT * FROM home_causes where slno='$sl_no'" ;
                $result13=mysqli_query($con,$query13);
                $i=1;
       
               while($data = mysqli_fetch_assoc($result13))
               {
                ?>
                        <div class="cause-block">
                            <div class="inner-box">
                                <div class="image-box">
                                    <figure><img src="images/resource/<?php echo $data['cause_image']; ?>" title="<?php echo $data['cause_title']; ?>" alt="<?php echo $data['cause_title']; ?>"></figure>
                            
                                    <!--<figure>-->
                                    <!--    <img src="images/resource/cause-single.jpg" alt="">-->
                                    <!--</figure>-->
                                </div>
                                <div class="lower-content">
                                    <div class="progress-bar">
                                        <div class="bar-inner"><div class="bar progress-line" data-width="0"><div class="bar-percentage"><div class="count-box"><span class="count-text" data-speed="2000" data-stop="0">0</span>%</div></div></div></div>
                                    </div>
                                    <div class="info-box clearfix">
                                        <a class="raised">Raised: <span>0</span></a>
                                        <a class="goal">Goal: <span>0</span></a>
                                    </div>  
                                   <h2><?php echo $data['cause_title']; ?></h2>
                                      <p><?php echo $data['cause_desc']; ?></p>
                                </div>
                            </div>
                        </div>
                        <?php
            }
            ?>
                        <!-- Donation Form -->   
                        <div class="sec-title ">
                            <span class="title">Donation</span>
                            <h2>Make a <span>donation</span> now!</h2>
                        </div>
                        <!-- Donation Form -->
                        <div class="donation-info">
                            <p class="">We support “education for the children”, “treatment for the poor”, “social activities to define the responsibilities of the citizens” and many more to improve the life of the poor people in the society. For a better society for the next generation, it is vital to help the poor children and single women to restore the values in the society.</p>
                        </div>
                        <a href="payu-checkout.php" class="theme-btn btn-style-one"><span>Donate Now</span></a>
                    </div>
                </div>

                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <aside class="sidebar">

                        <!-- Testimonial Widget -->
                        <div class="sidebar-widget qutoe-widget">
                            <div class="widget-content">
                                <blockquote>
                                    <span class="icon flaticon-right-quotation-sign"></span>
                                    Collaboratively administrate empowered markets via networks. ize customer directed convergence without revolutionary ROI.
                                    <cite>-Nattasha</cite>
                                </blockquote>
                            </div>
                        </div>

                        <!-- Post Widget -->
                        <div class="sidebar-widget post-widget">
                            <div class="sidebar-title"><h3>Past Event</h3></div>
                            <?php
                 $query2 = "SELECT * FROM home_events" ;
                $result2=mysqli_query($con,$query2);
       
               while($data = mysqli_fetch_assoc($result2))
               {
                ?>
                            <div class="widget-content">
                                
                                

                                <article class="post">
                                    <div class="thumb"><a href="event-single.php">
                                         <i class="far fa-calendar-alt"></i>
                                        <!--<img src="images/resource/post-thumb-2.jpg" alt="">-->
                                        </a></div>
                                    <h3><a href="event-single.php"><?php echo $data['events_name']; ?></a></h3>
                                     <div><?php echo $data['events_fromtime']; ?> / <?php
                                        $originalDate = $data['events_date'];
                             $newDate = date("M d , Y", strtotime($originalDate));
                             echo $newDate;;
                             ?></div>
                                       
                                    <!--<div class="date">08:30 am / Apr 14, 2018</div>-->
                                </article>

                                <?php
               }
               ?>
                            </div>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </div>  
    <!-- End Sidebar Page Container -->

    
    <?php include('addons/footer.php'); ?>
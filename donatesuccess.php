<style type="text/css">
  .O_P_S_full_box
{
  width: 600px;
 border: 2px solid #0F666E;
    height: 400px;
}
.O_P_S_full_box p
{
  font-size: 14px;
margin-top: 36px;
color: #0F666E;
}
.O_P_S_full_box
{
  padding: 42px 0;
  position: absolute;
  top: 50%;
  left: 50%;
  transform:translate(-50%,-50%);
  -webkit-transform:translate(-50%,-50%);
  -ms-transform:translate(-50%,-50%);
  -o-transform:translate(-50%,-50%);
  -moz-transform:translate(-50%,-50%);
}
.icon_div i
{
font-size: 62px;
border: 4px solid #1bb51f;
padding: 14px;
border-radius: 50%;
color: #1bb51f;
margin-top: 0px;
margin-bottom: 16px;
}
.icon_div
{
position: relative;
 -webkit-animation: bounceIn 0.3s; /* Safari 4.0 - 8.0 */
    animation: bounceIn 0.3s;
    animation-delay:1s;
}
@-webkit-keyframes bounceIn {
  from, 20%, 40%, 60%, 80%, to {
    -webkit-animation-timing-function: cubic-bezier(0.215, 0.610, 0.355, 1.000);
    animation-timing-function: cubic-bezier(0.215, 0.610, 0.355, 1.000);
  }

  0% {
    opacity: 0;
    -webkit-transform: scale3d(.3, .3, .3);
    transform: scale3d(.3, .3, .3);
  }

  20% {
    -webkit-transform: scale3d(1.1, 1.1, 1.1);
    transform: scale3d(1.1, 1.1, 1.1);
  }

  40% {
    -webkit-transform: scale3d(.9, .9, .9);
    transform: scale3d(.9, .9, .9);
  }

  60% {
    opacity: 1;
    -webkit-transform: scale3d(1.03, 1.03, 1.03);
    transform: scale3d(1.03, 1.03, 1.03);
  }

  80% {
    -webkit-transform: scale3d(.97, .97, .97);
    transform: scale3d(.97, .97, .97);
  }

  to {
    opacity: 1;
    -webkit-transform: scale3d(1, 1, 1);
    transform: scale3d(1, 1, 1);
  }
}
.order_placed_scfly_text
{
  font-size: 32px;
color: #0F666E;
}
</style>
<!DOCTYPE html>
<html>
<head>
	<title>order placed successfully</title>
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/animatesuccess.css" rel="stylesheet">
<!-- <link href="css/stylessuccess.css" rel="stylesheet"> -->


</head>
<body>

<div class="O_P_S_full_box">
	<div class="text-center icon_div"><i class="fa fa-check"></i></div>
  <h3 class="text-center order_placed_scfly_text">Donation Successful</h3>
  <p class="text-center">Your Transaction of 20 has been completed successfully<br>
  The Invoice of transaction has been send to your over mail<br></p>
  <p class="text-center"><span id="count">5</span> seconds</p>
	
	
</div>

<script type="text/javascript">

window.onload = function(){

(function(){
  var counter = 5;

  setInterval(function() {
    counter--;
    if (counter >= 0) {
      span = document.getElementById("count");
      span.innerHTML = counter;
    }
    // Display 'counter' wherever you want to display it.
    if (counter === 0) {
      window.location.href = "http://soumikcharitabletrust.org";
        clearInterval(counter);
    }

  }, 1000);

})();

}

</script>
</body>
</html>